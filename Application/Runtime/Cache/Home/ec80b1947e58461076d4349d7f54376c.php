<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link href="/Public/Theme3/css/font-awesome.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <script language="javascript" src="/Public/Theme3/js/jquery-1.11.1.min.js"></script>
    <script language="javascript" src="/Public/Theme3/js/jquery.gcjs.js"></script>
    <!--<link rel="stylesheet" type="text/css" href="./Public/Theme3/addons/sz_yi/template/mobile/default/static/css/style_red.css">-->


    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/base.css">
    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/jquery-weui.min.css">
    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/weui.min.css">
    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/style.css">
    <!--new add start for style1 2016.09.09-->
    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/font-awesome.min(1).css">

    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/style1.css">
    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/main.css">
    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/iconfont.css">

    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/mui.min.css">
    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/tuandui.css">
    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/icons-extra.css"/>


<nav class="mui-bar mui-bar-tab">
     
      <a href="/Goods/goods_list" class="mui-tab-item">
                      <span class="mui-icon mui-icon-extra mui-icon-extra-cart
                                   "></span>
                      <span class="mui-tab-label">商城</span>
                    </a>
     
	    		    <a href="/Home/Index/index"  class="mui-tab-item ">
	    		        <span class="mui-icon mui-icon-extra mui-icon-extra-peoples"></span>
	    		        <span class="mui-tab-label">团队</span>
	    		    </a>
	    		    
                    <a href="/User/order_list" class="mui-tab-item">
                      <span class="mui-icon mui-icon-extra mui-icon-extra-order"></span>
                      <span class="mui-tab-label">订单</span>
                    </a>
	    		    <a  href="/User/UserSet" class="mui-tab-item mui-active">
	    		        <span class="mui-icon mui-icon-gear"></span>
	    		        <span class="mui-tab-label">设置</span>
	    		    </a>
	    		</nav>
    <!--new add end for style1-->
    <script>window.PointerEvent = undefined</script>
</head>
<body>


<title>设置</title>

<style type="text/css">
    body {
        margin: 0px;
        background: #fff;
        font-family: '微软雅黑';
        -moz-appearance: none;
    }
    .mui-bar a:active{color:#007aff}
    .mui-bar a:hover{color:#007aff}
    .mui-bar a:link{color:#007aff}
    .info_main {
        height: auto;
        background: #fff;
        margin-top: 30px;
    }

    .info_main .line {
        margin: 0 3% 15px 3%;
        height: 40px;
        border: 1px solid #e8e8e8;
        line-height: 40px;
        color: #999;
        padding: 2px 0;
        box-sizing: content-box;
        position: relative;
    }

    .info_main .line .title {
        height: 40px;
        width: 40px;
        line-height: 40px;
        color: #444;
        float: left;
        font-size: 16px;
    }

    .info_main .line .title img {
        width: 30px;
        height: 30px;
        vertical-align: top;
        padding: 2px 0 0 5px;
        box-sizing: content-box;
    }

    .info_main .line .info {
        width: 100%;
        float: right;
        margin-left: -40px;
    }

    .info_main .line .inner {
        margin-left: 40px;
    }

    .info_main .line .inner input {
        height: 38px;
        width: 100%;
        display: block;
        padding: 0px;
        margin: 0px;
        border: 0px;
        float: left;
        font-size: 16px;
        color: #aeaeae;
        background: #fff;
        outline: none;
    }

    .info_main .line .inner .user_sex {
        line-height: 40px;
    }

    .editinfo {
        height: 44px;
        margin: 14px 3%;
        background: #ff644e;
        border-radius: 4px;
        text-align: center;
        font-size: 16px;
        line-height: 44px;
        color: #fff;
    }

    .register {
        height: 44px;
        margin: 14px 3%;
        background: #ff644e;
        border-radius: 4px;
        text-align: center;
        font-size: 16px;
        line-height: 44px;
        color: #fff;
    }

    .exit {
        height: 44px;
        margin: 14px 3%;
        background: #ff644e;
        border-radius: 4px;
        text-align: center;
        font-size: 16px;
        line-height: 44px;
        color: #fff;
    }

    /*.info_sub {height:44px; margin:14px 5px; background:#ccc; border-radius:4px; text-align:center; font-size:16px; line-height:44px; color:#fff;}*/
    .nobindmobile {
        clear: both;
        height: 44px;
        margin: 14px 5px;
        background: #ccc;
        border-radius: 4px;
        text-align: center;
        font-size: 16px;
        line-height: 44px;
        color: #fff;
    }

    .select {
        border: 1px solid #ccc;
        height: 25px;
    }

    #zphone {
        position: absolute;
        right: 0;
        top: 0;
        background: #ff644e;
        border: none;
        width: 25%;
        height: 100%;
        color: #fff;
        margin-right: 3%;
    }

    .info_main {
        width: 90%;
        margin: 10px auto;
        /* line-height: 28px;*/
    }

    .navs p {
        text-align: center;
        line-height: 40px;
    }

    .navs1 p {
        text-align: center;
        line-height: 40px;
    }

    .navs-left {
       /* float: left;
        margin-left: 8%;
        width: 40%;
        background-color: #efefef82;*/
        border-radius: 10px;
        box-shadow: #ccc 0px 0px 30px 5px inset;
    }

     .user_data {
        border-radius: 20px;
        width: 90%;
        border: solid 1px #777;
        margin: 10px 5%;
        box-shadow: darkgrey 0 0 8px 0px;
    }

    .user_data p {
        margin: 20px 10px 20px 10px;
        width: 80%;
    }

    .navs img {
        border: 0;
        width: 70px;
        height: 75px;
        margin-left: 27%;
        margin-top: 11%;
    }

    .navs1 img {
        border: 0;
        width: 70px;
        height: 75px;
        margin-left: 27%;
        margin-top: 11%;
    }
   .mui-media{background-color: none;}
  .mui-media p{  padding-bottom:10px ;}
  .mui-media a{background-color: #efefef82; border-radius: 10px; box-shadow:#ccc 0px 0px 30px 5px inset;}
  .mui-grid-view.mui-grid-9{ background-color: #fff;}
  .mui-grid-view.mui-grid-9 .mui-table-view-cell>a:not(.mui-btn){margin:5px;}

    .mui-bar a:hover{color:#007aff}
    
 </style>
<div class="backcover"></div>
<div id="container">
    <div class="page_topbar">
        <a href="javascript:history.back()" class="back"><i class="fa fa-angle-left"></i></a>
        <div class="title">设置</div>
    </div>

    <!---->
    <div class="user_data">
        <div>
            <p>用户姓名：<?php echo ($userinfo["truename"]); ?><br/></p>
            <p>用户等级：<?php echo ($userinfo["level"]); ?><br/></p>
            <p>联系电话：<?php echo ($userinfo["loginname"]); ?><br/></p>
        </div>


        <div style="float: right; position: absolute; margin-top: -150px; margin-left: 65%;">
            <p><img class="editinfos" style="width: 45px" src="/Public/Theme3/images/shezhisz.png"/></p>
            <p><img class="registers" style="width: 41px" src="/Public/Theme3/images/xiugai.png"/></p>
        </div>
    </div>


    <div class="mui-content">
        <ul class="mui-table-view mui-grid-view mui-grid-9">
            <li class="mui-table-view-cell mui-media mui-col-xs-6 mui-col-sm-6"><a href="#">
             <div class="shengji"><img src="/Public/Theme3/images/dingdan.png"/>
                <p><?php echo ($userinfo["sjorder"]); ?>个</p></div>
            </a>
          </li>
          <li class="mui-table-view-cell mui-media mui-col-xs-6 mui-col-sm-6">
            <a href="#">
              <div>
                <img src="/Public/Theme3/images/dianzan.png"/>
                <p><?php echo ($userinfo["dzcount"]); ?>个</p>
              </div>
           </a>
         </li>
            <li class="mui-table-view-cell mui-media mui-col-xs-6 mui-col-sm-6">
              <a href="#">
                 <div class="rucian"><img src="/Public/Theme3/images/shenhesz.png"/>
                <p><?php echo ($userinfo["shorder"]); ?>个</p></div>
            </a></li>
            <li class="mui-table-view-cell mui-media mui-col-xs-6 mui-col-sm-6">
              <a href="#">
                  <div>
                <img src="/Public/Theme3/images/toushu.png"/>
                <p><?php echo ($userinfo["tscount"]); ?>个</p></div>
            </a></li>


        </ul>
    </div>

    <form name="postform" ACTION="/Home/User/findpwdOp/" method="post" id="postform" enctype="multipart/form-data"
          onsubmit="return checkform()">
        <div class="info_main">
            &emsp;

        </div>
        <!--<div class="editinfo"><?php echo ($user); ?> 修改资料</div>-->
        <!--<div class="register">修改密码</div>-->
        <div class="exit">退出</div>

        <div>
    </form>

</div>

<script>


    $(".editinfos").click(function () {
        location.href = "/user/editInfo";
    })
    $(".registers").click(function () {
        location.href = "/user/uploadPwd";
    })
    $(".exit").click(function () {
        location.href = "/LoginTrue/ExitLogin";
    })
   /*$(".rucian").click(function () {
        location.href = "/User/userchecksj";
    })
    $(".shengji").click(function () {
        location.href = "/User/usersj";
    })*/


</script>



<div style="height:60px;"></div>

</body>
</html>