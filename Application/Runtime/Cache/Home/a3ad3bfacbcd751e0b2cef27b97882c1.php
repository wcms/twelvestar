<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>产品详情页</title>

    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" type="text/css" href="/Public/Theme1/css/goods.css">
	<link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <!-- Data Tables -->
    <link href="/Public/Theme1/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <style>
        body{
            font-family: "微软雅黑", "open sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        }
        
        .callout-info {    border-color: #0097bc;
    color: #fff !important;
    background-color: #00c0ef !important;
    margin-left: 20px;
    height: 50px;
    width: 90%;
    margin-top: 15px;
    line-height: 50px;
    padding-left: 0;
    font-size: 16px;
    border-radius: 8px;
    text-align: center;
    margin: 0;
    margin: 10px auto;
        }

        .callout {
            border-color: #0097bc;
        }

        .import li {
            list-style: none;
        }
        .goods-content img{width:100%;}
        #wrapper .mc{width:100%;}
    </style>

</head>
<body style="cursor: auto;">
<div class="callout callout-info"><p>产品详情</p></div>
<div id="bg" class="bg" style="display:none;"></div>
<div class="all-nav all-nav-border">
    <div class="w1210">
    </div>
</div>
<div class="margin-w1210 clearfix goods_box">
    <div class="w">
    </div>
    <div class="blank"></div>
    <div id="product-intro" class="goods-info row">
        <div id="preview" class="col-md-6 col-sm-6 col-xs-12">
            <div class="goods-img" id="li_1">
                <a href="<?php echo ($goods_info["goods_img"]); ?>"
                   class="MagicZoom" id="zoom" rel="zoom-position: right;" title="产品图片"
                   style="position: relative; display: inline-block; text-decoration: none; outline: 0px; width:100%;">
                    <img src="<?php echo ($goods_info["goods_img"]); ?>">
                </a>
            </div>
        </div>
        <div class="goods-detail-info col-md-6 col-sm-6 col-xs-12" >
            <form action="javascript:addToCart(1)" method="post" name="ECS_FORMBUY" id="ECS_FORMBUY">
                <div class="goods-name" style="height: 50px;padding-left: 15px">
                    <h1><?php echo ($goods_info["goods_name"]); ?></h1>
                </div>
                <div id="goods-price" style="height: auto;padding-left: 15px">
                    <div class="mar-l">

                        <span class="price">售价</span><strong class="p-price" id="ECS_GOODS_AMOUNT">¥<?php echo ($goods_info["goods_price"]); ?> + <?php echo ($goods_info["goods_gwjf"]); ?> 积分</strong>

                        <!-- <span class="depreciate"><a href="javascript:showDiv(1);">欢迎购买</a></span> -->
                    </div>
                    <div class="show-price">
                        <div class="market-prices-spe">
                            <span class="price">市场价</span>
                            <font class="market-price">¥<?php echo ($shop_price); ?>元</font>
                        </div>
                    </div>
                    <div class="show-price">
                        <div class="market-prices-spe">
                            <span class="price">已领取</span>
                            <font class=""><?php echo ($goods_num); ?></font>
                        </div>
                    </div>
                    <!-- <div class="show-price" >
                        <div class="market-prices-spe">
                            <span class="price" style="color: #00a2d4;font-weight: bold">购买此产品后,您的账号有效期将会增加</span>
                            <font color="red" style="font-weight: bold"><?php echo ($goods_info["goods_day"]); ?>天</font>
                        </div>
                    </div> -->
                </div>
                <div class="buyNub-buy-wrap" style="">
                    <div id="choose-btns" class="buyNub-buy" style="display: block;">

                        <a href="/Goods/goods_buy/goods_id/<?php echo ($goods_info["goods_id"]); ?>" name="bi_addToCart" class="u-buy1">立即购买</a>


                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="right-con">
        <div id="wrapper">
            <div id="main_widget_1">
              
			  

                <div class="mc" id="os_shouhou">
                    <div class="my-comment-pre">
                        <div class="tab-title"><span>产品详情</span></div>
                        <div class="goods-content goods-sales-support" >
                           <?php echo ($goods_info["goods_desc"]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="right-sidebar-con">
</div>
<div id="tell-me-result" class="tell-me-form" style="display:none;">
    <div class="pop-header"><span>温馨提示</span><a href="javascript:closeDiv1()" title="关闭" class="tell-me-close"></a>
    </div>
    <div class="tell-me-content">
        <div class="tell-me-result">
            <div class="result-icon"></div>
            <div class="result-text">
                <div class="title"></div>
                <div class="bottom">
                    <span onclick="javascript:closeDiv1();" class="cancel-btn">关闭</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="pop-mask"></div>
</body>
</html>