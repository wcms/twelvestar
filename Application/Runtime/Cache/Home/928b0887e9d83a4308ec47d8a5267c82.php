<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>产品订购</title>
    <link rel="stylesheet" href="/Public/Theme1/css/goods_buy_AdminLTE.min.css">
    <link rel="stylesheet" href="/Public/Theme1/css/goods_buy_style.css" type="text/css">


    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="telephone=no,email=no" name="format-detection">
    <link rel="stylesheet" href="/Public/Theme1/css/alert/animate.min.css"/> <!-- 动画效果 -->
    <link rel="stylesheet" href="/Public/Theme1/css/alert/common.css"/><!-- 页面基本样式 -->

<style>
	.example{text-align:center;}
	.demo a{width:39%;height:44px;}
	.order-but2{width:100%;height:44px;font-size:16px;font-weight:bold;}
        .content{padding:0px;}
        .order-goods-info{
            word-break: break-all;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
        }
</style>
</head>
<body class="skin-blue sidebar-mini" style="">
<div class="wrapper">
    <div class="content-wrapper">

        <link href="/Public/Theme1/css/goods_buy_style2.css" rel="stylesheet" type="text/css">
        <section class="content-header">
            <h1>
                产品订购
                <small>goods orders</small>
            </h1>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="nav-tabs-custom">
                        <div class="tab-content">
                            <div class="xm-box">
                                <div class="hd">
                                    <h3 class="title">订单号：<input value="<?php echo ($order_sn); ?>" id="order_sn" style="border: 0px solid white;font-size: 14px"/> </h3>
                                </div>
                                <div class="bd">
                                    <div class="order-delivery-items">
                                        <div class="order-delivery-item">
                                            <table class="order-delivery-table">
                                                <thead>
                                                <tr>
                                                    <th class="cell-order-goods">商品信息</th>
                                                    <th class="cell-order-total">订单金额</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td class="cell-order-goods">
                                                        <div class="order-goods-info"><?php echo ($goods_info["goods_name"]); ?></div>
                                                        <div class="order-goods-price xs_hidden"><?php echo ($goods_info["goods_price"]); ?>元 + <?php echo ($goods_info["goods_gwjf"]); ?> 积分
                                                        </div>
                                                        <div class="order-goods-amount">x <?php echo ($goods_number); ?></div>
                                                        <div class="order-goods-price"><?php echo ($total_price); ?>元 + <?php echo ($total_gwjf); ?> 积分</div>
                                                    </td>
                                                    <td rowspan="1"><?php echo ($total_price); ?>元  + <?php echo ($total_gwjf); ?> 积分</td>
                                                
                                                </tr>
												<tr>
													<td colspan="2">
													
													  <span id="check_user_36899" price="<?php echo ($total_price); ?>">未付款<br>
<div class="box">
    <div class="demo">
        <div class="example">
            <a class="rollIn dialog" style="cursor: pointer;">
                <input style="cursor: pointer;" class="order-but order-but2" type="button" value="支付"
                       style="text-align: center"></a>
            <input type="hidden" value="<?php echo ($user_info["loginname"]); ?>" name="loginname" id="loginname"/>
            <input type="hidden" value="<?php echo ($user_info["id"]); ?>" name="user_id" id="user_id"/>
            <input type="hidden" value="<?php echo ($goods_info["goods_id"]); ?>" name="goods_id" id="goods_id"/>
            <input type="hidden" value="<?php echo ($goods_info["goods_price"]); ?>" name="goods_price"/>
            <input type="hidden" value="<?php echo ($total_price); ?>" name="total_price"/>
            <input type="hidden" value="<?php echo ($goods_number); ?>" name="goods_number" id="goods_number"/>
        </div>

    </div>
    <!-- 注意：请将要放入弹框的内容放在比如id="HBox"的容器中，然后将box的值换成该ID即可，比如：$(element).hDialog({'box':'#HBox'}); -->
    <div id="HBox">
        <form action="" method="post" onsubmit="return false;">
            <ul class="list">
                <li>
                    <strong>二级密码  <font color="#ff0000">*</font></strong>
                    <div class="fl"><input type="password" name="pwd2" class="ipt nickname" id="password"/></div>
                </li>
                <li><input type="submit" value="确认提交" class="submitBtn"/></li>
            </ul>
        </form>
    </div><!-- HBox end -->
    </div><!-- box end --></span>
													</td>
												</tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="order-delivery-address clearfix">
                                        <div class="order-text-section-l">
                                            <h4>收货信息 </h4>
                                            <table class="order-text-table">
                                                <tbody>
                                                <tr>
                                                    <th>你的姓名：</th>
                                                    <td><?php echo ($user_info["shouhuo_name"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <th>收货地址：</th>
                                                    <td><?php echo ($user_info["address"]); ?></td>
                                                </tr>
                                                <tr>
                                                    <th>联系电话：</th>
                                                    <td><?php echo ($user_info["shouhuo_tel"]); ?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>


<script src="/Public/Theme1/js/alert_js/jquery-1.9.1.min.js"></script>
<script src="/Public/Theme1/js/alert_js/jquery.hDialog.min.js"></script>
<script>
    $(function () {
        var $el = $('.dialog');
        $el.hDialog(); //默认调用

        //页面加载完成后自动执行
        $('#autoShow').hDialog({title: '页面加载完成后自动执行', autoShow: true});

		

        //提示后回调
        $('.demo47').click(function () {
            $.tooltip('hello...', 1000, true, function () {
                $.tooltip('执行回调...');
            });
        });

        //confirm
        $('.demo48').click(function () {
            $.dialog('confirm', '提示', '您确认要删除么？', 0, function () {
                $.tooltip('删除成功～', 2000, true, function () {
                    //$.closeDialog();
                    $.closeDialog(function () {
                        alert('还可以回调了。');
                    });
                });
            });
        });

        //alert
        $('.demo49').click(function () {
            //$.dialog('alert','提示','正在处理中...'); 或者 $.dialog('alert','提示','正在处理中...',0); //不自动关闭
            $.dialog('alert', '提示', '正在处理中...', 2000, function () {
                $.tooltip('执行回调...', 2000, true);
            }); //2s自动关闭
        });

        //iframe
        $('.demo50').hDialog({
            types: 2,
            iframeSrc: 'http://css.doyoe.com/',
            iframeId: 'iframeHBox',
            width: 960,
            height: 600
        });

        //返回顶部
        $.goTop();

        //提交并验证表单
        $('.submitBtn').click(function () {
            var EmailReg = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/; //邮件正则
            var PhoneReg = /^0{0,1}(13[0-9]|15[0-9]|153|156|18[7-9])[0-9]{8}$/; //手机正则
            var $nickname = $('.nickname');
            var $email = $('.email');
            var $phone = $('.phone');
            var user_id = $("#user_id").val();
            var password = $("#password").val();
            var goods_id=$("#goods_id").val();
            var goods_number=$("#goods_number").val();
            var amount=<?php echo ($user_info["bonus_amount"]); ?>;
            var gouwujifen=<?php echo ($user_info["gouwujf"]); ?>;
            var order_amount=<?php echo ($total_price); ?>;
            var order_gwjf=<?php echo ($total_gwjf); ?>;
            var order_sn=$('#order_sn').val();
            if(amount<order_amount){
                $.tooltip('您的余额不足');
                document.getElementById('HBox').style.display="none";  //不显示
                $el.hDialog('close', {box: '#HBox'});
            } 
			if(gouwujifen<order_gwjf){
                $.tooltip('您的积分不足');
                document.getElementById('HBox').style.display="none";  //不显示
                $el.hDialog('close', {box: '#HBox'});
            }
            $.ajax({
                type: "GET",
                url: "/Home/Goods/goods_order/pwd2/pwd2/user_id/" + user_id + '/password/' + password,
                data: {user_id: user_id},
                success: function (data) {
                    if (data == '0') {
                        $.tooltip('二级密码错误');
                        $nickname.focus();
                    } else {
                        $.tooltip('提交成功，2秒后自动关闭', 2000, true,
                            function () {
                                document.getElementById('HBox').style.display="none";  //不显示
//                                $el.hDialog('close', {box: '#HBox'});
//                                top.location='/Home/Goods/goods_deal/';
                                $.ajax({
                                    type: "GET",
                                    url: "/Home/Goods/goods_deal/user_id/" + user_id + '/goods_id/' + goods_id+'/goods_number/'+goods_number+'/order_amount/'+order_amount+'/order_gwjf/'+order_gwjf+'/order_sn/'+order_sn,
                                    data: {user_id:user_id},
                                    success:function (data) {
                                       if(data=='1'){
                                           $.tooltip('您的余额不足');
                                       }
									   else if(data=='2'){
                                           $.tooltip('您的积分不足');
                                       }else{
                                           $.tooltip('产品订购成功');
                                           location.href='/User/order_list';
                                    }
                                    }
                                });

                            });
                    }
                }
            });
        });

    });
</script>

</body>
</html>