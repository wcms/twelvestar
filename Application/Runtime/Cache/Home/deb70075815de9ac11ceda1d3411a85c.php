<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>订单列表</title>
</head>
<style>
    body{background:#f5f5f5;padding:0px;}
    .orderbox{padding:5px;display:flex;flex-flow: column;border-bottom:10px;background:#fff;font-size:12px;line-height:25px;}
    .order_info{ display: flex;
    justify-content: space-between;border-bottom:1px #f5f5f5 solid;padding:10px;}
    .order_action{
            display: flex;
    justify-content: flex-end;
    }
    .btn{    width: 100px;
    text-align: center;
    background: orange;
    color: #fff;
    text-decoration: none;
    border-radius: 5px;}
    a{    text-decoration: none;
}
</style>
<body>
    	<?php if(is_array($order_list)): foreach($order_list as $key=>$val_staffLists): ?><div class="orderbox">
                    <div class="order_info">
                 <div><?php echo (date('Y-m-d H:i:s',$val_staffLists["add_time"])); ?></div>

                <div><?php echo ($val_staffLists["order_status_pp"]); ?></div>

            </div>                                                        
     <div class="order_info">
         <div><?php echo ($val_staffLists["goods_name"]); ?></div>
	 <div>共<?php echo ($val_staffLists["goods_number"]); ?>件商品<?php echo ($val_staffLists["goods_price"]); ?> + <?php echo ($val_staffLists["goods_gwjf"]); ?>积分</div>
									<!-- <td><?php echo ($val_staffLists["goods_day"]); ?> 天</td> -->
     </div>		

        <div class="order_action"><a href="/Home/User/order_list/id/<?php echo ($val_staffLists["id"]); ?>" class="btn">查看</a></div>
    </div><?php endforeach; endif; ?>


</body>


  <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/mui.min.css">
    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/tuandui.css">
  <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/icons-extra.css" />


<nav class="mui-bar mui-bar-tab">
     
      <a href="/Goods/goods_list" class="mui-tab-item">
                      <span class="mui-icon mui-icon-extra mui-icon-extra-cart
                                   "></span>
                      <span class="mui-tab-label">商城</span>
                    </a>
     
	    		    <a href="/Home/Index/index"  class="mui-tab-item ">
	    		        <span class="mui-icon mui-icon-extra mui-icon-extra-peoples"></span>
	    		        <span class="mui-tab-label">团队</span>
	    		    </a>
	    		    
                    <a href="/User/order_list" class="mui-tab-item mui-active">
                      <span class="mui-icon mui-icon-extra mui-icon-extra-order"></span>
                      <span class="mui-tab-label">订单</span>
                    </a>
	    		    <a  href="/User/UserSet" class="mui-tab-item ">
	    		        <span class="mui-icon mui-icon-gear"></span>
	    		        <span class="mui-tab-label">设置</span>
	    		    </a>
	    		</nav>

</html>