<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>

<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link href="/Public/Theme3/css/font-awesome.min.css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">
<meta name="format-detection" content="telephone=no">
<script language="javascript" src="/Public/Theme3/js/jquery-1.11.1.min.js"></script>
<script language="javascript" src="/Public/Theme3/js/jquery.gcjs.js"></script>
<!--<link rel="stylesheet" type="text/css" href="./Public/Theme3/addons/sz_yi/template/mobile/default/static/css/style_red.css">-->



<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/base.css">
<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/jquery-weui.min.css">
<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/weui.min.css">
<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/style.css">
<!--new add start for style1 2016.09.09-->
<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/font-awesome.min(1).css">

<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/style1.css">
<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/main.css">
<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/iconfont.css">

<!--new add end for style1-->
<script>window.PointerEvent = undefined</script></head>
<body>


<title><?php echo ($Sys_systemName["sName"]); ?></title>

<link rel="stylesheet" href="/Public/Theme3/css/swiper.min.css">
<style type="text/css">
  body{background:#fff}
/*通用样式*/
/* common */
.flex {
    display: -webkit-box;      /* OLD - iOS 6-, Safari 3.1-6 */
    display: -moz-box;         /* OLD - Firefox 19- (buggy but mostly works) */
    display: -ms-flexbox;      /* TWEENER - IE 10 */
    display: -webkit-flex;     /* NEW - Chrome */
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;             /* NEW, Spec - Opera 12.1, Firefox 20+ */
}
/* 宽度随屏幕改变变化 */
.flex-1 {
    flex: 1;
    -webkit-flex: 1;
}

/* 垂直 水平 居中 */
.flex-center {
    justify-content: center;
    -webkit-justify-content: center;
    align-items: center;
    -webkit-align-items: center;
}
/* 垂直 居中 */
.flex-ver {
    align-items: center;
    -webkit-align-items: center;
}
.flex-col-ver{
    justify-content: center;
    -webkit-justify-content: center;
}
/* 换行 */
.flex-wrap{
    flex-wrap: wrap;
    -moz-flex-wrap:wrap;
    -webkit-flex-wrap: wrap;
}
.flex-reverse{
    flex-direction:row-reverse;
    -webkit-flex-direction:row-reverse;
    -moz-flex-direction:row-reverse;
}
/* flex 容器（设置为flex）内子元素向两边顶齐平分 */
.flex-jcsb {
    justify-content: space-between;
    -webkit-justify-content: space-between;
}
/*中间空白左右各一个*/
.flex-around{
    justify-content: space-between;
    -webkit-justify-content: space-between;
  }
/*平均分每个空间*/
.flex-pjf{
    justify-content: space-around;
    -webkit-justify-content: space-around;
}
.flex-end{
    align-items: flex-end;
    -webkit-align-items: flex-end;
}
.flex-end1{
    justify-content: flex-end;
    -webkit-justify-content: flex-end;
}

.flex-col {

    flex-direction: column;
    -webkit-flex-direction: column;

}
/*通用结束*/
/*新样式*/
p{
	margin: 0;
	padding: 2px;
}
.rucian_tb{
	background: url(/Public/Theme3/images/bg3.gif) no-repeat;
    height: 135px;
    background-size: 100% 100%;
    padding: 20px;
}
.rucian_gg{
	background-color: rgb(255, 255, 255);
    height: 35px;
}
.rucian_da{
	width: 100%;
	padding: 2.5%;
}
.rucian_a{
	width: 40%;
    margin: 5%;
    padding: 6%;
    border-radius: 10px;
    background-color: #efefef82;
    box-shadow: #ccc 0px 0px 30px 5px inset;
}
.flex-wrap span{text-align:center}
.rucian_a span{
	width: 100%;
    text-align: center;
    color: rgb(0, 0, 0);
    margin-top: 10px;
}
.rucian_aimg{
	width: 100%;
    height: 50px;
    text-align: center;
}
.rucian_aimg img{
	width: 50px;
    height: 50px;
}
.rucian_kf{
    margin: 0 7.5%;
    height: 80px;
    padding: 5px 9px;
    border: 1px solid rgba(0, 0, 0, 0.25098039215686274);
    border-radius: 10px;
}
.rucian_kf p{
    height: 35px;
    line-height: 35px;
}
.rucian_aaaa {
    display: block;
    width: 47%;
    margin: 0 auto;
    text-align: center;
    background-color: rgb(241, 83, 83);
    color: rgb(255, 255, 255) !important;
    line-height: 35px;
    border-radius: 6px;
    margin-top: 30px;
}
.swiper-box,.swiper-container, .swiper-wrapper, .swiper-slide {
    width: 100%;
    height: 172px;
  }
  .swiper-slide img {
    width: 100%;
    height: 100%;
  }
  .advert{
    width: 100%;
    height: 40px;
    background: #fff;
    margin: 0 auto;
    line-height: 40px;
}
.ad-title{
    width: 17%;
    height: 100%;
    float: left;
    padding-left: 10px;
    text-align: right;
}
#ad-main{
    width: 83%;
    float: left;
}
.u-set{    width: 25px;
    height: 25px;
    position: absolute;
    right: 10px;
    top: 10px;z-index:9999;}
    .mui-bar a:hover{color:#007aff}
 </style>
<div class="rucian_tb">
   <p>ID：<?php echo ($user_info["id"]); ?></p>
   <p>等级：<?php echo ($user_info["levelName"]); ?></p>
   <p>姓名：<?php echo ($user_info["truename"]); ?></p>
    推荐码：<?php echo ($user_info["tuijianma"]); ?><p></p>
</div>


                <a class="u-set" href="/User/UserSet">
                    <img src="/Public/Theme3/images/shezhi.png" >
                </a>
           

<!-- 公告 -->
<div class="advert"><div class="ad-title"><img src="/Public/Theme3/images/laba.png" alt=""></div>
<marquee scrollamount="5" id="ad-main">
	<?php if(is_array($news_list)): foreach($news_list as $key=>$user): ?><span style="color: rgb(160, 0, 49);"><?php echo ($user["title"]); ?></span>&nbsp;<?php echo ($user["content"]); ?><br><?php endforeach; endif; ?>
</marquee></div>
			
<!-- 轮播图 -->
<!-- <div class="swiper-box"> -->
<?php if($Sys_systemName[sCheckCodeSwitch1] == 1): ?><div class="swiper-container swiper-container-horizontal">
    <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
              <div class="swiper-slide swiper-slide-active" style="width: 768px;"><a href="<?php echo ($Sys_systemName["hdp1url"]); ?>"><img src="/uploads/lunboimg/<?php echo ($Sys_systemName["hdp1"]); ?>" alt=""></a></div>
              <div class="swiper-slide swiper-slide-next" style="width: 768px;"><a href="<?php echo ($Sys_systemName["hdp2url"]); ?>"><img src="/uploads/lunboimg/<?php echo ($Sys_systemName["hdp2"]); ?>" alt=""></a></div>
              <div class="swiper-slide" style="width: 768px;"><a href="<?php echo ($Sys_systemName["hdp3url"]); ?>"><img src="/uploads/lunboimg/<?php echo ($Sys_systemName["hdp3"]); ?>" alt=""></a></div>
            </div>
  </div><?php endif; ?>
<!-- </div> -->


<div class="rucian_da flex flex-ver flex-wrap">
	<a href="/User/add_user" class="rucian_a flex flex-ver flex-wrap">
		<div class="rucian_aimg">
			<img src="/Public/Theme3/images/zhuce.png" alt="">
		</div>
		<span>帮助注册</span>
	</a>
	<a href="/User/usersj" class="rucian_a flex flex-ver flex-wrap">
		<div class="rucian_aimg">
			<img src="/Public/Theme3/images/shengji.png" alt="">
		</div>
		<span>申请升级</span>
	</a>
	<a href="/User/userchecksj" class="rucian_a flex flex-ver flex-wrap">
		<div class="rucian_aimg">
			<img src="/Public/Theme3/images/shenhe.png" alt="">
		</div>
		<span>审核升级</span>
	</a>
	<a href="/User/team" class="rucian_a flex flex-ver flex-wrap">
		<div class="rucian_aimg">
			<img src="/Public/Theme3/images/tuandui.png" alt="">
		</div>
		<span>我的团队</span>
	</a>

</div>

<div class="rucian_kf">
    <p>联系客服：<?php echo ($Sys_systemName["sCompanyName"]); ?></p>
    <p>工作时间：<?php echo ($Sys_systemName["sCompanyTel"]); ?></p>
      
        <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/mui.min.css">
    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/tuandui.css">
  <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/icons-extra.css" />


 <nav class="mui-bar mui-bar-tab">
     
      <a href="/Goods/goods_list" class="mui-tab-item">
                      <span class="mui-icon mui-icon-extra mui-icon-extra-cart
                                   "></span>
                      <span class="mui-tab-label">商城</span>
                    </a>
     
	    		    <a href="/Home/Index/index"  class="mui-tab-item mui-active">
	    		        <span class="mui-icon mui-icon-extra mui-icon-extra-peoples"></span>
	    		        <span class="mui-tab-label">团队</span>
	    		    </a>
	    		    
                    <a href="/User/order_list" class="mui-tab-item">
                      <span class="mui-icon mui-icon-extra mui-icon-extra-order"></span>
                      <span class="mui-tab-label">订单</span>
                    </a>
	    		    <a  href="/User/UserSet" class="mui-tab-item">
	    		        <span class="mui-icon mui-icon-gear"></span>
	    		        <span class="mui-tab-label">设置</span>
	    		    </a>
	    		</nav>


<script src="/Public/Theme3/js/swiper.min.js"></script>
<script type="text/javascript">
    var swiper = new Swiper('.swiper-container', {
        autoplay : 3000,
        /*pagination: '.swiper-pagination',
        paginationClickable: true,
        spaceBetween: 30,*/
    });

</script>
      <div style="height:60px;"></div>
</body></html>