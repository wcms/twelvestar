<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>商城</title>

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/Public/Theme2/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme2/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/Public/Theme2/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/Public/Theme2/css/welcome.min.css?v=4.1.0" rel="stylesheet">
    <style>
        .callout-info {
            border-color: #0097bc;
            color: #fff !important;
            background-color: #00c0ef !important;
            margin-left: 20px;
            height: 50px;
            width: 100%;
            margin-top: 15px;
            line-height: 50px;
            padding-left: 15px;
            font-family: "微软雅黑", "open sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 16px;
            border-radius: 8px;
        }

        .callout {
            border-color: #0097bc;
        }
        .import li {
            list-style: none;
        }
        a {
    text-decoration: none;
}
    </style>
</head>

<body class="gray-bg" style="background: #f2f4f8;">
<div>
    <div class="callout callout-info"><p>购买产品使用奖金和购物积分</p></div>
   
   
<div class="wrapper wrapper-content">
   
    <div class="list-div">
        <ul  class="import">
        <?php if(is_array($goods_info)): foreach($goods_info as $key=>$item): ?><li>
            <div class="col-sm-2">
                <div class="widget style1 yellow-bg">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <p><a href="/Goods/goods_info/goods_id/<?php echo ($item["goods_id"]); ?>"><img src="<?php echo ($item["goods_img"]); ?>" width="200" height="180"></a>
                            </p>
                            <p><a style="color: #ffffff; font-size:14px;" href="/Goods/goods_info/goods_id/<?php echo ($item["goods_id"]); ?>"><?php echo ($item["goods_name"]); ?></a></p>
                            <p><a style="color: #ffffff; font-size:14px;" href="/Goods/goods_info/goods_id/<?php echo ($item["goods_id"]); ?>">产品价格 : ￥<?php echo ($item["goods_price"]); ?> + <?php echo ($item["goods_gwjf"]); ?> 积分</a></p>
                            <!-- <p><a style="color: #ffffff; font-size:14px;" href="/Goods/goods_info/goods_id/<?php echo ($item["goods_id"]); ?>">账户增加时长 : <font color=""></font><?php echo ($item["goods_day"]); ?> 天</a></p> -->
                        </div>
                    </div>
                </div>
            </div>
        </li><?php endforeach; endif; ?>
    </ul>
	   

    </div>

</div>

</div>
    
    
      <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/mui.min.css">
    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/tuandui.css">
  <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/icons-extra.css" />

 <nav class="mui-bar mui-bar-tab">

    <a href="/Goods/goods_list" class="mui-tab-item mui-active">
                      <span class="mui-icon mui-icon-extra mui-icon-extra-cart
                                   "></span>
                      <span class="mui-tab-label">商城</span>
                    </a>
     
	    		    <a href="/Home/Index/index"  class="mui-tab-item ">
	    		        <span class="mui-icon mui-icon-extra mui-icon-extra-peoples"></span>
	    		        <span class="mui-tab-label">团队</span>
	    		    </a>
	    		    
                    <a href="/User/order_list" class="mui-tab-item">
                      <span class="mui-icon mui-icon-extra mui-icon-extra-order"></span>
                      <span class="mui-tab-label">订单</span>
                    </a>
	    		    <a  href="/User/UserSet" class="mui-tab-item">
	    		        <span class="mui-icon mui-icon-gear"></span>
	    		        <span class="mui-tab-label">设置</span>
	    		    </a>
	    		</nav>

    
    



</body>

</html>