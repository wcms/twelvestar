<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>修改会员信息</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/Public/Theme1/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/Public/Theme1/static/plupload/upfiless.css" rel="stylesheet" type="text/css"/>
    <link href="/Public/Theme1/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet" type="text/css"/>
    <link href="/Public/Theme1/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"
          rel="stylesheet">

		  
</head>
<body class="gray-bg">

 <?php if($ispassword != 1): ?><style>
				#alldiv{position: absolute;
    background: rgba(0,0,0,0.3);
    width: 100%;
    height: 100%;top:0px;left:0px;
    z-index: 999;}
				.divinput{background:#fff;height:190px;margin:10% auto 0;z-index:1000;    padding-top: 14px;
    text-align: center;}
			</style>
		
<div id="alldiv">
	<div class="divinput">
		<h2>请输入二级密码：</h2>
		<input type="password" value="" name="password2" id="password2" class="form-control" style="width:60%;display:inline-block;margin:10px 0px;"/>
		<input type="button" value="确认" id="btnconfirmpassword" onclick="confirmpassword()"  class="btn btn-primary" style="width:60%;display:block;margin:10px auto;" />
	</div>
	
		<script>
				function confirmpassword(){
					var thisvalue = $("#password2").val();
					if(thisvalue.length==0){
						alert("请输入二级密码");
						return false;
					}
					 htmlobj = $.ajax({url: "/Home/User/CheckPassword?username=" + escape(thisvalue), async: false});
					if (escape(htmlobj.responseText) == "1") {
						alert("密码输入错误");
						return false;
					}
					else{
						location.href= "/Home/User/Update_user";
						return true;
					}
				}
		</script>	
		
</div>
<?php else: ?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>修改会员信息</h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" action="" class="form-horizontal" id="form-admin-add" enctype="multipart/form-data">

                        <div class="form-group">
                            <label class="col-sm-1 control-label">会员编号</label>
                            <div class="col-sm-3">
                                <input type="text" value="<?php echo ($userarr["loginname"]); ?>" name="txt_loginname" id="txt_loginname" readonly="readonly" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 control-label">会员姓名</label>
                            <div class="col-sm-3">
                                <input type="text" value="<?php echo ($userarr["truename"]); ?>" name="txt_truename" id="txt_truename"   class="form-control" readonly="readonly"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-1 control-label">身份证号</label>
                           <div class="col-sm-3">
                                <input type="text" value="<?php echo ($userarr["identityid"]); ?>" name="txt_identityid" id="txt_identityid" class="form-control" required="required" />
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-1 control-label">手机号码</label>
                            <div class="col-sm-3">
                                <input type="number" name="txt_tel" id="txt_tel" class="form-control" pattern="^1[3-9]\d{9}$" value="<?php echo ($userarr["tel"]); ?>"  required="required"/>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 control-label">收货姓名</label>
                            <div class="col-sm-3">
                                <input type="text" name="txt_shouhuo_name" id="txt_shouhuo_name" class="form-control" value="<?php echo ($userarr["shouhuo_name"]); ?>"  required="required"/>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 control-label">收货电话</label>
                            <div class="col-sm-3">
                                <input type="text" name="txt_shouhuo_tel" id="txt_shouhuo_tel" class="form-control" value="<?php echo ($userarr["shouhuo_tel"]); ?>" pattern="^1[3-9]\d{9}$" required="required"/>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 control-label">收货地址</label>
                            <div class="col-sm-3">
                                <input type="text" name="txt_address" id="txt_address" class="form-control" value="<?php echo ($userarr["address"]); ?>"  required="required" placeholder=" 请按照省/市/区/街道/小区/门牌号填写,按该地址发货,否则概不负责"/>
                            </div>

                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">更新会员信息</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><?php endif; ?>
<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
<script src="/Public/Theme1/js/plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript" src="/Public/Theme1/check/js/jquery.validate.min.js"></script>

<script type="text/javascript" src="/Public/Theme1/check/js/messages_zh.min.js"></script>

<script type="text/javascript" src="/Public/Theme1/check/js/validate-methods.js"></script>
<script type="text/javascript" src="/Public/Theme1/lib/webuploader/0.1.5/webuploader.min.js"></script>

<script>
    $(document).ready(function () {
        $('#txt_shouhuo_name').focus();
//        $(".i-checks").iCheck({checkboxClass: "icheckbox_square-green", radioClass: "iradio_square-green",});
    });
</script>


</body>

</html>