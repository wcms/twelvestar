<?php if (!defined('THINK_PATH')) exit();?>
	<!DOCTYPE html>

<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link href="/Public/Theme3/css/font-awesome.min.css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">
<meta name="format-detection" content="telephone=no">
<script language="javascript" src="/Public/Theme3/js/jquery-1.11.1.min.js"></script>
<script language="javascript" src="/Public/Theme3/js/jquery.gcjs.js"></script>
<!--<link rel="stylesheet" type="text/css" href="./Public/Theme3/addons/sz_yi/template/mobile/default/static/css/style_red.css">-->



<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/base.css">
<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/jquery-weui.min.css">
<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/weui.min.css">
<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/style.css">
<!--new add start for style1 2016.09.09-->
<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/font-awesome.min(1).css">

<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/style1.css">
<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/main.css">
<link rel="stylesheet" type="text/css" href="/Public/Theme3/css/iconfont.css">
  
    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/mui.min.css">
    <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/tuandui.css">
  <link rel="stylesheet" type="text/css" href="/Public/Theme3/css/icons-extra.css" />



<nav class="mui-bar mui-bar-tab">
	    		    <a href="/Index/index" class="mui-tab-item">
	    		        <span class="mui-icon mui-icon-home"></span>
	    		        <span class="mui-tab-label">首页</span>
	    		    </a>
	    		     <a href="/User/add_user" class="mui-tab-item">
                      <span class="mui-icon mui-icon-extra mui-icon-extra-addpeople
                                   "></span>
                      <span class="mui-tab-label">注册</span>
                    </a>
                    <a href="/User/team" class="mui-tab-item">
                      <span class="mui-icon mui-icon-extra mui-icon-extra-peoples"></span>
                      <span class="mui-tab-label">团队</span>
                    </a>
	    		    <a  href="/User/UserSet" class="mui-tab-item">
	    		        <span class="mui-icon mui-icon-gear"></span>
	    		        <span class="mui-tab-label">设置</span>
	    		    </a>
	    		</nav>


<!--new add end for style1-->
<script>window.PointerEvent = undefined</script></head>
<body>


<title>申请升级</title>

<style type="text/css">
body {
    font-family: Arial, Helvetica, Sans-Serif;
    background-color: #fff;
}

/*通用*/
/* common */
.flex {
    display: -webkit-box;      /* OLD - iOS 6-, Safari 3.1-6 */
    display: -moz-box;         /* OLD - Firefox 19- (buggy but mostly works) */
    display: -ms-flexbox;      /* TWEENER - IE 10 */
    display: -webkit-flex;     /* NEW - Chrome */
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;             /* NEW, Spec - Opera 12.1, Firefox 20+ */
}
/* 宽度随屏幕改变变化 */
.flex-1 {
    flex: 1;
    -webkit-flex: 1;
}

/* 垂直 水平 居中 */
.flex-center {
    justify-content: center;
    -webkit-justify-content: center;
    align-items: center;
    -webkit-align-items: center;
}
/* 垂直 居中 */
.flex-ver {
    align-items: center;
    -webkit-align-items: center;
}
.flex-col-ver{
    justify-content: center;
    -webkit-justify-content: center;
}
/* 换行 */
.flex-wrap{
    flex-wrap: wrap;
    -moz-flex-wrap:wrap;
    -webkit-flex-wrap: wrap;
}
.flex-reverse{
    flex-direction:row-reverse;
    -webkit-flex-direction:row-reverse;
    -moz-flex-direction:row-reverse;
}
/* flex 容器（设置为flex）内子元素向两边顶齐平分 */
.flex-jcsb {
    justify-content: space-between;
    -webkit-justify-content: space-between;
}
/*中间空白左右各一个*/
.flex-around{
    justify-content: space-between;
    -webkit-justify-content: space-between;
  }
/*平均分每个空间*/
.flex-pjf{
    justify-content: space-around;
    -webkit-justify-content: space-around;
}
.flex-end{
    align-items: flex-end;
    -webkit-align-items: flex-end;
}
.flex-end1{
    justify-content: flex-end;
    -webkit-justify-content: flex-end;
}

.flex-col {

    flex-direction: column;
    -webkit-flex-direction: column;

}
/*通用结束*/
.rucian_sj{
    width: 85%;
    margin: 0 auto;
    margin-top: 15%;
    margin-bottom: 15%;
    padding: 10px;
    background-color: rgb(242, 242, 242);
    position: relative;
}
.rucian_tj{
    width: 40%;
    margin: 0 auto;
    line-height: 40px;
    border-radius: 7px;
    background-color: rgb(255, 255, 255);
    text-align: center;
    border: 1px solid rgb(27, 25, 25);
}

.wsjxx{
    position: absolute;
    top: 0;
    right: 11px;
}
.shdiv{border:1px solid #333;width:90%;margin:10px auto;overflow:hidden;padding:8px;}

.shdiv .sdleft{float:left;width:55%;    line-height: 28px;}
.shdiv .sdright{float:right;width:45%;text-align:center;}
.shdiv .sdright .tousu{    border: 1px solid #ddd;
    display: block;
    border-radius: 10px;
    padding: 5px 0px;
    color: #333;
    margin-top: 6px;}
 

.rucian_sj_2{height: auto;
    width: 90%;
    padding-bottom: 9px;
    margin-bottom: 0px;}
.rucian_sj_2 span{float:right;}
.bgdiv{position:fixed;width:100%;height:100%;background:rgba(0,0,0,0.5);top:0px;left:0;display:none;z-index:99}
.tousudiv{  background: #fff;
    width: 80%;
    margin: 0 auto;
    margin-top: 10%;
	
    position: absolute;
    height: 74%;
    z-index: 999;
    left: 10%;display:none;}
.tousudiv .tsuser{text-align:center;padding:10px 0px 15px 0;}
.tousudiv textarea{    border: 1px solid #ddd;
    display: block;
    width: 90%;
    margin: 0 auto;
    height: 100%;
    /* height: 383px; */
    /* position: relative; */}
.tousudiv button{      border: 1px solid #ddd;
    background: #fff;
    margin-top: 10px;
    margin: 10px auto;
    /* display: block; */
    width: 43%;
    padding: 3% 0;
    font-size: 14px;
    border-radius: 10px;
    margin: 10px 0px 0px 4%;}

	.cxsq{    border: 1px solid #ddd;
    display: block;
	margin:0 auto;text-align:center;background:#ff644e;color:#fff;
    border-radius: 10px;
    padding: 8px 0px;
    color: #fff;
    margin-top: 12px;
	font-size:15px;
	width:50%;}   
    .mui-bar a:hover{color:#007aff}
 </style>
<script src="/Public/Theme3/js/reg/mobiscroll.core-2.5.2.js" type="text/javascript"></script>
<script src="/Public/Theme3/js/reg/mobiscroll.core-2.5.2-zh.js" type="text/javascript"></script>
<link href="/Public/Theme3/js/reg/mobiscroll.core-2.5.2.css" rel="stylesheet" type="text/css">
<link href="/Public/Theme3/js/reg/mobiscroll.animation-2.5.2.css" rel="stylesheet" type="text/css">
<script src="/Public/Theme3/js/reg/mobiscroll.datetime-2.5.1.js" type="text/javascript"></script>
<script src="/Public/Theme3/js/reg/mobiscroll.datetime-2.5.1-zh.js" type="text/javascript"></script>
<script src="/Public/Theme3/js/reg/mobiscroll.android-ics-2.5.2.js" type="text/javascript"></script>
<link href="/Public/Theme3/js/reg/mobiscroll.android-ics-2.5.2.css" rel="stylesheet" type="text/css">

<div id="container">
<div class="bgdiv">
</div>
<div class="tousudiv">
	<form id="tsform" name="tsform">
	<div class="tsuser">
		投诉用户：<span id="tsuser"></span>
		<input type="hidden" name="tsuserobj" id="tsuserobj" value="" />
		<input type="hidden" name="tsorderid" id="tsorderid" value="" />
		
	</div>
	
		<input type="hidden" name="tstype" id="tstype" value="1" />
	<textarea name="tscontent" id="tscontent" placeholder="请输入投诉理由："></textarea>
	<button type="button" onclick="subthis()">提  交</button>
	<button class="qvxiao" type="button" onclick="closethis()">取  消</button>
	</form>
</div>
<div class="page_topbar">
    <a href="javascript:;" class="back" onclick="javascript:location.href='/'"><i class="fa fa-angle-left"></i></a>
    <div class="title">申请升级</div>
    <a class="wsjxx" href="/user/shuserlist">升级记录</a>
</div>
 <?php if($isExists == 1): ?><div class="rucian_sj rucian_sj_2">
    <div>你有申请正在处理 <span>升级为<?php echo ($shinfo["targetlevel"]); ?>星会员</span></div>
</div>

 <?php if($shinfo['shuser1']): ?><div class="shdiv">
	<div class="sdleft">
		<ul>
			<li>用户姓名：<?php echo ($shinfo["shuser1user"]["truename"]); ?></li>
			<li>用户等级：<?php echo ($shinfo["shuser1user"]["levelname"]); ?></li>
			<li>联系方式：<?php echo ($shinfo["shuser1"]); ?></li>
			<li>审核状态：<?php echo ($shinfo["shuserstatus1"]); ?></li>
		</ul>
	</div>
	<div class="sdright">
		<img  src="/uploads/<?php echo ($shinfo["shuser1user"]["wximg"]); ?>"  width="80" height="80" />
		
		<?php if($shinfo['status1'] !=2){ ?>
		<a href="javascript:tousu('<?php echo ($shinfo["shuser1"]); ?>',<?php echo ($shinfo["id"]); ?>)" class="tousu">我要投诉</a>
		<?php } ?>
	</div>
	
</div><?php endif; ?>
 <?php if($shinfo['shuser2']): ?><div class="shdiv">
	<div class="sdleft">
		<ul>
			<li>用户姓名：<?php echo ($shinfo["shuser2user"]["truename"]); ?></li>
			<li>用户等级：<?php echo ($shinfo["shuser2user"]["levelname"]); ?></li>
			<li>联系方式：<?php echo ($shinfo["shuser2"]); ?></li>
			<li>审核状态：<?php echo ($shinfo["shuserstatus2"]); ?></li>
		</ul>
	</div>
	<div class="sdright">
		<img  src="/uploads/<?php echo ($shinfo["shuser2user"]["wximg"]); ?>"  width="80" height="80" />
		
		<?php if($shinfo['status2'] !=2){ ?>
		<a href="javascript:tousu('<?php echo ($shinfo["shuser2"]); ?>',<?php echo ($shinfo["id"]); ?>)" class="tousu" >我要投诉</a>
		<?php } ?>
		
	</div>
	
</div><?php endif; ?>
<?php if($issh == 1): ?><a href="/user/sjactionagan/oid/<?php echo ($shinfo["id"]); ?>" class="cxsq">重新申请</a><?php endif; ?>
<script>
	function tousu(val,id){
		var hei = $(".tousudiv").height();
		$("#tsuser").html(val);
		$("#tsuserobj").val(val);
		$("#tsorderid").val(id);
		$("#tscontent").val("");
		$(".tousudiv textarea").css("height",hei-120);
		$(".tousudiv").css("display","block");
		$(".bgdiv").css("display","block");
		
		
	}
	function subthis(){
		//$("#tsform")
		 $.ajax({
				type: "POST",
				url: "/Home/User/tousush",
				data: $('#tsform').serialize(),
				success: function (result) {
					var strresult=result;
					if(strresult==1){
						alert("投诉失败");
					}
					else if(strresult==3){
						alert("你已投诉过");
						$(".tousudiv").css("display","none");
						$(".bgdiv").css("display","none");
					}
					else if(strresult==4){
						alert("24小时内不能投诉");
						$(".tousudiv").css("display","none");
						$(".bgdiv").css("display","none");
					}
					else{
						alert("投诉成功");
						$(".tousudiv").css("display","none");
						$(".bgdiv").css("display","none");
					}
				},
				error: function(data) {
					
				 }

			});
	}
	$(document).ready(function(){ 
　		}
	); 
</script>

 <?php else: ?>
 
<div class="rucian_sj">
    <div>您当前的等级：<?php echo ($userinfo["standardlevelname"]); ?></div>
    <div>您可申请升级的等级：<?php echo ($userinfo["target_standardlevelname"]); ?></div>
    <div>是否提交申请？</div>
</div>

<a href="/user/sjaction" class="rucian_tj flex flex-center" id="tijiao">提交申请</a><?php endif; ?>
  
<script>

    $(".qvxiao").click(function () {
        location.href = "/User/usersj";
    })


</script>
<div style="height:60px;"></div>

</body></html>