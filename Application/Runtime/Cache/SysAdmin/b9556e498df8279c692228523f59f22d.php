<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>系统参数设置</title>
    <link rel="shortcut icon" href="favicon.ico"> <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/Public/Theme1/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style.min.css?v=4.1.0" rel="stylesheet">

	<style>
		.form-control{width:5%;    display: inline-block;}
	</style>
</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>系统配置参数 <small></small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="form_basic.html#">
                                <i class="fa fa-wrench"></i>
                            </a>

                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="/SysAdmin/System/SystemParamAction" class="form-horizontal" enctype="multipart/form-data">
                          
                             <div class="form-group">
                                <label class="col-sm-2 control-label">短信接口管理</label>
                                <div class="col-sm-10">
                                 http://www.smsbao.com/
                                </div>
                            </div>                        
                            <div class="form-group">
                                <label class="col-sm-2 control-label">短信接口账号</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="msgno" id="msgno"  value="<?php echo ($rs_systemInfo["msgno"]); ?>" style="width:50%;" required >
                                 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">短信接口密钥</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="msgscrepet" id="msgscrepet"  value="<?php echo ($rs_systemInfo["msgscrepet"]); ?>"  style="width:50%;" required >
                                
                                </div>
                            </div>
						
                            <div class="form-group">
                                <label class="col-sm-2 control-label">推荐码前缀</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="inviteStart" id="inviteStart"  value="<?php echo ($rs_systemInfo["inviteStart"]); ?>" required >
                                  例如：13000000000这个账号的推荐码是ck561201，这个推荐码前面的“ck”就是前缀，最后6位数字为系统生成。
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">参数说明</label>
                                <div class="col-sm-10">
								需要直推【多少】个【多少】星会员并且团队有【多少】个【多少】星会员才能申请升级，首要审核级别第【多少】级第【多少】星，次要审核级别第【多少】级第【多少】星，如果不用某个条件就设置为“-1”。
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">升级一星</label>
                                <div class="col-sm-10">
								需要直推 <input type="text" class="form-control" name="one_star_ge" id="one_star_ge"  value="<?php echo ($rs_systemInfo["one_star_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="one_star_level" id="one_star_level"  value="<?php echo ($rs_systemInfo["one_star_level"]); ?>" required >星会员且团队下有
								 <input type="text" class="form-control" name="one_star_team_ge" id="one_star_team_ge"  value="<?php echo ($rs_systemInfo["one_star_team_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="one_star_team_level" id="one_star_team_level"  value="<?php echo ($rs_systemInfo["one_star_team_level"]); ?>" required >星会员
								 &nbsp;&nbsp;
								首要审核级别第 <input type="text" class="form-control" name="one_star_sh1_dai" id="one_star_sh1_dai"  value="<?php echo ($rs_systemInfo["one_star_sh1_dai"]); ?>" required >代 <input type="text" class="form-control" name="one_star_sh1" id="one_star_sh1"  value="<?php echo ($rs_systemInfo["one_star_sh1"]); ?>" required >星 &nbsp;&nbsp;
								次要审核级别第<input type="text" class="form-control" name="one_star_sh2_dai" id="one_star_sh2_dai"  value="<?php echo ($rs_systemInfo["one_star_sh2_dai"]); ?>" required >代<input type="text" class="form-control" name="one_star_sh2" id="one_star_sh2"  value="<?php echo ($rs_systemInfo["one_star_sh2"]); ?>" required >星
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">升级二星</label>
                                <div class="col-sm-10">
								 需要直推<input type="text" class="form-control" name="two_star_ge" id="two_star_ge"  value="<?php echo ($rs_systemInfo["two_star_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="two_star_level" id="two_star_level"  value="<?php echo ($rs_systemInfo["two_star_level"]); ?>" required >星会员且团队下有
								 <input type="text" class="form-control" name="two_star_team_ge" id="two_star_team_ge"  value="<?php echo ($rs_systemInfo["two_star_team_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="two_star_team_level" id="two_star_team_level"  value="<?php echo ($rs_systemInfo["two_star_team_level"]); ?>" required >星会员
								 &nbsp;&nbsp;
								首要审核级别第<input type="text" class="form-control" name="two_star_sh1_dai" id="two_star_sh1_dai"  value="<?php echo ($rs_systemInfo["two_star_sh1_dai"]); ?>" required >代 <input type="text" class="form-control" name="two_star_sh1" id="two_star_sh1"  value="<?php echo ($rs_systemInfo["two_star_sh1"]); ?>" required >星&nbsp;&nbsp;
								次要审核级别第<input type="text" class="form-control" name="two_star_sh2_dai" id="two_star_sh2_dai"  value="<?php echo ($rs_systemInfo["two_star_sh2_dai"]); ?>" required >代<input type="text" class="form-control" name="two_star_sh2" id="two_star_sh2"  value="<?php echo ($rs_systemInfo["two_star_sh2"]); ?>" required >星
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">升级三星</label>
                                <div class="col-sm-10">
								 需要直推<input type="text" class="form-control" name="three_star_ge" id="three_star_ge"  value="<?php echo ($rs_systemInfo["three_star_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="three_star_level" id="three_star_level"  value="<?php echo ($rs_systemInfo["three_star_level"]); ?>" required >星会员且团队下有
								 <input type="text" class="form-control" name="three_star_team_ge" id="three_star_team_ge"  value="<?php echo ($rs_systemInfo["three_star_team_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="three_star_team_level" id="three_star_team_level"  value="<?php echo ($rs_systemInfo["three_star_team_level"]); ?>" required >星会员
								 &nbsp;&nbsp;
								首要审核级别第<input type="text" class="form-control" name="three_star_sh1_dai" id="three_star_sh1_dai"  value="<?php echo ($rs_systemInfo["three_star_sh1_dai"]); ?>" required >代 <input type="text" class="form-control" name="three_star_sh1" id="three_star_sh1"  value="<?php echo ($rs_systemInfo["three_star_sh1"]); ?>" required >星&nbsp;&nbsp;
								次要审核级别第<input type="text" class="form-control" name="three_star_sh2_dai" id="three_star_sh2_dai"  value="<?php echo ($rs_systemInfo["three_star_sh2_dai"]); ?>" required >代<input type="text" class="form-control" name="three_star_sh2" id="three_star_sh2"  value="<?php echo ($rs_systemInfo["three_star_sh2"]); ?>" required >星
                                   
                                </div>
                            </div>
							
                            <div class="form-group">
                                <label class="col-sm-2 control-label">升级四星</label>
                                <div class="col-sm-10">
								需要直推 <input type="text" class="form-control" name="four_star_ge" id="four_star_ge"  value="<?php echo ($rs_systemInfo["four_star_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="four_star_level" id="four_star_level"  value="<?php echo ($rs_systemInfo["four_star_level"]); ?>" required >星会员且团队下有
								 <input type="text" class="form-control" name="four_star_team_ge" id="four_star_team_ge"  value="<?php echo ($rs_systemInfo["four_star_team_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="four_star_team_level" id="four_star_team_level"  value="<?php echo ($rs_systemInfo["four_star_team_level"]); ?>" required >星会员
								 &nbsp;&nbsp;
								首要审核级别第 <input type="text" class="form-control" name="four_star_sh1_dai" id="four_star_sh1_dai"  value="<?php echo ($rs_systemInfo["four_star_sh1_dai"]); ?>" required >代<input type="text" class="form-control" name="four_star_sh1" id="four_star_sh1"  value="<?php echo ($rs_systemInfo["four_star_sh1"]); ?>" required >星&nbsp;&nbsp;
								次要审核级别第<input type="text" class="form-control" name="four_star_sh2_dai" id="four_star_sh2_dai"  value="<?php echo ($rs_systemInfo["four_star_sh2_dai"]); ?>" required >代<input type="text" class="form-control" name="four_star_sh2" id="four_star_sh2"  value="<?php echo ($rs_systemInfo["four_star_sh2"]); ?>" required >星
                                   
                                </div>
                            </div>
							
                            <div class="form-group">
                                <label class="col-sm-2 control-label">升级五星</label>
                                <div class="col-sm-10">
								需要直推 <input type="text" class="form-control" name="five_star_ge" id="five_star_ge"  value="<?php echo ($rs_systemInfo["five_star_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="five_star_level" id="five_star_level"  value="<?php echo ($rs_systemInfo["five_star_level"]); ?>" required >星会员且团队下有
								 <input type="text" class="form-control" name="five_star_team_ge" id="five_star_team_ge"  value="<?php echo ($rs_systemInfo["five_star_team_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="five_star_team_level" id="five_star_team_level"  value="<?php echo ($rs_systemInfo["five_star_team_level"]); ?>" required >星会员
								 &nbsp;&nbsp;
								首要审核级别第 <input type="text" class="form-control" name="five_star_sh1_dai" id="five_star_sh1_dai"  value="<?php echo ($rs_systemInfo["five_star_sh1_dai"]); ?>" required >代 <input type="text" class="form-control" name="five_star_sh1" id="five_star_sh1"  value="<?php echo ($rs_systemInfo["five_star_sh1"]); ?>" required >星&nbsp;&nbsp;
								次要审核级别第<input type="text" class="form-control" name="five_star_sh2_dai" id="five_star_sh2_dai"  value="<?php echo ($rs_systemInfo["five_star_sh2_dai"]); ?>" required >代<input type="text" class="form-control" name="five_star_sh2" id="five_star_sh2"  value="<?php echo ($rs_systemInfo["five_star_sh2"]); ?>" required >星
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">升级六星</label>
                                <div class="col-sm-10">
								需要直推 <input type="text" class="form-control" name="six_star_ge" id="six_star_ge"  value="<?php echo ($rs_systemInfo["six_star_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="six_star_level" id="six_star_level"  value="<?php echo ($rs_systemInfo["six_star_level"]); ?>" required >星会员且团队下有
								 <input type="text" class="form-control" name="six_star_team_ge" id="six_star_team_ge"  value="<?php echo ($rs_systemInfo["six_star_team_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="six_star_team_level" id="six_star_team_level"  value="<?php echo ($rs_systemInfo["six_star_team_level"]); ?>" required >星会员
								 &nbsp;&nbsp;
								首要审核级别第 <input type="text" class="form-control" name="six_star_sh1_dai" id="six_star_sh1_dai"  value="<?php echo ($rs_systemInfo["six_star_sh1_dai"]); ?>" required >代 <input type="text" class="form-control" name="six_star_sh1" id="six_star_sh1"  value="<?php echo ($rs_systemInfo["six_star_sh1"]); ?>" required >星&nbsp;&nbsp;
								次要审核级别第<input type="text" class="form-control" name="six_star_sh2_dai" id="six_star_sh2_dai"  value="<?php echo ($rs_systemInfo["six_star_sh2_dai"]); ?>" required >代<input type="text" class="form-control" name="six_star_sh2" id="six_star_sh2"  value="<?php echo ($rs_systemInfo["six_star_sh2"]); ?>" required >星
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">升级七星</label>
                                <div class="col-sm-10">
								需要直推 <input type="text" class="form-control" name="seven_star_ge" id="seven_star_ge"  value="<?php echo ($rs_systemInfo["seven_star_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="seven_star_level" id="seven_star_level"  value="<?php echo ($rs_systemInfo["seven_star_level"]); ?>" required >星会员且团队下有
								 <input type="text" class="form-control" name="seven_star_team_ge" id="seven_star_team_ge"  value="<?php echo ($rs_systemInfo["seven_star_team_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="seven_star_team_level" id="seven_star_team_level"  value="<?php echo ($rs_systemInfo["seven_star_team_level"]); ?>" required >星会员
								 &nbsp;&nbsp;
								首要审核级别第 <input type="text" class="form-control" name="seven_star_sh1_dai" id="seven_star_sh1_dai"  value="<?php echo ($rs_systemInfo["seven_star_sh1_dai"]); ?>" required >代 <input type="text" class="form-control" name="seven_star_sh1" id="seven_star_sh1"  value="<?php echo ($rs_systemInfo["seven_star_sh1"]); ?>" required >星&nbsp;&nbsp;
								次要审核级别第<input type="text" class="form-control" name="seven_star_sh2_dai" id="seven_star_sh2_dai"  value="<?php echo ($rs_systemInfo["seven_star_sh2_dai"]); ?>" required >代 <input type="text" class="form-control" name="seven_star_sh2" id="seven_star_sh2"  value="<?php echo ($rs_systemInfo["seven_star_sh2"]); ?>" required >星
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">升级八星</label>
                                <div class="col-sm-10">
								需要直推 <input type="text" class="form-control" name="eight_star_ge" id="eight_star_ge"  value="<?php echo ($rs_systemInfo["eight_star_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="eight_star_level" id="eight_star_level"  value="<?php echo ($rs_systemInfo["eight_star_level"]); ?>" required >星会员且团队下有
								 <input type="text" class="form-control" name="eight_star_team_ge" id="eight_star_team_ge"  value="<?php echo ($rs_systemInfo["eight_star_team_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="eight_star_team_level" id="eight_star_team_level"  value="<?php echo ($rs_systemInfo["eight_star_team_level"]); ?>" required >星会员
								 &nbsp;&nbsp;
								首要审核级别第 <input type="text" class="form-control" name="eight_star_sh1_dai" id="eight_star_sh1_dai"  value="<?php echo ($rs_systemInfo["eight_star_sh1_dai"]); ?>" required >代 <input type="text" class="form-control" name="eight_star_sh1" id="eight_star_sh1"  value="<?php echo ($rs_systemInfo["eight_star_sh1"]); ?>" required >星&nbsp;&nbsp;
								次要审核级别第 <input type="text" class="form-control" name="eight_star_sh2_dai" id="eight_star_sh2_dai"  value="<?php echo ($rs_systemInfo["eight_star_sh2_dai"]); ?>" required >代 <input type="text" class="form-control" name="eight_star_sh2" id="eight_star_sh2"  value="<?php echo ($rs_systemInfo["eight_star_sh2"]); ?>" required >星
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">升级九星</label>
                                <div class="col-sm-10">
								需要直推 <input type="text" class="form-control" name="nine_star_ge" id="nine_star_ge"  value="<?php echo ($rs_systemInfo["nine_star_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="nine_star_level" id="nine_star_level"  value="<?php echo ($rs_systemInfo["nine_star_level"]); ?>" required >星会员且团队下有
								 <input type="text" class="form-control" name="nine_star_team_ge" id="nine_star_team_ge"  value="<?php echo ($rs_systemInfo["nine_star_team_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="nine_star_team_level" id="nine_star_team_level"  value="<?php echo ($rs_systemInfo["nine_star_team_level"]); ?>" required >星会员
								 &nbsp;&nbsp;
								首要审核级别第<input type="text" class="form-control" name="nine_star_sh1_dai" id="nine_star_sh1_dai"  value="<?php echo ($rs_systemInfo["nine_star_sh1_dai"]); ?>" required >代 <input type="text" class="form-control" name="nine_star_sh1" id="nine_star_sh1"  value="<?php echo ($rs_systemInfo["nine_star_sh1"]); ?>" required >星&nbsp;&nbsp;
								次要审核级别第<input type="text" class="form-control" name="nine_star_sh2_dai" id="nine_star_sh2_dai"  value="<?php echo ($rs_systemInfo["nine_star_sh2_dai"]); ?>" required >代 <input type="text" class="form-control" name="nine_star_sh2" id="nine_star_sh2"  value="<?php echo ($rs_systemInfo["nine_star_sh2"]); ?>" required >星
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">升级十星</label>
                               <div class="col-sm-10">
								需要直推 <input type="text" class="form-control" name="ten_star_ge" id="ten_star_ge"  value="<?php echo ($rs_systemInfo["ten_star_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="ten_star_level" id="ten_star_level"  value="<?php echo ($rs_systemInfo["ten_star_level"]); ?>" required >星会员且团队下有
								 <input type="text" class="form-control" name="ten_star_team_ge" id="ten_star_team_ge"  value="<?php echo ($rs_systemInfo["ten_star_team_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="ten_star_team_level" id="ten_star_team_level"  value="<?php echo ($rs_systemInfo["ten_star_team_level"]); ?>" required >星会员
								 &nbsp;&nbsp;
								首要审核级别第<input type="text" class="form-control" name="ten_star_sh1_dai" id="ten_star_sh1_dai"  value="<?php echo ($rs_systemInfo["ten_star_sh1_dai"]); ?>" required >代 <input type="text" class="form-control" name="ten_star_sh1" id="ten_star_sh1"  value="<?php echo ($rs_systemInfo["ten_star_sh1"]); ?>" required >星&nbsp;&nbsp;
								次要审核级别第<input type="text" class="form-control" name="ten_star_sh2_dai" id="ten_star_sh2_dai"  value="<?php echo ($rs_systemInfo["ten_star_sh2_dai"]); ?>" required >代 <input type="text" class="form-control" name="ten_star_sh2" id="ten_star_sh2"  value="<?php echo ($rs_systemInfo["ten_star_sh2"]); ?>" required >星
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">升级十一星</label>
                               <div class="col-sm-10">
								需要直推 <input type="text" class="form-control" name="eleven_star_ge" id="eleven_star_ge"  value="<?php echo ($rs_systemInfo["eleven_star_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="eleven_star_level" id="eleven_star_level"  value="<?php echo ($rs_systemInfo["eleven_star_level"]); ?>" required >星会员且团队下有
								 <input type="text" class="form-control" name="eleven_star_team_ge" id="eleven_star_team_ge"  value="<?php echo ($rs_systemInfo["eleven_star_team_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="eleven_star_team_level" id="eleven_star_team_level"  value="<?php echo ($rs_systemInfo["eleven_star_team_level"]); ?>" required >星会员
								 &nbsp;&nbsp;
								首要审核级别第<input type="text" class="form-control" name="eleven_star_sh1_dai" id="eleven_star_sh1_dai"  value="<?php echo ($rs_systemInfo["eleven_star_sh1_dai"]); ?>" required >代 <input type="text" class="form-control" name="eleven_star_sh1" id="eleven_star_sh1"  value="<?php echo ($rs_systemInfo["eleven_star_sh1"]); ?>" required >星&nbsp;&nbsp;
								次要审核级别第<input type="text" class="form-control" name="eleven_star_sh2_dai" id="eleven_star_sh2_dai"  value="<?php echo ($rs_systemInfo["eleven_star_sh2_dai"]); ?>" required >代 <input type="text" class="form-control" name="eleven_star_sh2" id="eleven_star_sh2"  value="<?php echo ($rs_systemInfo["eleven_star_sh2"]); ?>" required >星
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">升级十二星</label>
                               <div class="col-sm-10">
								需要直推 <input type="text" class="form-control" name="Twelve_star_ge" id="Twelve_star_ge"  value="<?php echo ($rs_systemInfo["Twelve_star_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="Twelve_star_level" id="Twelve_star_level"  value="<?php echo ($rs_systemInfo["Twelve_star_level"]); ?>" required >星会员且团队下有
								 <input type="text" class="form-control" name="Twelve_star_team_ge" id="Twelve_star_team_ge"  value="<?php echo ($rs_systemInfo["Twelve_star_team_ge"]); ?>" required >个
								 <input type="text" class="form-control" name="Twelve_star_team_level" id="Twelve_star_team_level"  value="<?php echo ($rs_systemInfo["Twelve_star_team_level"]); ?>" required >星会员
								 &nbsp;&nbsp;
								首要审核级别第<input type="text" class="form-control" name="Twelve_star_sh1_dai" id="Twelve_star_sh1_dai"  value="<?php echo ($rs_systemInfo["Twelve_star_sh1_dai"]); ?>" required >代 <input type="text" class="form-control" name="Twelve_star_sh1" id="Twelve_star_sh1"  value="<?php echo ($rs_systemInfo["Twelve_star_sh1"]); ?>" required >星&nbsp;&nbsp;
								次要审核级别第<input type="text" class="form-control" name="Twelve_star_sh2_dai" id="Twelve_star_sh2_dai"  value="<?php echo ($rs_systemInfo["Twelve_star_sh2_dai"]); ?>" required >代 <input type="text" class="form-control" name="Twelve_star_sh2" id="Twelve_star_sh2"  value="<?php echo ($rs_systemInfo["Twelve_star_sh2"]); ?>" required >星
                                   
                                </div>
                            </div>
							
						  
                            <div class="form-group">
                                <label class="col-sm-2 control-label">系统开关</label>
                                <div class="col-sm-10">
									<input type="radio" name="close_sys" value="0" id="sysset1" <?php if($rs_systemInfo["close_sys"] == 0): ?>checked<?php endif; ?>><label for="sysset1">开启</label>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="radio" name="close_sys" value="1" id="sysset2" <?php if($rs_systemInfo["close_sys"] == 1): ?>checked<?php endif; ?>><label for="sysset2">关闭</label>

                                </div>
                            </div>
                          
						  

                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">保存配置</button>
                                    <button class="btn btn-white" type="submit">取消</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
    <script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
    <script src="/Public/Theme1/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>
</body>

</html>