<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>发送站内信</title>
    <LINK href="../Admin_STYLE.CSS" rel=stylesheet type=text/css>


    <script src="/Public/Theme1/js/jquery.min.js"></script>
    <style type="text/css">

        <!--
        body {
            font-family: "微软雅黑", "宋体", "新宋体", Verdana, Geneva, sans-serif;
            font-size: 15px;
        }

        td {

            font-size: 12px;

            line-height: 22px;

        }

        -->
    </style>
</head>
<html>
<body>
<div id="toubu"><span class="tbbiaoti"><strong>发送站内信</strong></span></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="border">

    <tr class="tdbg">

        <td width="15"></td>

        <td>
            <div align="left">

                <table width="30%" height="22" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                        <td width="14%" height="25">
                            <div align="center"></div>
                        </td>

                        <td width="86%" height="25" valign="bottom" style="font-size:12px">&nbsp;</td>

                    </tr>

                </table>

            </div>
        </td>

        <td width="15"></td>

    </tr>

    <tr class="tdbg">

        <td>&nbsp;</td>

        <td height="150">
            <script language="javascript">

                function fmCheck() {
                    with (document.fmOrder) {
                        if (accept_username.value == "") {
                            alert("收件人不能为空！");
                            accept_username.focus();
                            accept_username.style.border = "1px solid #888888";
                            return false;
                        }
                        if (accept_title.value == "") {
                            alert("标题不能为空！");
                            accept_title.focus();
                            accept_title.style.border = "1px solid #888888";
                            return false;
                        }
                        if (accept_content.value == "") {
                            alert("内容不能为空！");
                            accept_content.focus();
                            accept_content.style.border = "1px solid #888888";
                            return false;
                        }

                    }
                }

                function checkUser_exist(user_name) {
                    var user_name = user_name;
                    $.ajax({
                        type: "POST",
                        url: "/SysAdmin/Message/checkUser_exist",
                        dataType: "json",
                        data: {"user_name": user_name},
                        success: function (msg) {
                            if (msg == '0') {
                                document.getElementById('tijiao').disabled = 'disabled';
                                $('#accept_username').focus().val('所有人');
                                alert('用户名不存在');
                            } else {
                                document.getElementById('tijiao').disabled = '';
                            }
                        }
                    })
                    if (user_name == '所有人') {
                        document.getElementById('tijiao').disabled = '';
                    }
                }
                function accept_all() {
                    document.getElementById('accept_username').value = '所有人';
                }
            </script>
            <form name="fmSend" action="/SysAdmin/Message/send_zxn" method="post" onSubmit="return fmCheck();">
                <input name="Action" type="hidden" id="Action" value="Modify"/>
                <table width="99%" border="0" align="center" cellpadding="3" cellspacing="0" class="tablebg2">
                    <tr>
                        <td width="150" height="50" align="right" class="tit1">收件人：</td>
                        <td>

                            <input name="accept_username" type="text" class="float_left" id="accept_username"
                            <?php if($user_name): ?>value='<?php echo ($user_name); ?>' readonly='readonly'<?php endif; ?>
                            size="50" onblur="checkUser_exist(this.value)"/>
                            <?php if($user_name): else: ?>
                                <input name="all_user" type="radio" onclick="accept_all()"/>发给所有人<?php endif; ?>

                        </td>
                    </tr>
                    <tr>
                        <td width="150" height="50" align="right" class="tit1">标题：</td>
                        <td>
                            <input name="accept_title" type="text" class="float_left" id="accept_title" size="50"/></td>
                    </tr>
                    <tr>
                        <td width="150" height="50" align="right" class="tit1">内容：</td>
                        <td><textarea name="accept_content" cols="50" rows="6" class="float_left"
                                      id="accept_content"></textarea></td>
                    </tr>
                    <tr>
                        <td height="25">&nbsp;</td>
                        <td align="left">
                            <input name="Submit2" type="submit" class="btn2" value="发送" id="tijiao"/>
                            <input name="Submit22" type="reset" class="btn2" value="取消"/>
                            <font color='red'></font></td>
                    </tr>
                </table>
            </form>
        <td>&nbsp;</td>

    </tr>


</table>


</body>
</html>