<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html style="background:none;">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>审核订单</title>

	
	
	 <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">

    <!-- Data Tables -->
    <link href="/Public/Theme1/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style.min.css?v=4.1.0" rel="stylesheet">
	<!-- Bootstrap Core JavaScript -->
	
	<style>
		.form-control, .single-line{display:inline-block;width:55%;}
		.panel-body a{color:##06cbc4;}
	</style>
	
</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>审核订单</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>

				
                    <div class="ibox-content">	
    <div class="panel" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
				
				<div class="ibox-content">
                <form method="post" action="/SysAdmin/User/orderlist" class="form-horizontal" id="form-admin-add" enctype="multipart/form-data">
					<div class="grid_box1">
						 <input type="text" name="search_loginname" id="search_loginname" class="form-control input-sm" placeholder="会员编号" style="width:10%"/>&nbsp;&nbsp;
                      <!--
						  <span>开始日期</span><input type="date" class="form-control1 search_loginname" name="from" > &nbsp;&nbsp;<span> 结束日期</span><input type="date" class="form-control1 search_loginname" name="until" >&nbsp;&nbsp;
						-->
						
						  <button class="btn-success btn">搜索</button>&nbsp;
					</div>
					
				</form>
				
				</div>
				
				

                    <div class="ibox-content">
                        <table class="table table-striped table-bordered table-hover dataTables-example">

                            <thead>
                                <th>订单ID</th>
                                <th>申请人</th>
                                <th>原级别</th>
                                <th>新级别</th>
                                <th>审核人一</th>
                                <th>审核状态</th>
                                <th>订单投诉</th>
                                <th>点赞投诉</th>
                                <th>点赞状态</th>
								<th>审核时间</th>
                                <th>审核人二</th>
                                <th>审核状态</th>
                                <th>订单投诉</th>
                                <th>点赞投诉</th>
                                <th>点赞状态</th>
								<th>审核时间</th>
								
                                <th>操作</th>
                            </thead>
                            <tbody>
                            <?php if(is_array($shList)): foreach($shList as $key=>$val_staffLists): ?><tr class="text-c">
                                <td ><?php echo ($val_staffLists['id']); ?></td>
								<td ><?php echo ($val_staffLists["loginname"]); ?></td>
								<td><?php echo ($val_staffLists["levelname"]); ?></td>
                                <td><?php echo ($val_staffLists["targetlevelname"]); ?></td>
                                    <td><?php echo ($val_staffLists["shuser1"]); ?></td>
                                    <td><?php echo ($val_staffLists["status1name"]); ?></td>
                                    <td>
										<?php if($val_staffLists[sh1ts]): ?><a href="/SysAdmin/User/tslists/orderid/<?php echo ($val_staffLists["id"]); ?>">有</a>
										<?php else: ?>
											无<?php endif; ?>
										</td>
                                    <td>
										<?php if($val_staffLists[sh1tsdz]): ?><a href="/SysAdmin/User/tslists/orderid/<?php echo ($val_staffLists["id"]); ?>">有</a>
										<?php else: ?>
											无<?php endif; ?></td>
                                    <td><?php echo ($val_staffLists["sh1dianzanname"]); ?></td>
                                    <td><?php echo ($val_staffLists["shtime1date"]); ?></td>
                                    <td><?php echo ($val_staffLists["shuser2"]); ?></td>
                                    <td><?php echo ($val_staffLists["status2name"]); ?></td>
                                    <td>
										<?php if($val_staffLists[sh2ts]): ?><a href="/SysAdmin/User/tslists/orderid/<?php echo ($val_staffLists["id"]); ?>">有</a>
										<?php else: ?>
											无<?php endif; ?></td>
                                    <td>
										<?php if($val_staffLists[sh2tsdz]): ?><a href="/SysAdmin/User/tslists/orderid/<?php echo ($val_staffLists["id"]); ?>">有</a>
										<?php else: ?>
											无<?php endif; ?>
									</td>
                                    <td>
									<?php echo ($val_staffLists["sh1dianzanname"]); ?></td>
                                    <td><?php echo ($val_staffLists["shtime2date"]); ?></td>
							
								<td>
								<?php if($val_staffLists['status1'] != 2 || $val_staffLists['status2'] != 2): ?><a title="升级会员" href="/SysAdmin/user/shengjishenhe/uid/<?php echo ($val_staffLists['id']); ?>">通过</a><?php endif; ?>
								&nbsp;&nbsp;&nbsp;<a title="升级会员" href="/SysAdmin/user/shenjidel/uid/<?php echo ($val_staffLists['id']); ?>">删除</a>
								</td>

								</tr><?php endforeach; endif; ?>

							
                            </tbody>
                            
                        </table>
					<div class="result page"><?php echo ($page); ?></div>
                    </div>
					
	</div>
	</div>
	
                </div>
            </div>
        </div>
    </div>

    <script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
    <script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="/Public/Theme1/js/plugins/jeditable/jquery.jeditable.js"></script>
    <script src="/Public/Theme1/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="/Public/Theme1/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
    <script>
	
		function fnClickAddRow() {
			$("#editable").dataTable().fnAddData(["Custom row", "New row", "New row", "New row", "New row"])
		};
    </script>
</body>



</html>