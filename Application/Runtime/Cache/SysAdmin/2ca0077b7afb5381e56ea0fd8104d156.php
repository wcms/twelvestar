<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>系统参数设置</title>
    <link rel="shortcut icon" href="favicon.ico"> <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/Public/Theme1/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style.min.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>系统配置参数 <small></small></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="form_basic.html#">
                                <i class="fa fa-wrench"></i>
                            </a>

                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="/SysAdmin/System/SystemAction" class="form-horizontal" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">系统名称</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="sName" id="sName" placeholder="控制在25个字、50个字节以内" value="<?php echo ($rs_systemInfo["sName"]); ?>" required >
                                </div>
                            </div>


                                <div class="form-group">
                                <label class="col-sm-2 control-label">登录界面名字</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="sCompany" id="sCompany" placeholder="登录界面名字" value="<?php echo ($rs_systemInfo["sCompany"]); ?>" class="form-control" required>
                                    </div>
                                </div>

								
                                <div class="form-group">
                                <label class="col-sm-2 control-label">联系客服</label>
                                    <div class="col-sm-10">
                                        <input type="tel" name="sCompanyName" id="sCompanyName" placeholder="客服姓名" value="<?php echo ($rs_systemInfo["sCompanyName"]); ?>" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                <label class="col-sm-2 control-label">工作时间</label>
                                    <div class="col-sm-10">
                                        <input type="tel" name="sCompanyTel" id="sCompanyTel" placeholder="客服电话" value="<?php echo ($rs_systemInfo["sCompanyTel"]); ?>" class="form-control" required>
                                    </div>
                                </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">推荐链接开关</label>

                                <div class="col-sm-10">
                                <?php if($rs_systemInfo["tjcheckcode"] == 1): ?><label class="checkbox-inline">

                                        <input type="radio" value="1" name="tjcheckcode" checked> 开启</label>
                                    <label class="checkbox-inline">
                                        <input type="radio" value="0" name="tjcheckcode"> 关闭</label>
                                        <?php else: ?>
                                        <label class="checkbox-inline">

                                        <input type="radio" value="1" name="tjcheckcode" > 开启</label>
                                    <label class="checkbox-inline">
                                        <input type="radio" value="0" name="tjcheckcode" checked> 关闭</label><?php endif; ?>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">注册短信开关</label>

                                <div class="col-sm-10">
                                <?php if($rs_systemInfo["sRegUserCheckCode"] == 1): ?><label class="checkbox-inline">

                                        <input type="radio" value="1" name="sRegUserCheckCode" checked> 开启</label>
                                    <label class="checkbox-inline">
                                        <input type="radio" value="0" name="sRegUserCheckCode"> 关闭</label>
                                        <?php else: ?>
                                        <label class="checkbox-inline">

                                        <input type="radio" value="1" name="sRegUserCheckCode" > 开启</label>
                                    <label class="checkbox-inline">
                                        <input type="radio" value="0" name="sRegUserCheckCode" checked> 关闭</label><?php endif; ?>

                                </div>
                            </div>
								
                            <div class="form-group">
                                <label class="col-sm-2 control-label">幻灯片开关</label>

                                <div class="col-sm-10">
                                <?php if($rs_systemInfo["sCheckCodeSwitch1"] == 1): ?><label class="checkbox-inline">

                                        <input type="radio" value="1" name="sCheckCodeSwitch1" checked> 开启</label>
                                    <label class="checkbox-inline">
                                        <input type="radio" value="0" name="sCheckCodeSwitch1"> 关闭</label>
                                        <?php else: ?>
                                        <label class="checkbox-inline">

                                        <input type="radio" value="1" name="sCheckCodeSwitch1" > 开启</label>
                                    <label class="checkbox-inline">
                                        <input type="radio" value="0" name="sCheckCodeSwitch1" checked> 关闭</label><?php endif; ?>

                                </div>
                            </div>
							
                                <div class="form-group">
                                <label class="col-sm-2 control-label">幻灯片1</label>
                                    <div class="col-sm-10">
                                    <img src="/uploads/lunboimg/<?php echo ($rs_systemInfo["hdp1"]); ?>" width="300" height="100">

                                        <input style="margin-top: 8px;" type="file" name="hdp1" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                <label class="col-sm-2 control-label">幻灯片1URL</label>
                                    <div class="col-sm-10">
                                    

                                        <input style="margin-top: 8px;" type="text" name="hdp1url" value="<?php echo ($rs_systemInfo["hdp1url"]); ?>" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                <label class="col-sm-2 control-label">幻灯片2</label>
                                    <div class="col-sm-10">
                                    <img src="/uploads/lunboimg/<?php echo ($rs_systemInfo["hdp2"]); ?>" width="300" height="100">

                                        <input style="margin-top: 8px;" type="file" name="hdp2" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                <label class="col-sm-2 control-label">幻灯片2URL</label>
                                    <div class="col-sm-10">
                                    

                                        <input style="margin-top: 8px;" type="text" name="hdp2url" value="<?php echo ($rs_systemInfo["hdp2url"]); ?>" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                <label class="col-sm-2 control-label">幻灯片3</label>
                                    <div class="col-sm-10">
                                    <img src="/uploads/lunboimg/<?php echo ($rs_systemInfo["hdp3"]); ?>" width="300" height="100">

                                        <input style="margin-top: 8px;" type="file" name="hdp3" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                <label class="col-sm-2 control-label">幻灯片3URL</label>
                                    <div class="col-sm-10">
                                    

                                        <input style="margin-top: 8px;" type="text" name="hdp3url" value="<?php echo ($rs_systemInfo["hdp3url"]); ?>" class="form-control" >
                                    </div>
                                </div>
								
                                <div class="form-group">
                                <label class="col-sm-2 control-label">站点LOGO</label>
                                    <div class="col-sm-10">
                                    <img src="/uploads/lunboimg/<?php echo ($rs_systemInfo["slogo"]); ?>" width="150" height="150">

                                        <input style="margin-top: 8px;" type="file" name="sLogo" class="form-control" >
                                    </div>
                                </div>


                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">保存配置</button>
                                    <button class="btn btn-white" type="submit">取消</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
    <script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
    <script src="/Public/Theme1/js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>
</body>

</html>