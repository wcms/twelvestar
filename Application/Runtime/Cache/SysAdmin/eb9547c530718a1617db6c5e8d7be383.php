<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>站内信管理</title>

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">

    <link href="/Public/Theme1/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style.min.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>产品列表</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <script type="text/javascript">
                    function CheckAll(val) {
                        $("input[name='node[]']").each(function () {
                            this.checked = val;
                        });
                    }
                </script>


                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example">

                        <thead>
                        <tr>
                            <th width="100px">产品id</th>
                            <th>产品名称</th>
                            <th>产品价格</th>
                             <th>产品图片</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(is_array($news_list)): foreach($news_list as $key=>$item): ?><tr class="text-c">
                                <td><?php echo ($item["goods_id"]); ?></td>
                                <td><?php echo ($item["goods_name"]); ?></td>
                                <td><?php echo ($item["goods_price"]); ?></td>
                                <td><img src="<?php echo ($item["goods_img"]); ?>" width="100px;" height="100px;"></td>
                                
                                <td>
                                    
									
                                    <a href="/SysAdmin/Goods/goods_update/id/<?php echo ($item["goods_id"]); ?>">修改</a>
                                      <a href="/SysAdmin/Goods/goods_del/id/<?php echo ($item["goods_id"]); ?>">删除</a>
                            </tr><?php endforeach; endif; ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/Public/Theme1/js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="/Public/Theme1/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="/Public/Theme1/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
<script>
    $(document).ready(function () {
        $(".dataTables-example").dataTable();
        var oTable = $("#editable").dataTable();
        oTable.$("td").editable("../example_ajax.php", {
            "callback": function (sValue, y) {
                var aPos = oTable.fnGetPosition(this);
                oTable.fnUpdate(sValue, aPos[0], aPos[1])
            },

            "width": "90%",
            "height": "100%"
        })
    });
    function fnClickAddRow() {
        $("#editable").dataTable().fnAddData(["Custom row", "New row", "New row", "New row", "New row"])
    }
    ;
    function del(){

        var msg=confirm("你确定删除此记录吗？");

        if(msg==true){

            return true;

        }else{

            return false;

        }

    }
</script>
</body>

</html>