<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>会员列表</title>

    <link rel="shortcut icon" href="favicon.ico">
    <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">

    <!-- Data Tables -->
    <link href="/Public/Theme1/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style.min.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>会员列表 <!-- <a href="/SysAdmin/User/add" style="margin-left:15px; color:#06cbc4">添加会员</a>--></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>

                <form method="post" action="/SysAdmin/User/DelAll" class="form-horizontal" id="form-admin-add">
                    <script type="text/javascript">
                        function CheckAll(val) {
                            $("input[name='node[]']").each(function () {
                                this.checked = val;
                            });
                        }
                    </script>


                    <div class="ibox-content">
                        <table class="table table-striped table-bordered table-hover dataTables-example">

                            <thead>
                            <tr>

                                <th>会员编号</th>
                                <th>级别</th>
                                <th>真实姓名</th>
                                <th>注册时间</th>
                                <th>推荐码</th>
                                <th>手机号</th>
                                <th>推荐人</th>
                                <th>直推</th>
                                <th>团队人数</th>
                                <th>账号状态</th>
                                <th>二维码</th>
                               
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(is_array($rs_staffLists)): foreach($rs_staffLists as $key=>$val_staffLists): ?><tr>
                                    <td><?php echo ($val_staffLists["id"]); ?></td>
                                    <td><?php echo GetLevel($val_staffLists['standardlevel']) ?></td>
                                    <td><?php echo ($val_staffLists["truename"]); ?></td>
                                    <td><?php echo (date('Y-m-d H:i',$val_staffLists["addtime"])); ?></td>
									
                                    <td><?php echo ($val_staffLists["tuijianma"]); ?></td>
                                    <td><?php echo ($val_staffLists["loginname"]); ?></td>
                                    <td><?php echo ($val_staffLists["rname"]); ?></td>
                                    
                                    <td><?php echo ($val_staffLists["invite_count"]); ?></td>
                                    <td><?php echo ($val_staffLists["team_count"]); ?></td>
                                    <td><?php echo ($val_staffLists['lockuser'] == 1 ? "冻结" : "正常"); ?></td>
                                    <td><a href="/uploads/<?php echo ($val_staffLists["wximg"]); ?>" target="_blank"><img src="/uploads/<?php echo ($val_staffLists["wximg"]); ?>" width=80 height=80 /></a></td>
                                  
                                    <td>
                                        <div>
										 <a
                                                href="/SysAdmin/User/GoIndex/stId/<?php echo ($val_staffLists["id"]); ?>" target="_blank">去前台</a>
												&nbsp;&nbsp; <a
                                                href="/SysAdmin/User/update/stId/<?php echo ($val_staffLists["id"]); ?>">修改</a>
												<?php if($val_staffLists[id] != 1): ?><a
                                                href="Del/stId/<?php echo ($val_staffLists["id"]); ?>">删除</a><?php endif; ?>
												<?php if($val_staffLists[lockuser] == 1): ?><a
                                                href="dongjie/status/2/id/<?php echo ($val_staffLists["id"]); ?>">解冻</a>
												<?php else: ?>
												<a
                                                href="dongjie/status/1/id/<?php echo ($val_staffLists["id"]); ?>">冻结</a><?php endif; ?>
                                        </div>
                                        <!--<a title="删除" href="/SysAdmin/User/DelAction/stId/<?php echo ($val_staffLists["id"]); ?>" ><i class="glyphicon glyphicon-remove"></i></a>-->

                                    </td>

                                </tr><?php endforeach; endif; ?>
                            </tbody>

                        </table>
                        <!-- <input type='checkbox' id='chkAll' onclick="CheckAll(this.checked)"> <span style="margin-right: 10px;color: #2c86da; font-size: 12px; font-weight: bold">全 选</span>
                        <input class="btn btn-success btn-xs" type="submit" value="删除" > -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/Public/Theme1/js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="/Public/Theme1/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="/Public/Theme1/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
<script>
    $(document).ready(function () {
        $(".dataTables-example").dataTable();
        var oTable = $("#editable").dataTable();
        oTable.$("td").editable("../example_ajax.php", {
            "callback": function (sValue, y) {
                var aPos = oTable.fnGetPosition(this);
                oTable.fnUpdate(sValue, aPos[0], aPos[1])
            },

            "width": "90%",
            "height": "100%"
        })
    });
    function fnClickAddRow() {
        $("#editable").dataTable().fnAddData(["Custom row", "New row", "New row", "New row", "New row"])
    }
    ;
</script>
</body>

</html>