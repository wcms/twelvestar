<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html style="background:none;">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>会员推荐关系图</title>

	
	
	 <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">

    <!-- Data Tables -->
    <link href="/Public/Theme1/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style.min.css?v=4.1.0" rel="stylesheet">
	<!-- Bootstrap Core JavaScript -->
	<script src="/Public/Theme1/tree/dtree.css"></script>
	<script src="/Public/Theme1/tree/dtree.js"></script>
	
	<style>
		.form-control, .single-line{display:inline-block;width:55%;}
		.panel-body a{color:##06cbc4;}
	</style>
	
</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>会员关系图 <a href="/SysAdmin/User/lists" style="margin-left:15px; color:#06cbc4">会员列表</a></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>

				
                    <div class="ibox-content">	
    <div class="panel" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
				
				<div class="row">
                <form method="post" action="/SysAdmin/User/affectlists" class="form-horizontal" id="form-admin-add" enctype="multipart/form-data">
					<div class="col-md-3 grid_box1">
						 <input type="text" name="search_loginname" id="search_loginname" class="form-control input-sm" placeholder="用户编号"/>
						 <button class="btn-success btn">搜索</button>&nbsp;
						 <a href="/SysAdmin/User/affectlists" class="btn-success btn">全部</a>
					</div>
				</form>
				</div>
				
				<div class="panel-body no-padding">
				<div class="panel-content">
				
				<div class="dtree col-md-12 grid_box1">

				<p style="padding:5px 0;"><a href="javascript: d.openAll();">展开全部</a> | <a href="javascript: d.closeAll();">关闭全部</a>
					
				</p>

				<script type="text/javascript">
					d = new dTree('d');
					
					<?php
 function getcatelog($tarray){ foreach($tarray as $k=>$v){ if($v['name']!=$top_info['loginname']) { echo "d.add('".$v['name']."','".$v['rid']."','".$v['name']."(".$v['level'].")"."(".$v['childrencount'].") ','','','','/Public/Theme1/ico/index/dwuser.gif','/Public/Theme1/ico/index/dwuser.gif');"; } getcatelog($v['children']); } } ?>
					
					d.add('<?php echo $top_info['loginname']?>',-1,'<?php echo $top_info['loginname']."(".$top_info['level'].")"."({$top_info[childrencount]})"?>','','','','images/dwuser.gif');
				
					<?php
 getcatelog($a_lists); ?>	
					
					document.write(d);

					d.closeAll();
				</script>
				</div>
				</div>
				
				
				</div>
	</div>
	</div>
	
                </div>
            </div>
        </div>
    </div>

    <script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
    <script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="/Public/Theme1/js/plugins/jeditable/jquery.jeditable.js"></script>
    <script src="/Public/Theme1/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="/Public/Theme1/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
    <script>
        $(document).ready(function() {
			$(".dataTables-example").dataTable();
			var oTable = $("#editable").dataTable();
			oTable.$("td").editable("../example_ajax.php", {
				"callback": function(sValue, y) {
					var aPos = oTable.fnGetPosition(this);
					oTable.fnUpdate(sValue, aPos[0], aPos[1])
				},
				
				"width": "90%",
				"height": "100%"
			})
		});
		function fnClickAddRow() {
			$("#editable").dataTable().fnAddData(["Custom row", "New row", "New row", "New row", "New row"])
		};
    </script>
</body>



</html>