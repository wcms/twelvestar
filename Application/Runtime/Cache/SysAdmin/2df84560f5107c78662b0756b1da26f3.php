<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html style="background:none;">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>投诉列表</title>

	
	
	 <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">

    <!-- Data Tables -->
    <link href="/Public/Theme1/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style.min.css?v=4.1.0" rel="stylesheet">
	<!-- Bootstrap Core JavaScript -->
	
	<style>
		.form-control, .single-line{display:inline-block;width:55%;}
		.panel-body a{color:##06cbc4;}
	</style>
	
</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>投诉列表</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>

				
                    <div class="ibox-content">	
    <div class="panel" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
				
				<div class="ibox-content">
                <form method="post" action="/SysAdmin/User/tslists" class="form-horizontal" id="form-admin-add" enctype="multipart/form-data">
					<div class="grid_box1">
						 <input type="text" name="search_loginname" id="search_loginname" class="form-control input-sm" placeholder="投诉者电话" style="width:10%"/>&nbsp;&nbsp;
						 <input type="text" name="search_loginname1" id="search_loginname1" class="form-control input-sm" placeholder="被投诉者电话" style="width:10%"/>&nbsp;&nbsp;
                      <!--
						  <span>开始日期</span><input type="date" class="form-control1 search_loginname" name="from" > &nbsp;&nbsp;<span> 结束日期</span><input type="date" class="form-control1 search_loginname" name="until" >&nbsp;&nbsp;
						-->
						
						  <button class="btn-success btn">搜索</button>&nbsp;
					</div>
					
				</form>
				
				</div>
				
				

                    <div class="ibox-content">
                        <table class="table table-striped table-bordered table-hover dataTables-example">

                            <thead>
                                <th>ID</th>
                                <th>类型</th>
                                <th>订单ID</th>
                                <th>投诉者电话</th>
                                <th>投诉者姓名</th>
                                <th>被投诉者电话</th>
                                <th>被投诉者姓名</th>
                                <th>内容</th>
                                <th>时间</th>
								
								
                                <th>操作</th>
                            </thead>
                            <tbody>
                            <?php if(is_array($shList)): foreach($shList as $key=>$val_staffLists): ?><tr class="text-c">
                                <td ><?php echo ($val_staffLists['id']); ?></td>
                                <td ><?php echo ($val_staffLists["tstypename"]); ?></td>
                                <td ><?php echo ($val_staffLists["tsorderid"]); ?></td>
								<td ><?php echo ($val_staffLists["tsuser"]); ?></td>
								<td ><?php echo ($val_staffLists["tsusername"]); ?></td>
								<td ><?php echo ($val_staffLists["tsuserobj"]); ?></td>
								<td ><?php echo ($val_staffLists["tsuserobjname"]); ?></td>
								<td ><?php echo ($val_staffLists["content"]); ?></td>
								<td><?php echo ($val_staffLists["addtime"]); ?></td>
								
							
								<td>
								<a title="删除" href="/SysAdmin/user/tousudel/uid/<?php echo ($val_staffLists['id']); ?>">删除</a>
								</td>

								</tr><?php endforeach; endif; ?>

							
                            </tbody>
                            
                        </table>
					<div class="result page"><?php echo ($page); ?></div>
                    </div>
					
	</div>
	</div>
	
                </div>
            </div>
        </div>
    </div>

    <script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
    <script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="/Public/Theme1/js/plugins/jeditable/jquery.jeditable.js"></script>
    <script src="/Public/Theme1/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="/Public/Theme1/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
    <script>
	
		function fnClickAddRow() {
			$("#editable").dataTable().fnAddData(["Custom row", "New row", "New row", "New row", "New row"])
		};
    </script>
</body>



</html>