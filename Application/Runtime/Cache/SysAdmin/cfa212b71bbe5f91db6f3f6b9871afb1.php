<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>添加管理员</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/Public/Theme1/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/Public/Theme1/static/plupload/upfiless.css" rel="stylesheet" type="text/css"/>
    <link href="/Public/Theme1/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet" type="text/css"/>
    <link href="/Public/Theme1/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"
          rel="stylesheet">
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>添加管理员</h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" action="" class="form-horizontal" id="form-admin-add"
                          enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-sm-1 control-label">管理账号</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="aUser" id="aUser"
                                       style="width:65%;" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 control-label">登陆密码</label>
                            <div class="col-sm-3">
                                <input type="password" name="aPwd" id="aPwd" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 control-label">再次确认</label>
                            <div class="col-sm-3">
                                <input type="password" name="aPwd_2" id="aPwd_2" class="form-control" required>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">添加管理员</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
<script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
<script src="/Public/Theme1/js/plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript" src="/Public/Theme1/check/js/jquery.validate.min.js"></script>

<script type="text/javascript" src="/Public/Theme1/check/js/messages_zh.min.js"></script>

<script type="text/javascript" src="/Public/Theme1/check/js/validate-methods.js"></script>
<script type="text/javascript" src="/Public/Theme1/lib/webuploader/0.1.5/webuploader.min.js"></script>

<script>
    $(document).ready(function () {
        $(".i-checks").iCheck({checkboxClass: "icheckbox_square-green", radioClass: "iradio_square-green",});
    });
</script>

</body>

</html>