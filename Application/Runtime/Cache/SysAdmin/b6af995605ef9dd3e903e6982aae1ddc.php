<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>系统后台</title>

    <link rel="shortcut icon" href="favicon.ico"> <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">

    <!-- Morris -->
    <link href="/Public/Theme1/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="/Public/Theme1/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style.min.css?v=4.1.0" rel="stylesheet">

</head>

<body class="gray-bg" style="background-image:url(/Public/Theme1/img/bg.jpg)">
    <div class="wrapper wrapper-content">


            <div class="col-sm-3">
                <div class="widget style1 yellow-bg">
                    <div class="row">
                        <div class="col-xs-4" style="height: 70px">
                            <i><img src="/Public/Theme1/ico/index/staffinfo.png"></i>
                        </div>
                        <div class="col-xs-6">
                            <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" href="/SysAdmin/User/lists">用户总数&nbsp;<?php echo ($people_count); ?>&nbsp;人</a></strong></span>

                        </div>
                    </div>
                </div>
            </div>
             <div class="col-sm-3">
                <div class="widget style1 lazur-bg">
                    <div class="row">
                        <div class="col-xs-4"  style="height: 70px">
                            <i><img src="/Public/Theme1/ico/index/staffadd.png"></i>
                        </div>
                        <div class="">
                            <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" >今日注册会员&nbsp;<?php echo ($todayuser); ?>&nbsp;人</a></strong></span>

                        </div>
                    </div>
                </div>
            </div>


            <div class="col-sm-3">
                <div class="widget style1 blue-bg">
                    <div class="row">
                        <div class="col-xs-4" style="height: 70px">
                            <i><img src="/Public/Theme1/ico/index/setsystem.png"></i>
                        </div>
                        <div class="">
                            <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" >审核总数&nbsp;<?php echo ($passuser); ?>&nbsp;人</a></strong></span>

                        </div>
                    </div>
                </div>
            </div>

          
		  
        <div class="col-sm-3">
            <div class="widget style1 " style="background-color:#CFC006; color:#ffffff">
                <div class="row">
                    <div class="col-xs-4" style="height: 70px">
                        <i><img src="/Public/Theme1/ico/index/register.png"></i>
                    </div>
                    <div class="">
                        <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" >普通会员&nbsp;<?php echo ($reg_member); ?>个</a></strong></span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="widget style1 " style="background-color:#7B3FF2; color:#ffffff">
                <div class="row">
                    <div class="col-xs-4" style="height: 70px">
                        <i><img src="/Public/Theme1/ico/index/1.png"></i>
                    </div>
                    <div class="">
                        <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" href="javascript:;">一星会员&nbsp;<?php echo ($vip_member); ?>个</a></strong></span>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="widget style1 " style="background-color:#92DCC9; color:#ffffff">
                <div class="row">
                    <div class="col-xs-4" style="height: 70px">
                        <i><img src="/Public/Theme1/ico/index/2.png"></i>
                    </div>
                    <div class="">
                        <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" href="javascript:;">二星会员&nbsp;<?php echo ($one_star); ?>个</a></strong></span>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="widget style1 " style="background-color:#58BAE2; color:#ffffff">
                <div class="row">
                    <div class="col-xs-4" style="height: 70px">
                        <i><img src="/Public/Theme1/ico/index/3.png"></i>
                    </div>
                    <div class="">
                        <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" href="javascript:;">三星会员&nbsp;<?php echo ($two_star); ?>个</a></strong></span>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="widget style1 " style="background-color:#A18172; color:#ffffff">
                <div class="row">
                    <div class="col-xs-4" style="height: 70px">
                        <i><img src="/Public/Theme1/ico/index/4.png"></i>
                    </div>
                    <div class="">
                        <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" href="javascript:;">四星会员&nbsp;<?php echo ($three_star); ?>个</a></strong></span>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="widget style1 " style="background-color:#21B416; color:#ffffff">
                <div class="row">
                    <div class="col-xs-4" style="height: 70px">
                        <i><img src="/Public/Theme1/ico/index/5.png"></i>
                    </div>
                    <div class="">
                        <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" href="javascript:;">五星会员&nbsp;<?php echo ($four_star); ?>个</a></strong></span>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="widget style1 " style="background-color:#A564DB; color:#ffffff">
                <div class="row">
                    <div class="col-xs-4" style="height: 70px">
                        <i><img src="/Public/Theme1/ico/index/6.png"></i>
                    </div>
                    <div class="">
                        <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" href="javascript:;">六星会员&nbsp;<?php echo ($five_star); ?>个</a></strong></span>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="widget style1 " style="background-color:#658278; color:#ffffff">
                <div class="row">
                    <div class="col-xs-4" style="height: 70px">
                        <i><img src="/Public/Theme1/ico/index/7.png"></i>
                    </div>
                    <div class="">
                        <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" href="javascript:;">七星会员&nbsp;<?php echo ($six_star); ?>个</a></strong></span>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="widget style1 " style="background-color:#13A1FF; color:#ffffff">
                <div class="row">
                    <div class="col-xs-4" style="height: 70px">
                        <i><img src="/Public/Theme1/ico/index/8.png"></i>
                    </div>
                    <div class="">
                        <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" href="javascript:;">八星会员&nbsp;<?php echo ($seven_star); ?>个</a></strong></span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="widget style1 " style="background-color:#13A1FF; color:#ffffff">
                <div class="row">
                    <div class="col-xs-4" style="height: 70px">
                        <i><img src="/Public/Theme1/ico/index/9.png"></i>
                    </div>
                    <div class="">
                        <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" href="javascript:;">九星会员&nbsp;<?php echo ($eight_star); ?>个</a></strong></span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="widget style1 " style="background-color:#13A1FF; color:#ffffff">
                <div class="row">
                    <div class="col-xs-4" style="height: 70px">
                        <i><img src="/Public/Theme1/ico/index/10.png"></i>
                    </div>
                    <div class="">
                        <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" href="javascript:;">十星会员&nbsp;<?php echo ($ten_star); ?>个</a></strong></span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="widget style1 " style="background-color:#13A1FF; color:#ffffff">
                <div class="row">
                    <div class="col-xs-4" style="height: 70px">
                        <i><img src="/Public/Theme1/ico/index/11.png"></i>
                    </div>
                    <div class="">
                        <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" href="javascript:;">十一星会员&nbsp;<?php echo ($elve_star); ?>个</a></strong></span>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="widget style1 " style="background-color:#13A1FF; color:#ffffff">
                <div class="row">
                    <div class="col-xs-4" style="height: 70px">
                        <i><img src="/Public/Theme1/ico/index/12.png"></i>
                    </div>
                    <div class="">
                        <span><strong style="font-size:17px; line-height: 70px;padding-left: -15px"><a style="color: #ffffff" href="javascript:;">十二星会员&nbsp;<?php echo ($tenv_star); ?>个</a></strong></span>

                    </div>
                </div>
            </div>
        </div>

    <link href="/Public/Theme2/css/welcome.min.css?v=4.1.0" rel="stylesheet">

		<div class="list-div">
		
</div>



    </div>


    <script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
    <script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="/Public/Theme1/js/plugins/flot/jquery.flot.js"></script>
    <script src="/Public/Theme1/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="/Public/Theme1/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="/Public/Theme1/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="/Public/Theme1/js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="/Public/Theme1/js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="/Public/Theme1/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="/Public/Theme1/js/demo/peity-demo.min.js"></script>
    <script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
    <script src="/Public/Theme1/js/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="/Public/Theme1/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/Public/Theme1/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="/Public/Theme1/js/plugins/easypiechart/jquery.easypiechart.js"></script>
    <script src="/Public/Theme1/js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="/Public/Theme1/js/demo/sparkline-demo.min.js"></script>
</body>

</html>