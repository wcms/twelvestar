<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>修改会员信息</title>
    <link rel="shortcut icon" href="favicon.ico"> <link href="/Public/Theme1/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/Public/Theme1/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/Public/Theme1/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/Public/Theme1/css/animate.min.css" rel="stylesheet">
    <link href="/Public/Theme1/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="/Public/Theme1/static/plupload/upfiless.css" rel="stylesheet" type="text/css" />
    <link href="/Public/Theme1/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet" type="text/css" />
    <link href="/Public/Theme1/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

</head>

<body class="gray-bg">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>修改会员信息 <a style="margin-left: 15px; color:#06cbc4" href="/SysAdmin/User/lists">返回列表</a></h5>
                        <div class="ibox-tools">

                        </div>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="/SysAdmin/User/UpdateAction/stId/<?php echo ($rs_staffInfo["id"]); ?>" class="form-horizontal" id="form-admin-add" enctype="multipart/form-data" >

                           
                            <div class="form-group">
                                <label class="col-sm-1 control-label">手机号码</label>
                                <div class="col-sm-3">
                                    <input type="text" name="mobile" id="mobile" class="form-control" value="<?php echo ($rs_staffInfo["tel"]); ?>" />
                                </div>

                            </div>
						   
                            <div class="form-group">
                                <label class="col-sm-1 control-label">会员姓名</label>
                                <div class="col-sm-3">
                                    <input type="text" name="truename" id="txt_truename" class="form-control" value="<?php echo ($rs_staffInfo["truename"]); ?>" />
                                </div>

                            </div>
						   
                            <div class="form-group">
                                <label class="col-sm-1 control-label">级别</label>
                                <div class="col-sm-3">
                                    
									<select name="standardlevel">
										<option value="0" <?php if($rs_staffInfo[standardlevel] == 0): ?>selected<?php endif; ?> >普通会员</option>
										<option value="1" <?php if($rs_staffInfo[standardlevel] == 1): ?>selected<?php endif; ?> >一星会员</option>
										<option value="2" <?php if($rs_staffInfo[standardlevel] == 2): ?>selected<?php endif; ?> >二星会员</option>
										<option value="3" <?php if($rs_staffInfo[standardlevel] == 3): ?>selected<?php endif; ?> >三星会员</option>
										<option value="4" <?php if($rs_staffInfo[standardlevel] == 4): ?>selected<?php endif; ?> >四星会员</option>
										<option value="5" <?php if($rs_staffInfo[standardlevel] == 5): ?>selected<?php endif; ?> >五星会员</option>
										<option value="6" <?php if($rs_staffInfo[standardlevel] == 6): ?>selected<?php endif; ?> >六星会员</option>
										<option value="7" <?php if($rs_staffInfo[standardlevel] == 7): ?>selected<?php endif; ?> >七星会员</option>
										<option value="8" <?php if($rs_staffInfo[standardlevel] == 8): ?>selected<?php endif; ?> >八星会员</option>
										<option value="9" <?php if($rs_staffInfo[standardlevel] == 9): ?>selected<?php endif; ?> >九星会员</option>
										<option value="10" <?php if($rs_staffInfo[standardlevel] == 10): ?>selected<?php endif; ?> >十星会员</option>
										<option value="11" <?php if($rs_staffInfo[standardlevel] == 11): ?>selected<?php endif; ?> >十一星会员</option>
										<option value="12" <?php if($rs_staffInfo[standardlevel] == 12): ?>selected<?php endif; ?> >十二星会员</option>
										
									</select>
									
                                </div>

                            </div>
						   
						   
                            <div class="form-group">
                                <label class="col-sm-1 control-label">微信二维码</label>
                                <div class="col-sm-3">
                                   <input type="file" name="wximg" />
								   <?php if($rs_staffInfo[wximg]): ?><a href="/uploads/<?php echo ($rs_staffInfo["wximg"]); ?>" target="_blank"><img src="/uploads/<?php echo ($rs_staffInfo["wximg"]); ?>" width=80 height=80/></a><?php endif; ?>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-1 control-label">密码</label>
                                <div class="col-sm-3">
                                    <input type="text" name="txt_pwd1" id="txt_pwd1" class="form-control" placeholder=" 不修改可留空"/>
                                </div>

                            </div>
							

                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">修改会员信息</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/Public/Theme1/js/jquery.min.js?v=2.1.4"></script>
    <script src="/Public/Theme1/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="/Public/Theme1/js/content.min.js?v=1.0.0"></script>
    <script src="/Public/Theme1/js/plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript" src="/Public/Theme1/check/js/jquery.validate.min.js"></script>

<script type="text/javascript" src="/Public/Theme1/check/js/messages_zh.min.js"></script>

<script type="text/javascript" src="/Public/Theme1/check/js/validate-methods.js"></script>
<script type="text/javascript" src="/Public/Theme1/lib/webuploader/0.1.5/webuploader.min.js"></script>

    <script>
        $(document).ready(function(){$(".i-checks").iCheck({checkboxClass:"icheckbox_square-green",radioClass:"iradio_square-green",})});
    </script>


    <script type="text/javascript">

	function shoujihaoma(thisvalue){

			var reg = /^\d{9,}$/;

		 if(!reg.test(thisvalue)){
			$("#txt_tel").addClass("error");
			$("#txt_tel").after('<label id="txt_tel-error" class="error" for="txt_tel" style="display: inline-block;">格式不正确</label>');

			return false;
		 }
		 else{
			$("#txt_tel-error").remove();
			$("#txt_tel").removeClass("error");
			return true;
		 }

	}

//	function is_tuijian(thisvalue)
//	{
//
//		if(thisvalue.length>0){
//			$("#is_tuijian-error").remove();
//
//		htmlobj=$.ajax({url:"/SysAdmin/User/CheckTuijian/username/" + escape(thisvalue),async:false});
//
//		if (escape(htmlobj.responseText)=="0"){
//
//			$("#txt_rid").addClass("error");
//			$("#txt_rid").after('<label id="is_tuijian-error" class="error" for="txt_rid" style="display: inline-block;">推荐人不存在</label>');
//
//			return false;
//		}
//		if (escape(htmlobj.responseText)=="1"){
//			$("#txt_rid").removeClass("error");
//			return true;
//		}
//	 }
//
//	}


	$(function(){

	$("#form-admin-add").validate({
		rules:{
            txt_loginname1:{
                required:true,
                minlength:1,
                maxlength:8
            },
            txt_bankname:{
                required:true,
                minlength:1,
                maxlength:16
            },

            txt_rid:{
                required:true,
                minlength:1,
                maxlength:16
            },
            txt_tel:{
                required:true,
                minlength:7,
                maxlength:11
            }
        },
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){

			if(is_tuijian($("#txt_rid").val()) && shoujihaoma($("#txt_tel").val()))
			{

				$(form).ajaxSubmit();
				var index = parent.layer.getFrameIndex(window.name);
				parent.$('.btn-refresh').click();
				parent.layer.close(index);
			}


		}
	});
});
    // 验证身份证
    /*function sfz(sfz) {
        var pattern = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
        if(!pattern.test(sfz)){
            $("#bankno").css("display","block");
        }else {
            $("#bankno").css("display","none");
        }
    }*/
    // 验证银行卡号
    function isCardNo(bankno) {
        var bankno=bankno;
        $.ajax({
            type:"POST",
            url:"/SysAdmin/User/check_bankno",
            dataType:"json",
            data:{"bankno":bankno,"uid":<?php echo ($rs_staffInfo["id"]); ?>},
            success:function(msg){
                if(msg==0){
                    $("#bankno").css("display","block");
                    alert('银行卡卡号已存在');
                }else{
                    $("#bankno").css("display","none");
                }

            }
        })
    }

    </script>

</body>

</html>