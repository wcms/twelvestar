<?php
namespace SysAdmin\Controller;
use Think\Controller;
header("content-type:text/html;charset=utf-8");
class IndexController extends CalendarsController {
    public function Index(){
        $this->LoginTrue();
        $systemName=M("system");
        $aUser=session("aUser");
        $this->assign("aUser",$aUser);
        $logintime=session("loginTime");
        $this->assign("logintime",$logintime);
        $aName=session("aName");
        $this->assign("aName",$aName);
        $aPowers=session("aPowers");
        $admin_role=M("admin_role");
        if($aPowers==0){
            $powersName="系统管理员";
            $powersValue="0";
        }else{
            $rs_role=$admin_role->where("arId={$aPowers}")->field("arName,arPowers")->find();
            $powersName=$rs_role["arName"];
            $powersValue=$rs_role["arPowers"];
            $morepowersValue=$powersValue;
            $powersValue=explode("-", $powersValue);
        }
		 $admin_info=M('admin')->find();
//        print_r($admin_info);die();
        $this->assign("admin_info",$admin_info);
        $this->assign("powersName",$powersName);
        $this->assign("morepowersValue",$morepowersValue);
        $this->assign("powersValue",$powersValue);
       
        //获取当前日期
        $this->NowToday();
        // 活动自定义菜单
       
	   
        $this->display();
    }
    function random_color(){
        mt_srand((double)microtime()*1000000);
        $c = '';
        while(strlen($c)<6){
            $c .= sprintf("%02X", mt_rand(0, 200));
        }
        return $c;
    }
    public function Welcome(){
        $this->LoginTrue();
        //获取网站基本信息
        $users=M('users');
        $people_count=$users->field("count(id)")->count();
        $this->assign("people_count",$people_count);

		
        $todayuser=$users->field("count(id)")->where("from_unixtime(addtime,'%Y-%m-%d') = '".date('Y-m-d')."'")->count();
        $this->assign("todayuser",$todayuser);

        $passuser=M("usersjinfo")->field("count(id)")->where("status1=2 and status2=2")->count();
        $this->assign("passuser",$passuser);

		
	  

        $reg_member=$users->field("count(id)")->where("standardlevel=0")->count();
        $this->assign("reg_member",$reg_member);

        $vip_member=$users->field("count(id)")->where("standardlevel=1")->count();
        $this->assign("vip_member",$vip_member);

        $one_star=$users->field("count(id)")->where("standardlevel=2")->count();
        $this->assign("one_star",$one_star);

        $two_star=$users->field("count(id)")->where("standardlevel=3")->count();
        $this->assign("two_star",$two_star);

        $three_star=$users->field("count(id)")->where("standardlevel=4")->count();
        $this->assign("three_star",$three_star);

        $four_star=$users->field("count(id)")->where("standardlevel=5")->count();
        $this->assign("four_star",$four_star);

        $five_star=$users->field("count(id)")->where("standardlevel=6")->count();
        $this->assign("five_star",$five_star);

        $six_star=$users->field("count(id)")->where("standardlevel=7")->count();
        $this->assign("six_star",$six_star);

        $seven_star=$users->field("count(id)")->where("standardlevel=8")->count();
        $this->assign("seven_star",$seven_star);

		
        $eight_star=$users->field("count(id)")->where("standardlevel=9")->count();
        $this->assign("eight_star",$eight_star);
		
        $ten_star=$users->field("count(id)")->where("standardlevel=10")->count();
        $this->assign("ten_star",$ten_star);
		
        $elve_star=$users->field("count(id)")->where("standardlevel=11")->count();
        $this->assign("elve_star",$elve_star);
		
        $tenv_star=$users->field("count(id)")->where("standardlevel=12")->count();
        $this->assign("tenv_star",$tenv_star);
		
        $adminInfo=M("admin");
        $aName=session("aName");
        $aUser=session("aUser");
        $this->assign("aName",$aName);
        $aUser=session("aUser");
        $aPowers=session("aPowers");
        $rs_adminInfo=$adminInfo->field("aLoginNum")->where("aUser='{$aUser}'")->find();
        $this->assign("rs_adminInfo",$rs_adminInfo);
        
        $admin_role=M("admin_role");
        if($aPowers==0){
            $powersName="系统管理员";
            $powersValue="0";
        }else{
            $rs_role=$admin_role->where("arId={$aPowers}")->field("arName,arPowers")->find();
            $powersName=$rs_role["arName"];
            $powersValue=$rs_role["arPowers"];
            $morepowersValue=$powersValue;
            $powersValue=explode("-", $powersValue);
        }
       
        $this->assign("powersName",$powersName);
        $this->assign("morepowersValue",$morepowersValue);
        $this->assign("powersValue",$powersValue);
        
		
        //颜色
        for($i=0;$i<30;$i++){
            $colors[$i]="#".$this->random_color();
            $color=array($colors);
        }
        $this->assign("color",$color);
        
        
		$t = time();
		$start = mktime(0,0,0,date("m",$t),date("d",$t),date("Y",$t));
		$end = mktime(23,59,59,date("m",$t),date("d",$t),date("Y",$t));
		$incomeModel =  M("income");
		
		$userModel = M("users");
		
		
		
		
		
        $year=date("Y");
        $this->assign("year",$year);
        $this->display();
    }

}