<?php

namespace SysAdmin\Controller;
use Think\Controller;
header("content-type:text/html;charset=utf-8");
class GoodsController extends LoginTrueController{

    public function Index() {

        $this->display();
    }

    public function goods_list() {  //站内公告列表显示
        $this->assign('news_list', M('goods')->order(array('goods_id' => 'desc'))->select());
        $this->display();
    }

    public function news_content() {  //站内公告内容显示
        $id = $_GET['id'];
        $new = M('news')->select($id);
        $this->assign('new', $new);
        $this->display();
    }

    public function goods_del() {  //站内公告删除
        $id = $_GET['id'];
        $where['where']=array('goods_id'=>$id);
        $del = M('goods')->delete($where);
        if ($del) {
            $this->success("删除成功");
        } else {
            $this->error('删除失败');
        }
        $this->display('goods_list');
    }

    public function goods_add() {  //添加站内公告
        if ($_POST['goods_name']) {
            $news['goods_type'] = 2; //直接定为0:系统公告
            $news['goods_name'] = $_POST['goods_name'];
            $news['goods_price'] = $_POST['goods_price'];
            $news['goods_desc'] = $_POST['goods_desc'];
            $news['add_time'] = time();

            $upload = new \Think\Upload(); // 实例化上传类
            $upload->maxSize = 3145728; // 设置附件上传大小
            $upload->exts = array('jpg', 'gif', 'png', 'jpeg'); // 设置附件上传类型
            $upload->rootPath = './uploads/goods/'; // 设置附件上传根目录
            $upload->savePath = ''; // 设置附件上传（子）目录
            $info = $upload->upload();

            if ($_FILES['goods_img']['name']) {
                if (!$info) {// 上传错误提示错误信息
                    $this->error($upload->getError());
                } else {
                    $news["goods_img"] = "/uploads/goods/".$info['goods_img']['savepath'] . $info['goods_img']['savename'];
                }
            }

            if (M('goods')->add($news)) {
                $this->success('添加成功', './goods_list');
                exit;
            } else {
                $this->error('添加失败');
            }
        }
        $this->display('goods_add');
    }

    public function goods_update() {  //修改站内公告
         if ($_POST['goods_id']) {
            $news['goods_type'] = 2; //直接定为0:系统公告
            $news['goods_name'] = $_POST['goods_name'];
            $news['goods_price'] = $_POST['goods_price'];
            $news['goods_desc'] = $_POST['goods_desc'];
            $news['add_time'] = time();

            $upload = new \Think\Upload(); // 实例化上传类
            $upload->maxSize = 3145728; // 设置附件上传大小
            $upload->exts = array('jpg', 'gif', 'png', 'jpeg'); // 设置附件上传类型
            $upload->rootPath = './uploads/goods/'; // 设置附件上传根目录
            $upload->savePath = ''; // 设置附件上传（子）目录
            $info = $upload->upload();

            if ($_FILES['goods_img']['name']) {
                if (!$info) {// 上传错误提示错误信息
                    $this->error($upload->getError());
                } else {
                    $news["goods_img"] = "/uploads/goods/".$info['goods_img']['savepath'] . $info['goods_img']['savename'];
                }
            }

          

            $where=array('goods_id'=>$_POST['goods_id']);
            if (M('goods')->where($where)->save($news)) {
                $this->success('编辑成功', './goods_list');
                exit;
            } else {
                $this->error('编辑失败');
            }
        }

        $id = $_GET['id'];
        $where=array('goods_id'=>$id);
        $new = M('goods')->where($where)->select();
        $this->assign('goods', $new[0]);
        $this->display('goods_update');
    }

    public function change_status() {  //是否已经阅读的标识修改
        $id = $_POST['id'];
        $where['id'] = $id;
        $save['states'] = 1;
        M('message')->where($where)->save($save);
    }

    public function read_znx() {  //用户阅读站内信
        $id = $_GET['id'];
        $where['id'] = $id;
        $new = M('message')->where($where)->select();
        $this->assign('new', $new);
        $this->display();
    }

    public function delete() {
        $id = $_GET['id'];
        $where['id'] = $id;
        $res = M('message')->where($where)->delete();
        if ($res) {
            $this->success('删除成功');
        }
    }

}
