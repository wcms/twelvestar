<?php
namespace SysAdmin\Controller;
use Think\Controller;
header("content-type:text/html;charset=utf-8");
class MessageController extends CalendarsController {
    public function Index(){

        $this->display();   
    }
    public function znx_box(){
        $znx_info=M('message')->order("id desc")->select();
        $this->assign('znx_info',$znx_info);
        $this->display();
    }
    public function send_zxn(){  //发送站内信
//        $user=M('users'); //实例化用户表
        if($_REQUEST['accept_username']){  //先看看是不是发站内信的操作
            if($_REQUEST['accept_username']=='所有人'){
                $zn_info['send_types']='0';  //发送类型  1是给指定的人  0 是给所有人
                $zn_info['accept_username']='所有人';  //接收人
            }else{
                $zn_info['send_types']='1';
                $zn_info['accept_username']=$_REQUEST['accept_username'];  //接收人(固定的人)
            }
            $zn_info['accept_title']=$_REQUEST['accept_title'];//内容标题
            $zn_info['accept_content']=$_REQUEST['accept_content'];//发送内容
            $zn_info['send_username']='系统管理员';//发送人姓名:统一是系统管理员
            $zn_info['addtime']=date('Y-m-d H:i:s',time());//发送时间
            $zn_info['states']='0';//发送时间
//            print_r($zn_info);die;
            M('message')->add($zn_info);
            $this->success('发送成功',U("znx_box"));die;
        }
        if($_GET['stId']){  //如果是用户列表传过来的
            $user_name=M('users')->where("id=$_GET[stId]")->getField('loginname');
            $this->assign('user_name',$user_name);
        }
        $this->display();
    }
    public function checkUser_exist(){  //检测用户输入的用户名是不是存在
        $user_name=$_POST['user_name'];  //ajax传递过来的接收人
        $user=M('users'); //实例化用户表
        if($user->where(array('loginname'=> $user_name))->getField('id')){
           echo 1;
        }else{
            if($user_name=='所有人'){
                echo 1;
            }else{
                echo 0;
            }
        }
    }
    public function news_list(){  //站内公告列表显示
        $this->assign('news_list',M('news')->order(array('id'=>'desc'))->select());
        $this->display();
    }
    public function news_content(){  //站内公告内容显示
        $id=$_GET['id'];
        $new=M('news')->select($id);
        $this->assign('new',$new);
        $this->display();
    }
    public function news_del(){  //站内公告删除
        $id=$_GET['id'];
        $del=M('news')->delete($id);
        if($del){
            $this->success("公告删除成功");
        }else{
            $this->error('公告删除失败');
        }
        $this->display('news_list');
    }
    public function news_add(){  //添加站内公告
        if($_POST['title']){
            $news['types']='0'; //直接定为0:系统公告
            $news['title']=$_POST['title'];
            $news['pic']=empty($_POST['pic'])?0:$_POST['pic'];
            $news['content']=$_POST['content'];
            $news['UpdateTime']=$_POST['UpdateTime'];
            if(M('news')->add($news)){
                $this->success('公告添加成功','./news_list');
				exit;
            }else{
                $this->error('公告添加失败');
            }
        }
        $this->assign('UpdateTime',date('Y-m-d H:i:s',time()));//传递公告更新时间
        $this->display();

    }
    public function news_update(){  //修改站内公告

        if($_POST['update_id']){
            $news['types']='0'; //直接定为0:系统公告
            $news['title']=$_POST['title'];
            $news['pic']=empty($_POST['pic'])?0:$_POST['pic'];
            $news['content']=$_POST['content'];
            $news['UpdateTime']=$_POST['UpdateTime'];
            $update_id=$_POST['update_id'];
            $where=array('id'=>$update_id);
           if(M('news')->where($where)->save($news)){
               $this->success('公告修改成功','./news_list');
			   exit;
           }else{
               $this->error('公告修改失败','./news_list');
           }
        }

        $id=$_GET['id'];
        $new=M('news')->select($id);
        $this->assign('new',$new);
        $this->assign('UpdateTime',date('Y-m-d H:i:s',time()));//传递公告更新时间
        $this->display('news_update');

    }
    public function change_status(){  //是否已经阅读的标识修改
        $id=$_POST['id'];
        $where['id']=$id;
        $save['states']=1;
        M('message')->where($where)->save($save);
    }
    public function read_znx(){  //用户阅读站内信
        $id=$_GET['id'];
        $where['id']=$id;
        $new=M('message')->where($where)->select();
        $this->assign('new',$new);
        $this->display();
    }
    public function delete(){
        $id=$_GET['id'];
        $where['id']=$id;
        $res=M('message')->where($where)->delete();
        if($res){
            $this->success('删除成功');
        }

    }
}