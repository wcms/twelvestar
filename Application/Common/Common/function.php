<?php
function Page($count, $pagesize = 10) {
    $p = new Think\Page($count, $pagesize);
    $p->setConfig('header', '<li class="rows">共<b>%TOTAL_ROW%</b>条记录&nbsp;第<b>%NOW_PAGE%</b>页/共<b>%TOTAL_PAGE%</b>页</li>');
    $p->setConfig('prev', '上一页');
    $p->setConfig('next', '下一页');
    $p->setConfig('last', '末页');
    $p->setConfig('first', '首页');
    $p->setConfig('theme', '%FIRST%%UP_PAGE%%LINK_PAGE%%DOWN_PAGE%%END%%HEADER%');
    $p->lastSuffix = false;//最后一页不显示为总页数
    return $p;
}

//处理方法
function rmdirr($dirname) {
    if (!file_exists($dirname)) {
        return false;
    }
    if (is_file($dirname) || is_link($dirname)) {
        return unlink($dirname);
    }
    $dir = dir($dirname);
    if ($dir) {
        while (false !== $entry = $dir->read()) {
            if ($entry == '.' || $entry == '..') {
                continue;
            }
            //递归
            rmdirr($dirname . DIRECTORY_SEPARATOR . $entry);
        }
    }
}

//公共函数
//获取文件修改时间
function getfiletime($file, $DataDir) {
    $a = filemtime($DataDir . $file);
    $time = date("Y-m-d H:i:s", $a);
    return $time;
}

//获取文件的大小
function getfilesize($file, $DataDir) {
    $perms = stat($DataDir . $file);
    $size = $perms['size'];
    // 单位自动转换函数
    $kb = 1024;         // Kilobyte
    $mb = 1024 * $kb;   // Megabyte
    $gb = 1024 * $mb;   // Gigabyte
    $tb = 1024 * $gb;   // Terabyte

    if ($size < $kb) {
        return $size . " B";
    } else if ($size < $mb) {
        return round($size / $kb, 2) . " KB";
    } else if ($size < $gb) {
        return round($size / $mb, 2) . " MB";
    } else if ($size < $tb) {
        return round($size / $gb, 2) . " GB";
    } else {
        return round($size / $tb, 2) . " TB";
    }
}

function think_send_mail($to, $name, $subject = '', $body = '', $attachment = null){
    $config = C('THINK_EMAIL');
    $setmail=M("set_mail");
    $rs=$setmail->where("smId=1")->find();
    vendor('PHPMailer.class#phpmailer'); //从PHPMailer目录导class.phpmailer.php类文件
    vendor('SMTP');
    $mail = new PHPMailer(); //PHPMailer对象
    $mail->CharSet = 'UTF-8'; //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
    $mail->IsSMTP(); // 设定使用SMTP服务
    $mail->SMTPDebug = 0; // 关闭SMTP调试功能
    // 1 = errors and messages
    // 2 = messages only
    $mail->SMTPAuth = true; // 启用 SMTP 验证功能
    $mail->SMTPSecure = 'ssl'; // 使用安全协议
    $mail->Host =$rs["smSmtpHost"]; // SMTP 服务器
    $mail->Port =$rs["smSmtpPort"]; // SMTP服务器的端口号
    $mail->Username =$rs["smSmtpUser"]; // SMTP服务器用户名
    $mail->Password =$rs["smSmtpPass"]; // SMTP服务器密码
    $mail->SetFrom($rs["smFromEmail"], $rs["smFromName"]);
    $replyEmail = $rs["smReplayEmail"]?$rs["smReplayEmail"]:$rs["smFromEmail"];
    $replyName = $rs["smReplayName"]?$rs["smReplayName"]:$rs["smFromName"];
    $mail->AddReplyTo($replyEmail, $replyName);
    $mail->Subject = $subject;
    $mail->AltBody = "为了查看该邮件，请切换到支持 HTML 的邮件客户端";
    $mail->MsgHTML($body);
    $mail->AddAddress($to, $name);
    if(is_array($attachment)){ // 添加附件
        foreach ($attachment as $file){
            is_file($file) && $mail->AddAttachment($file);
        }
    }
    return $mail->Send() ? true : $mail->ErrorInfo;
}
function GetLevel($level){
    if($level=='-1'){
        return "普通会员";
    }
	switch($level){
        case 12:
            return "十二星会员";
        case 11:
            return "十一星会员";
        case 10:
            return "十星会员";
        case 9:
            return "九星会员";
        case 8:
            return "八星会员";
        case 7:
            return "七星会员";
        case 6:
            return "六星会员";
        case 5:
            return "五星会员";
        case 4:
            return "四星会员";
		case 3:
			return "三星会员";
		case 2:
			return "二星会员";
		case 1:
			return "一星会员";
		case 0:
			return "普通会员";
		default:
			return "普通会员";
	}
}
/**
 * 密保问题
 */
 function common_password_protect($question){
     switch($question){
         case 1:
             return "您父亲的姓名是？";
         case 2:
             return "您母亲的姓名是？";
         case 3:
             return "您配偶的姓名是？";
         case 4:
             return "您的出生地是？";
         case 5:
             return "您的学号（或工号）是？";
         case 6:
             return "您的小学校名是？";
         case 7:
             return "您初中班主任的名字是？";
         case 8:
             return "您最崇拜的人是?";

     }
 }

function GetChuli($chuli){
    switch ($chuli) {
        case '0':
           return "待审核";
        case '1':
           return "已审核";
        case '2':
           return "审核未通过";
        case '3':
           return "已发放";
        case '4':
           return "驳回请求";   
        default:
           return "待审核";

    }
}

	function getZhituiTree($info,$users){
		//$str .= "<ul>";
		$tarray = array();
		$info_lists = $users->field("id,rid,loginname,standardlevel")->where("rid='{$info[loginname]}'")->select();
		  
		foreach($info_lists as $k=>$v){
			//$str .= "<li>".$v['loginname'];
			$tarray[$v['loginname']]['rid'] = $v['rid'];
            $tarray[$v['loginname']]['standardlevel'] = $v['standardlevel'];
			$tarray[$v['loginname']]['name'] = $v['loginname'];
			$ch_array = $this->getZhituiTree($v,$users);
			$tarray[$v['loginname']]['childrencount'] = count($ch_array);
			$tarray[$v['loginname']]['children'] = $ch_array;

			
			//$str .= "</li>";
		}
		//$str .= "</ul>";
		return $tarray;
	}

	
	function qrcode($url,$filename){
        Vendor('phpqrcode.phpqrcode');
        //生成二维码图片
        $object = new \QRcode();
        $url=$url;//网址或者是文本内容
        $level=3;
        $size=4;
        $errorCorrectionLevel =intval($level) ;//容错级别
        $matrixPointSize = intval($size);//生成图片大小
		//echo "uploads/".$filename.".png";
        $object->png($url, "uploads/tjm/".$filename.".png", $errorCorrectionLevel, $matrixPointSize, 2);
    }
	
	
?>