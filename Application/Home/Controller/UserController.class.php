<?php
namespace Home\Controller;
ob_end_clean();
header("content-type:text/html;charset=utf-8");

class UserController extends LoginTrueController
{

    public function __construct()
    {
        parent::__construct();

        $this->LoginTrue();
		
    }

	
    /**
     *  订单列表pp
     **/
    public function UserSet()
    {
		
		$loginname = $_SESSION[nvip_nvip_member_User];
		$userid = $_SESSION[nvip_member_id];
		$userinfo = M("users")->field("truename,standardlevel,loginname")->where("loginname='".$loginname."'")->find();
		$userinfo['sjorder'] = M("usersjinfo")->where("user_id=$userid")->count();
	
		$userinfo['dzcount'] = M("usersjinfo")->field("sum(sh1dianzan)+sum(sh2dianzan) as tj")->where("user_id=".$userid)->find();
		
		if($userinfo['dzcount']){
			$userinfo['dzcount'] = $userinfo['dzcount']['tj'] ?: 0;
		}
		else{
			$userinfo['dzcount'] = 0;
		}
			
			
		$userinfo['level'] = getLevel($userinfo['standardlevel']);
		$userinfo['shorder'] = M("usersjinfo")->where("((shuser1='$loginname' and status1=2) or (shuser2='$loginname' and status2=2)) and status1=2 and status2=2 ")->count();
		
		$userinfo['tscount'] = M("tousuinfo")->where("tsuserobj='".$loginname."'")->count();
		
		$this->assign("userinfo",$userinfo);
		$this->assign("user",$_SESSION[nvip_nvip_member_User]);
		$this->display();
	}
	
	
    /**
     *  订单列表pp
     **/
    public function editInfo()
    {
		$loginname = $_SESSION[nvip_nvip_member_User];
		//$this->assign("user",$_SESSION[nvip_nvip_member_User]);
		
		$userinfo = M("users")->field("loginname,wximg,truename")->where("loginname='$loginname'")->find();
		
		$this->assign("userinfo",$userinfo);
		$this->display();
	}
	
    /**
     *  订单列表pp
     **/
    public function editInfoOp()
    {
		$loginname = $_SESSION[nvip_nvip_member_User];
		
		
		$userinfo = M("users")->field("loginname,wximg,id")->where("loginname='$loginname'")->find();
		
		
		$txt_loginname = $_POST["mobile"];
      
		if($txt_loginname){
			$save["truename"] = $txt_loginname;
		}
		
		$upload = new \Think\Upload();// 实例化上传类
		$upload->rootPath = './uploads/';
		$upload->maxSize   =     3145728 ;// 设置附件上传大小
		$upload->saveName = array('uniqid','');
		$upload->exts     = array('jpg', 'gif', 'png', 'jpeg');
		$upload->autoSub  = true;
		$upload->subName  = array('date','Ymd');
		$info   =   $upload->upload();
		if(!$info) {// 上传错误提示错误信息
			//$this->error($upload->getError());
		}else{// 上传成功 获取上传文件信息
			foreach($info as $file){
				$file_path =  $file['savepath'].$file['savename'];
			}
		}
        if ($file_path) {
           
			$save["wximg"] = $file_path;
        }
		
		
        $result = M('users')->where("id=".$userinfo['id'])->save($save);
       
            $this->error("操作成功",U("index/index"));
	}
	
    /**
     *  订单列表pp
     **/
    public function uploadPwd()
    {
		$this->display();
	}
	
	
	public function findpwdOp(){
		
		
		$mobile = $_POST['mobile'];
			$msginfo = M("msgvalidate")->where("mobile='$mobile' and ".time()."-addtime < 180")->order("id desc")->find();
			if(!$msginfo){
				$this->error("验证码错误");
			}
			
	$oldpassword = $_POST['oldpassword'];

	
	$code = $_POST['code'];
	$password = $_POST['password'];
		
	if($code != $msginfo['code']){
				$this->error("验证码错误");
	}
	$userinfo = M("users")->where("loginname='$mobile'")->field("pwd1")->find();
	if($userinfo['pwd1'] != md5($oldpassword)){
		$this->error("原始密码错误");
	}
		$save = array("pwd1"=>md5($password));
		M("users")->where("loginname='$mobile'")->save($save);
		$this->success("设置密码成功",U("index/index/index"));
exit;		
	}
	
/**
 * 随机生成指定长度的数字
 *
 * @param number $length        	
 * @return number
 */
function rand_number ($length = 6)
{
	if($length < 1)
	{
		$length = 6;
	}
	
	$min = 1;
	for($i = 0; $i < $length - 1; $i ++)
	{
		$min = $min * 10;
	}
	$max = $min * 10 - 1;
	
	return rand($min, $max);
}
	public function reguser(){
		
		$mobile = $_REQUEST['mobile_phone'];
		if($mobile){
			$msginfo = M("msgvalidate")->where("mobile='$mobile'")->find();
			if(time() - $msginfo['addtime']  < 60){
				$this->error("请稍后再操作");
			}
		$email_code = $this->rand_number(6);
		$msgtext = "【猫咪源码】欢迎注册，您的验证码为".$email_code."，千万不要告诉别人哦。";
		
		if($this->SendMsg($mobile,$msgtext)){
			
			$save = array(
				"mobile" =>$mobile,
				"code" =>$email_code,
				"addtime" => time()
			);
			
			M("msgvalidate")->add($save);
			
			echo "ok";
			exit;
		}
		}
		echo "短信发送失败";
		exit;
	}
	public function findpwd(){
		
		$mobile = $_REQUEST['mobile_phone'];
		if($mobile){
			$msginfo = M("msgvalidate")->where("mobile='$mobile'")->find();
			if(time() - $msginfo['addtime']  < 60){
				$this->error("请稍后再操作");
			}
		$email_code = $this->rand_number(6);
		$msgtext = "【猫咪源码】您正在修改密码，验证码为".$email_code."，千万不要告诉别人哦。";
		
		if($this->SendMsg($mobile,$msgtext)){
			
			$save = array(
				"mobile" =>$mobile,
				"code" =>$email_code,
				"addtime" => time()
			);
			
			M("msgvalidate")->add($save);
			
			echo "ok";
			exit;
		}
		}
		echo "短信发送失败";
		exit;
	}
    /**
     *  订单列表pp
     **/
    public function order_list()
    {
        if ($_GET['id'] && !$_POST['update_order']) {
            $order = M('order_info')->where("id='{$_GET[id]}'")->find();
            $order_status = $order['order_status'] . $order['shipping_status'] . $order['pay_status'];
            $this->assign('order_status_num', $order_status);
            switch ($order_status) {
                case 102:
                    $this->assign('order_status', '已付款,待发货');
                    break;
                case 521:
                    $this->assign('order_status', '已付款,已发货,待收货');
                    break;
                case 522:
                    $this->assign('order_status', '已付款,已发货,已收货');
                    break;
            }
            $this->assign('order', $order);
            $this->display('order_handle');
            die;
        } elseif ($_POST['update_order'] == 'update_order') {
            if (!$_POST['kd_name'] || !$_POST['kd_order']) {
                $this->success('没有发货信息,暂不能收货');
                die;
            }
            //更新快递信息
            $data = array(
                'order_status' => 5,
                'shipping_status' => 2,
                'pay_status' => 2,
                'confirm_time' => time(),
            );
            if (M('order_info')->where("id='$_POST[order_id]'")->save($data)) {
                $this->success('确认收货成功', U('User/order_list'));
            } else {
                $this->success('操作失败,请联系管理员');
            }
            die;
        } else {
            //获取订购的订单
            $order_list = M('order_info')->where("loginname='$_SESSION[nvip_nvip_member_User]'")->select();
            foreach ($order_list as &$v) {
                $order_status = $v['order_status'] . $v['shipping_status'] . $v['pay_status'];
                switch ($order_status) {
                    case 102:
                        $v['order_status_pp'] = '已付款,待发货';
                        break;
                    case 521:
                        $v['order_status_pp'] = '已付款,已发货,待收货';
                        break;
                    case 522:
                        $v['order_status_pp'] = '已付款,已发货,已收货';
                        break;
                }
            }
            $this->assign('order_list', $order_list);
            $this->display();
        }


    }
	//判断用户名

	public function Checkloginname()
	{
		$getusername = str_replace('\'','',stripslashes($_GET["username"]));

		$users=M("users")->field("loginname")->where("loginname='".$getusername."'")->find();

		
		if(empty($users)){
			echo "0";
			exit();
		}
		echo "1";
		exit();


	}

    /**
     *  系统全部公告显示列表pp
     **/
    public function news_list()
    {
        if ($_GET['id']) {
            $news_one = M("news")->where("id='{$_GET[id]}'")->find();
            $this->assign("news_one", $news_one);//公告单条
            $this->display('news_one');
        } else {
            $gonggao = M("news")->select();
            $this->assign("news_list", $gonggao);//公告
            $this->display();
        }

    }

    /**
     *  注册会员-pp
     **/
    public function add_user()
    {
        $this->LoginTrue();


		$this->assign("tuijianma", $_SESSION['nvip_member_tuijianma']);
       
	   
        $i = 0;
       
	   
	   

        $this->display();
    }

    /**
     * 注册会员提交后的处理-pp
     */
    public function Add_Action()
    {
		
        $this->LoginTrue();
        $txt_loginname = $_POST["mobile"];
        if (!$txt_loginname) {
            $this->error("手机号不能为空");
            exit();
        }
        if ($this->check_loginname($txt_loginname)) {
            $this->error("手机号重复，请重新获取!");
            exit();
        }
		if($this->Sys_systemName['sRegUserCheckCode'] == 1){
			$code = $_POST["code"];
			if (!$code) {
				$this->error("手机验证码不能为空");
				exit();
			}
			$msginfo = M("msgvalidate")->where("mobile='$txt_loginname' and ".time()."-addtime < 180")->order("id desc")->find();
			if(!$msginfo){
				$this->error("验证码错误");
			}
				
			
			if($code != $msginfo['code']){
						$this->error("验证码错误");
					exit();
			}
		}
	
	
        //验证身份证是不是重复
        $id_sfz = $_POST['txt_identityid'];
		$rid = $_SESSION['nvip_member_tuijianma'];
        if (!$rid) {
            $this->error("推荐人不能为空");
            exit();
        }
		
        if (!$_POST["realname"]) {
            $this->error("商家姓名不能为空");
            exit();
        }
        if (!$_POST["password"]) {
            $this->error("请填写密码");
            exit();
        }
        if ($_POST["cpassword"] != $_POST["password"]) {
            $this->error("两次输入的密码不一致");
            exit();
        }

		$upload = new \Think\Upload();// 实例化上传类
		$upload->rootPath = './uploads/';
		$upload->maxSize   =     3145728 ;// 设置附件上传大小
		$upload->saveName = array('uniqid','');
		$upload->exts     = array('jpg', 'gif', 'png', 'jpeg');
		$upload->autoSub  = true;
		$upload->subName  = array('date','Ymd');
		$info   =   $upload->upload();
		if(!$info) {// 上传错误提示错误信息
			$this->error($upload->getError());
		}else{// 上传成功 获取上传文件信息
			foreach($info as $file){
				$file_path =  $file['savepath'].$file['savename'];
			}
		}
        if (!$file_path) {
            $this->error("请选择微信二维码上传");
            exit();
        }
		
		
        $r_user = M("users")->where("tuijianma='{$rid}'")->find();//rpath根据推荐人来计算
		if(!$r_user){
            $this->error("推荐人信息错误");
            exit();
		}
        if ($r_user['rpath']) {
            $data['rpath'] = $r_user['rpath'] . "," . $r_user['id'];//推荐rpath
        } else {
            $data['rpath'] = $r_user['id'];//推荐rpath
        }
       
	   
        $data['wximg'] = $file_path;//微信二维码
        $data['ceng'] = $r_user_p['ceng'] + 1;//层
        $data['dai'] = $r_user['dai'] + 1;
        $data['standardlevel'] = '0';//刚开始就是临时会员
        $data["loginname"] = $txt_loginname;
        $data["tuijianma"] = $this->SysSet['inviteStart'].$this->get_random(3);;
        $data["truename"] = $_POST['realname'];
        $data["tel"] = $txt_loginname;
        $data["rid"] = $r_user['id'];
        $data["pwd1"] = md5(trim($_POST["password"]));
        $data["pwd2"] = md5(trim($_POST["password"]));
        $data["states"] = 0;
        $data["addtime"] = time();
		
		
       
        $result = M('users')->add($data);
        if ($result) {
			
            $this->assign("txt_loginname", $data['loginname']);
            $this->assign("txt_pwd1", $_POST["password"]);
            $this->assign("txt_pwd2", $_POST["password"]);
            $this->assign("rid", base64_encode($data["rid"]));
            $this->assign("result", $result);
        } else {
            $this->error("注册失败,请重新注册");
            exit();
        }


        $this->display();
    }

	
	//会员升级
    public function usersjold()
	{
        $this->LoginTrue();
		
        $id = $_SESSION['nvip_member_id'];
		
		//判断是否有正在升级的宴请  and (status1!=1 and status2!=1)
		$isExists =M("usersjinfo")->where("user_id=$id")->order("id desc")->find();
		if($isExists){
				if($isExists['status1'] ==0 or $isExists['status2'] ==0){
					if((!($isExists['status1'] ==1 or $isExists['status2'] ==1))){
						$this->assign("isExists",1);
						$isExists["shuser1"] = $isExists["shuser1"] ?: "无";
						$isExists["shuser2"] = $isExists["shuser2"] ?: "无";
						$isExists["shuserstatus1"] = $isExists["shuser1"] ? ' - '.$this->GetStatus($isExists['status1']): '';
						$isExists["shuserstatus2"] = $isExists["shuser2"] ? ' - '.$this->GetStatus($isExists['status2']) : '';
						$this->assign("shinfo",$isExists);	
					}
			}
			
		}

			
			$user = M('users')->where("id='{$id}'")->field("standardlevel")->find();
			
			$user['standardlevelname'] = GetLevel($user['standardlevel']);
			if($user['standardlevel']+1>9){
				$user['target_standardlevelname'] = "已是最高等级";
			}else{
				$user['target_standardlevelname'] = GetLevel($user['standardlevel']+1);
			}
	
		
		$this->assign("userinfo",$user);
        $this->display();
	}
	//会员升级
    public function usersj()
	{
        $this->LoginTrue();
		
        $id = $_SESSION['nvip_member_id'];
		
		//判断是否有正在升级的宴请  and (status1!=1 and status2!=1)
		$isExists =M("usersjinfo")->where("user_id=$id")->order("id desc")->find();
			
		if($isExists){
					if(!($isExists['status1'] ==2 && $isExists['status2'] ==2)){
						$this->assign("isExists",1);
						if($isExists["shuser1"]){
							$isExists["shuser1user"] = M("users")->field("standardlevel,truename,wximg")->where("loginname='".$isExists["shuser1"]."'")->order("id desc")->find();
							$isExists["shuser1user"]['levelname'] = GetLevel($isExists["shuser1user"]['standardlevel']);
						}
						if($isExists["shuser2"]){
							$isExists["shuser2user"] = M("users")->field("standardlevel,truename,wximg")->where("loginname='".$isExists["shuser2"]."'")->order("id desc")->find();
							$isExists["shuser2user"]['levelname'] = GetLevel($isExists["shuser2user"]['standardlevel']);
						}
						//$isExists["shuser1"] = $isExists["shuser1"] ?: "无";
						//$isExists["shuser2"] = $isExists["shuser2"] ?: "无";
						$isExists["shuserstatus1"] = $isExists["shuser1"] ? $this->GetStatus($isExists['status1']): '';
						$isExists["shuserstatus2"] = $isExists["shuser2"] ? $this->GetStatus($isExists['status2']) : '';
						
						
						$this->assign("shinfo",$isExists);	
						
						if($isExists['status1'] !=0 && $isExists['status2'] !=0){
							
							$this->assign("issh",1);	
						}
						
					}
				
			
		}

			
			$user = M('users')->where("id='{$id}'")->field("standardlevel")->find();
			
			$user['standardlevelname'] = GetLevel($user['standardlevel']);
			if($user['standardlevel']+1>12){
				$user['target_standardlevelname'] = "已是最高等级";
			}else{
				$user['target_standardlevelname'] = GetLevel($user['standardlevel']+1);
			}
	
		
		$this->assign("userinfo",$user);
        $this->display();
	}
	
	
	public function GetStatus($opstatus){
		
		if($opstatus==0)
			return "未审核";
		if($opstatus==1)
			return "未通过";
		if($opstatus==2)
			return "已通过";
	}
	
	public function tousush(){
		
        $curuser = $_SESSION['nvip_nvip_member_User'];
		
		$tsobj = $_POST["tsuserobj"];
		$tscontent = $_POST["tscontent"];
		$tsorderid = $_POST["tsorderid"];
		$tstype = $_POST["tstype"];
		
		if(!$tsobj || !$tscontent || !$tsorderid){
			echo "1";
			exit;
		}
		
		//判断是否已投诉过
		$istousu = M("tousuinfo")->where("tsuser='".$curuser."' and tsorderid=$tsorderid and tsuserobj='".$tsobj."' and tstype=$tstype")->find();
      
		if($istousu){
			echo "3";
			exit;
		}
		$sjinfo = M("usersjinfo")->where("id=$tsorderid")->find();
	
		if($tstype==1){
			if(time() - $sjinfo['addtime'] < 24*60*60){
				echo "4";
				exit;
			}
		}
		else{
			if($sjinfo['shuser1'] == $tsobj){
				if(time() - $sjinfo['shtime1'] < 24*60*60){
					echo "4";
					exit;
				}
			}
			if($sjinfo['shuser2'] == $tsobj){
				//print_r($sjinfo);
			//	echo time() - $sjinfo['shtime2'] ;24088
			//	echo "ss";
				if(time() - $sjinfo['shtime2'] < 24*60*60){
					echo "4";
					exit;
				}
			}
			
		}
		
		$data = array(
			"tsorderid" =>$tsorderid,
			"tsuser" =>$curuser,
			"tsuserobj" =>$tsobj,
			"content" =>$tscontent,
			"tstype" => $tstype,
			"addtime" => time()
		);
		if(M("tousuinfo")->add($data)){
			echo "2";
		exit;
		}
		echo "1";
		exit;
		
	}

/* 	//会员升级
    public function usersj()
	{
        $this->LoginTrue();
		
        $id = $_SESSION['nvip_member_id'];
		
		//判断是否有正在升级的宴请 
		$isExists =M("usersjinfo")->where("user_id=$id and (status1=0 or status2=0)")->find();
		if($isExists){
			$this->assign("isExists",1);
			$isExists["shuser1"] = $isExists["shuser1"] ?: "无";
			$isExists["shuser2"] = $isExists["shuser2"] ?: "无";
			$this->assign("shinfo",$isExists);
			
		}
		else{
			$user = M('users')->where("id='{$id}'")->field("standardlevel")->find();
			
			$user['standardlevelname'] = GetLevel($user['standardlevel']);
			if($user['standardlevel']+1>9){
				$user['target_standardlevelname'] = "已是最高等级";
			}else{
				$user['target_standardlevelname'] = GetLevel($user['standardlevel']+1);
			}
		}
		
		
		$this->assign("userinfo",$user);
        $this->display();
	} */
	//会员升级
    public function sjaction()
	{
		
        $this->LoginTrue();
		
        $id = $_SESSION['nvip_member_id'];
		
		//判断是否有正在升级的宴请  and ((status1=0 and status2=0) or (status1=2 or status2=2)
		$isExists =M("usersjinfo")->where("user_id=$id")->order("id desc")->find();
		if($isExists){
			if($isExists['status1'] == 0 && $isExists['status2'] ==0){
				$this->error("你有申请正在处理");
			}
			
		}
		
		
        $user = M('users')->where("id='{$id}'")->field("standardlevel,loginname,rpath,dai")->find();
		if($user['standardlevel']+1>12){
			$this->error("已经是最高等级");
		}
		$targetlevel = $user['standardlevel']+1;
		// $tjcount = M('users')->where("rid='{$id}'")->count();
		$sjshuser = $this->isShengji($targetlevel,$id,$user['rpath'],$user['dai']);
		
		if(!is_array($sjshuser)){
			$this->error("升级条件未满足<br/>".$sjshuser);
		}
		
		if($sjshuser['find1'])
			$shuser1 = $sjshuser['find1']['loginname'];
		
		if($sjshuser['find2'])
			$shuser2 = $sjshuser['find2']['loginname'];
		
		
		
		$data = array(
			"user_id" => $id,
			"loginname" => $user['loginname'],
			"curlevel" => $user['standardlevel'],
			"targetlevel" => $user['standardlevel']+1,
			"shuser1" => $shuser1,
			"shuser2" => $shuser2,
			"addtime" => time()
		);
		

		
			
		if(M("usersjinfo")->add($data)){
			$msgtext = "【猫咪源码】用户".$user['loginname']."向您发来审核申请，请尽快处理。";
			if($shuser1){
				$this->SendMsg($shuser1,$msgtext);
			}
			if($shuser2){
				$this->SendMsg($shuser2,$msgtext);
			}
			
			$this->success("申请成功");
			exit;
		}
		
		$this->assign("userinfo",$user);
        $this->display();
	}
	
	
	public function sjactionagan(){
		
        $this->LoginTrue();
		
        $id = $_SESSION['nvip_member_id'];
		$orderid = $_REQUEST['oid'];
		if(!$orderid){
				$this->error("操作错误");
		}
		
		//判断是否有正在升级的宴请  and ((status1=0 and status2=0) or (status1=2 or status2=2)
		$isExists =M("usersjinfo")->where("id=$orderid")->order("id desc")->find();
		if($isExists){
			if($isExists['status1'] == 0 || $isExists['status2'] ==0){
				$this->error("你有申请正在处理");
			}
			$shuser1 = "";
			if($isExists['status1']==1){
				$data['status1'] = 0;
				$data['shtime1'] = '';
				$shuser1 = $isExists['shuser1'];	
			}
			else if($isExists['status2']==1){
				$data['status2'] = 0;
				$data['shtime2'] = '';
				$shuser2 = $isExists['shuser2'];	
			}
			if(M("usersjinfo")->where("id=$orderid")->save($data)){
				
				$msgtext = "【猫咪源码】用户".$isExists['loginname']."向您发来审核申请，请尽快处理。";
				if($shuser1){
					$this->SendMsg($shuser1,$msgtext);
				}
				if($shuser2){
					$this->SendMsg($shuser2,$msgtext);
				}
				
				$this->success("申请成功");
				exit;
			}
			
		}
		else{
				$this->error("操作错误");
		}
		
		if($sjshuser['find1'])
			$shuser1 = $sjshuser['find1']['loginname'];
		
		if($sjshuser['find2'])
			$shuser2 = $sjshuser['find2']['loginname'];
		
		
		
		$data = array(
			"user_id" => $id,
			"loginname" => $user['loginname'],
			"curlevel" => $user['standardlevel'],
			"targetlevel" => $user['standardlevel']+1,
			"shuser1" => $shuser1,
			"shuser2" => $shuser2,
			"addtime" => time()
		);
		

		
			
		if(M("usersjinfo")->add($data)){
			$msgtext = "【猫咪源码】用户".$user['loginname']."向您发来审核申请，请尽快处理。";
			if($shuser1){
				$this->SendMsg($shuser1,$msgtext);
			}
			if($shuser2){
				$this->SendMsg($shuser2,$msgtext);
			}
			
			$this->success("申请成功");
			exit;
		}
		
		$this->assign("userinfo",$user);
        $this->display();
	}
	
		//商家信息
    public function shuserlist01()
	{
        $this->LoginTrue();
		
        $id = $_SESSION['nvip_member_id'];
		
		//判断是否有正在升级的宴请 
		$isExists =M("usersjinfo")->where("user_id=$id")->order("id desc")->find();
		$sjarray=array();
		if($isExists['shuser1']){
			$sjarray[] = M("users")->where("loginname='".$isExists['shuser1']."'")->find();
		}
		if($isExists['shuser2']){
			$sjarray[] = M("users")->where("loginname='".$isExists['shuser2']."'")->find();
		}
		
		$this->assign("sjarray",$sjarray);
        $this->display();
	}
	
		//商家信息
    public function shuserlist()
	{
		
        $this->LoginTrue();
		
        $id = $_SESSION['nvip_member_id'];
		
		 $count = M("usersjinfo")->where("user_id=$id and status1=2 and status2=2 ")->count();
		 
		 $Page = Page($count, $this->pagesize);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();
		
		//判断是否有正在升级的宴请 
		$sjarray =M("usersjinfo")->where("user_id=$id and status1=2 and status2=2 ")->limit($Page->firstRow . ',' . $Page->listRows)->order("id desc")->select();
		
		foreach($sjarray as $key=>$val){
			
			if($val['shuser1']){
				$sjarray[$key]["shuser1user"] = M("users")->where("loginname='".$val['shuser1']."'")->find();
				$sjarray[$key]["shuser1user"]['levelname'] = GetLevel($val['standardlevel']);
				$sjarray[$key]["isz1"] = $val['sh1dianzan']==1 ? "已点赞" : "未点赞";
			}
			if($val['shuser2']){
				$sjarray[$key]["shuser2user"] = M("users")->where("loginname='".$val['shuser2']."'")->find();
				$sjarray[$key]["shuser2user"]['levelname'] = GetLevel($val['standardlevel']);
				$sjarray[$key]["isz2"] = $val['sh2dianzan']==1 ? "已点赞" : "未点赞";
			}
			
		}
		
		
        $this->assign("page", $show);
		$this->assign("sjarray",$sjarray);
        $this->display();
	}
	
		//审核升级
    public function userchecksjlog()
	{
        $this->LoginTrue();
		
        $loginname = $_SESSION['nvip_nvip_member_User'];
		//$shList = M("usersjinfo")->where("(shuser1='$loginname' or shuser2='$loginname'  )and ( status1=2 and status2=2)")->order("id desc")->select();
		$shList = M("usersjinfo")->where("((shuser1='$loginname' and status1=2) or (shuser2='$loginname' and status2=2)) and status1=2 and status2=2 ")->order("id desc")->select();
		foreach($shList as $key=>$val){
			
			$shList[$key]['user'] = M("users")->where("id=".$val['user_id'])->find();
			$shList[$key]['levelname'] = GetLevel($val['targetlevel']);
		//	$shList[$key]['sjtimes'] = M("usersjinfo")->where("user_id=".$val['user_id'])->count();
			// $shList[$key]['dztimes'] = M("usersjinfo")->field("sum(sh1dianzan)+sum(sh2dianzan) as tj")->where("user_id=".$val['user_id'])->find();
			$shList[$key]['dztimes'] =$val['sh1dianzan']+$val['sh2dianzan'];
			
			// if($shList[$key]['dztimes']){
				// $shList[$key]['dztimes'] = $shList[$key]['dztimes']['tj'];
			// }
			// else{
				// $shList[$key]['dztimes'] = 0;
			// }
			if($val["shuser1"] == $loginname){
				$shList[$key]['isz'] = $val['sh1dianzan'] ;
				$shList[$key]['curuser'] = 1;
			}
			else{
				$shList[$key]['isz'] = $val['sh2dianzan'] ;
				$shList[$key]['curuser'] = 2;
			}
			
		}
		$this->assign("shList",$shList);
		
        $this->display();
	}
	
	public function dianzaction(){
			
        $this->LoginTrue();
        $loginname = $_SESSION['nvip_nvip_member_User'];
		$orderid = $_POST['id'];
		$dzuser = $_POST['name'];
		$curuser = $_POST['curuser'];
		
		if(!$orderid){
			echo 2;exit;
		}
		if($curuser==1){
			$data = array(
				"sh1dianzan" => 1,
				"sh1dztime" => time()
			);
		}else{
			$data = array(
				"sh2dianzan" => 1,
				"sh2dztime" => time()
			);
		}
		
		if(M("usersjinfo")->where("id=$orderid")->save($data)){
			echo 1;exit;
		}
		echo 2;exit;
		
		
	}
		//审核升级
    public function userchecksj()
	{
        $this->LoginTrue();
		
        $loginname = $_SESSION['nvip_nvip_member_User'];
		
		$shList = M("usersjinfo")->where("(shuser1='$loginname' and status1=0) or (shuser2='$loginname' and status2=0)")->order("id desc")->select();
		foreach($shList as $key=>$val){
			
			$shList[$key]['user'] = M("users")->where("id=".$val['user_id'])->find();
			$shList[$key]['levelname'] = GetLevel($val['targetlevel']);
			
		}
		$this->assign("shList",$shList);
		
        $this->display();
	}
	//审核操作
    public function checksjop()
	{
        $this->LoginTrue();
		
        $loginname = $_SESSION['nvip_nvip_member_User'];
		
		$id = $_REQUEST['id'];
		$op = intval($_REQUEST['op']);
		if(!$id || !$op){
			$this->error("非法操作");
		}
		
		
		$shList = M("usersjinfo")->where("id='$id'  and (shuser1='$loginname' or shuser2='$loginname' )")->order("id desc")->find();
		if(!$shList){
			$this->error("不正确的操作");
		}
		$status = "";
		if($shList['shuser1'] == $loginname){
			
			if($shList['status1'] !=0){
				$this->error("您已经审核过此订单");
			}
			$save = array(
			"status1" => $op,
			"shtime1" => time(),
			"status2" => $shList['status2'],
			"shtime2" => $shList['shtime2'],
			);
			if(!$shList['shuser2']){
				$save["status2"] = $op;
				$save["shtime1"] = time();
			}
		
		}else if($shList['shuser2'] == $loginname){
			$status = "status2";
			if($shList['status2'] !=0){
				$this->error("您已经审核过此订单");
			}
			$save = array(
			"status1" => $shList['status1'],
			"shtime1" => $shList['shtime1'],
			"status2" => $op,
			"shtime2" => time()
			);
			if(!$shList['shuser1']){
				$save["status1"] = $op;
				$save["shtime1"] = time();
			}
		}
		if($shList['shuser1'] == $shList['shuser2']){
			
			$save = array(
			"status1" => $op,
			"shtime1" => time(),
			"status2" => $op,
			"shtime2" => time()
			);
		}
		$ispass = 0;
		if($save['status1'] == 2 && $save['status2'] ==2 ){
			$ispass = 1;
		}
		elseif($save['status1'] == 1 && $save['status2'] ==1 ){
			$ispass = 3;
		}
		
		M("usersjinfo")->startTrans();
		
		
		if(M("usersjinfo")->where("id=$id")->save($save)){
			if($ispass==1){
				M("users")->where("id=$shList[user_id]")->save(array("standardlevel"=>$shList['targetlevel']));
				$msgtext = "【猫咪源码】您的审核已通过，恭喜您成功升级为".$shList['targetlevel']."级会员。";
				$this->SendMsg($shList['loginname'],$msgtext);
			}
			if($ispass ==3){
				$msgtext = "【猫咪源码】您的审核申请被拒绝，请重新申请，如果被多次拒绝请联系客服。";
				$this->SendMsg($shList['loginname'],$msgtext);
			}
			
			M("usersjinfo")->commit();
			
			$this->success("操作成功");
			exit;
		}
		else{
			$this->error("操作失败");
		}
		
		$this->assign("shList",$shList);
		
        $this->display();
	}


    public function teamdata()
    {
        $this->LoginTrue();
		
        $id = $_SESSION['nvip_member_id'];
		$page = intval($_REQUEST['page']);
		$pagesize = 10;
		$start = ($page)*$pagesize;
		
		//$start = 0;//"rid='{$id}'"
        $invite_list = M('users')->where("FIND_IN_SET({$id},rpath)")->limit($start,$pagesize)->order("id desc")->select();
		$arraylist = array();
        foreach ($invite_list as $key=>$val) {
			$invite_list[$key]['levelname'] = GetLevel($val['standardlevel']);
			$tlist['levelname'] = $invite_list[$key]['levelname'];
			$tlist['loginname'] = $val['loginname'];
			$tlist['truename'] = $val['truename'];
			$tlist['wximg'] = $val['wximg'];
			$arraylist[] = $tlist;
        }
       // $this->assign('invite_list', $invite_list);
		
		//$arrays = array("Result"=>1);
		echo json_encode($arraylist);
		exit;
	}
	
    public function team()
    {
		
        $this->LoginTrue();
        $id = $_SESSION['nvip_member_id'];
        $invite_list = M('users')->where("FIND_IN_SET({$id},rpath)")->limit(10)->order("id desc")->select();
        foreach ($invite_list as $key=>$val) {
			$invite_list[$key]['levelname'] = GetLevel($val['standardlevel']);
        }
        $this->assign('invite_list', $invite_list);
		$allcount = M('users')->where("FIND_IN_SET({$id},rpath)")->order("id desc")->count();
		$xingcount = M('users')->where("FIND_IN_SET({$id},rpath) and standardlevel>0")->order("id desc")->count();
		
		
		$tjm = base64_encode(base64_encode($_SESSION['nvip_member_tuijianma']));
		$tjurl = "http://".$_SERVER['HTTP_HOST']."/Login/UserReg/rid/".$tjm;
		
		if(!file_exists("uploads/tjm/".$_SESSION['nvip_member_tuijianma'].".png")){
			(qrcode($tjurl,$_SESSION['nvip_member_tuijianma']));	
		}
		
		
		
		
		$this->assign('tjimg',"/uploads/tjm/".$_SESSION['nvip_member_tuijianma'].".png");
        $this->assign('allcount', $allcount);
		$this->assign('HttpUrl',$tjurl);
        $this->assign('xingcount', $xingcount);
		$this->display();
	}
	
	
	
    /**
     * 推荐列表 --直推  - - pp
     */
    public function invite_list()
    {
        $this->LoginTrue();
        $invite_list = M('users')->where("rid='{$_SESSION['nvip_nvip_member_User']}'")->select();
        foreach ($invite_list as &$v) {
            if ($v['jihuotime']) {
                $info = intval($v['daoqi_time']) - intval($v['jihuotime']);
                $day_num = floor($info / (24 * 60 * 60));
                $v['day_num'] = $day_num;
//                $this->assign("day_num", $day_num);
            } else {
                $v['day_num'] = '还未激活';
            }
        }
        $this->assign('invite_list', $invite_list);
        //获取距离到期还有多少天
//        if($userModel['jihuotime']){
//            $info=intval($userModel['daoqi_time'])-intval($userModel['jihuotime']);
//            $day_num=floor($info/(24*60*60));
//            $this->assign("day_num",$day_num);
//        }else{
//            $this->assign("day_num",'您还未激活');
//        }
        $this->display();
    }

    /**
     * 当前登录注册的会员待激活的-因为推荐人不一定是当前登录的人
     */
    public function reg_path_await_jihuo()
    {
        $this->LoginTrue();
        $current_name = $search_loginname = session("nvip_nvip_member_User");
        $reg_pathinfo = M('users')->where("loginname='$current_name'")->find();
        $reg_path = $reg_pathinfo['reg_path'];
        if ($reg_path) {
            $list = M('users')->where("(id IN ($reg_path) OR rid= '{$current_name}') AND states<1")->select();
        } else {
            $list = M('users')->where("rid= '{$current_name}' AND states<1")->select();
        }
        $this->assign('invite_list', $list);
        $this->display();
    }

    public function Lists()
    {
        $this->LoginTrue();
        $userLists = M("users");
        $search_loginname = $_POST['search_loginname'];
        $tel = $_POST['tel'];
        $truename = $_POST['truename'];
        $states = $_POST["states"];
        $where = " rid='" . session("nvip_nvip_member_User") . "' ";
        if ($search_loginname) {
            $where .= " and loginname like '%{$search_loginname}%'";
        }
        if ($tel) {
            $where .= " and tel like '%{$tel}%'";
        }
        if ($truename) {
            $where .= " and truename like '%{$truename}%'";
        }
        if ($states) {
            $states = $states == 2 ? 0 : $states;
            $where .= " and states=$states ";
        }

        $count = $userLists->where($where)->count();

        $Page = Page($count, $this->pagesize);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();


        $rs_suserLists = $userLists->field('id,loginname,rid,pid,tel,truename,standardlevel,states')->where($where)->order('id desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();

        foreach ($rs_suserLists as $key => $value) {
            $rs_suserLists[$key]['standardlevel'] = GetLevel($value['standardlevel']);


            $tuijian = $userLists->where('rid=' . $value['loginname'])->count();
            $rs_suserLists[$key]['tuijian'] = $tuijian;
        }

        $this->assign("rs_staffLists", $rs_suserLists);
        $year = date("Y");
        $this->assign("year", $year);
        $this->assign("host", $_SERVER['HTTP_HOST']);
        $this->assign("page", $show);

        $this->display();
    }

    public function Add()
    {
        $this->LoginTrue();

        $pid = $_GET['pid'];
        $area = $_GET['area'];

        if ($pid == '') {
            $this->error("您没有选择上级接点", "affectlists");
        }
        $user = M('users')->where("pid = '{$pid}'  and area=1")->find();


        if ($area == 2) {
            if (!$user) {
                $this->error("请优先在一部注册", "affectlists");
            }
        }

        $i = 0;
        while ($this->check_loginname($bianhao = $this->get_random(4))) {
            if ($i > 10) {
                exit('系统错误');
            }
            $i++;
        }

        $this->assign("bianhao", $bianhao);

        $this->display();
    }


    public function AddAction()
    {


        $txt_loginname = $_POST["txt_loginname"];

        if (!$txt_loginname) {
            $this->error("会员编号不能为空");
        }
        if ($this->check_loginname($txt_loginname)) {
            $this->error("会员编号重复，请重新获取!");
        }
        if (!$_POST["txt_rid"]) {
            $this->error("推荐编号不能为空");
        }


        $r_user = M("users")->where("id='{$_SESSION['nvip_member_id']}'")->find();


        if ($r_user['rpath']) {
            $data['rpath'] = $r_user['rpath'] . "," . $r_user['id'];//推荐path
        } else {
            $data['rpath'] = $r_user['id'];//推荐path
        }
        if ($r_user['ppath']) {
            $data['ppath'] = $r_user['ppath'] . "," . $r_user['id'];//推荐path
        } else {
            $data['ppath'] = $r_user['id'];//推荐path
        }


        $data['ceng'] = $r_user['ceng'] + 1;//层
        $data['dai'] = $r_user['dai'] + 1;


        $data['pid'] = $_POST['txt_pid'];//接点人
        $data['area'] = $_POST['txt_area'];//接点区域
        $data['standardlevel'] = $_POST['txt_jibie'];//级别
        $data['taobao'] = $_POST['txt_taobao'];
        $data['zhifubao'] = $_POST['txt_zhifubao'];
        $data['zfbName'] = $_POST['txt_zfbname'];
        $data['bank'] = $_POST['txt_bank'];
        $data['bankno'] = $_POST['txt_bankno'];
        $data["bankname"] = $_POST["txt_bankname"];
        $data["truename"] = $_POST["txt_bankname"];
        $data["bankaddress"] = $_POST["txt_bankaddress"];
        $data['teamdai'] = $r_user['teamdai'] + 1;
        $data["loginname"] = $txt_loginname;
        $data["tel"] = $_POST["txt_tel"];
        $data["identityid"] = $_POST["txt_cardno"];
        $data["rid"] = $_POST["txt_rid"];
        $data["pwd1"] = md5(trim($_POST["txt_pwd1"]));
        $data["pwd2"] = md5(trim($_POST["txt_pwd2"]));
        $data["states"] = 0;


        if ($_POST['txt_jibie'] == 0) {
            $data["states"] = 1;
            $data["jihuotime"] = time();
        } else {
            switch ($_POST['txt_jibie']) {
                case 1:
                    $data['jihuoAmount'] = $this->Vip1;
                    $data['keyongedu'] = $this->Tongka;
                    break;
                case 2:
                    $data['jihuoAmount'] = $this->Vip2;
                    $data['keyongedu'] = $this->Yinka;
                    break;
                case 3:
                    $data['jihuoAmount'] = $this->Vip3;
                    $data['keyongedu'] = $this->Jinka;
                    break;
            }
        }

        $data["addtime"] = time();


        $result = M('users')->add($data);

        $this->assign("txt_loginname", $data['loginname']);
        $this->assign("txt_pwd1", $_POST["txt_pwd1"]);
        $this->assign("txt_pwd2", $_POST["txt_pwd2"]);
        $this->assign("pay_amount", $data['pay_amount']);

        $this->assign("rid", base64_encode($data["rid"]));

        $this->assign("result", $result);

        $this->display();

    }


    /**
     *  奖金列表
     **/
    public function Bonuslists01()
    {
        $this->LoginTrue();
        $username = session("nvip_nvip_member_User");

        $BonusModel = M("income");


        $fdate = isset($_REQUEST['fdate']) ? trim($_REQUEST['fdate']) : "";
        if (!$fdate) {
            $this->error("error");
        }

        $where = " 1=1 and types in ('推荐奖','见点奖','佣金') and userid='{$username}' and FROM_UNIXTIME(addtime,'%Y-%m-%d')='$fdate' ";

        $reason = isset($_POST['search_loginname']) ? trim($_POST['search_loginname']) : "";
        $from = isset($_POST['from']) ? strtotime(trim($_POST['from'])) : "";
        $until = isset($_POST['until']) ? strtotime(trim($_POST['until'])) : "";
        $jiangtype = isset($_POST['jiangtype']) ? trim($_POST['jiangtype']) : "";
        $isfafang = isset($_POST['isfafang']) ? trim($_POST['isfafang']) : "";
        $search_loginname_reg = isset($_POST['search_loginname_reg']) ? trim($_POST['search_loginname_reg']) : "";


        $where .= $reason ? " AND userid like '%$reason%'" : "";
        $where .= $from ? " AND addtime >= '{$from}'" : "";
        $where .= $until ? " AND addtime <= '{$until}'" : "";
        $where .= $jiangtype ? " AND types = '{$jiangtype}'" : "";
        $where .= $search_loginname_reg ? " and reason like '%{$search_loginname_reg}%'" : "";

        if ($isfafang) {
            $isfafang = $isfafang == 2 ? 0 : $isfafang;
            $where .= " and ischuli = $isfafang";
        }


        $count = $BonusModel->where($where)->count();

        $Page = Page($count, $this->pagesize);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();


        $bonusList = $BonusModel->where($where)->order("id desc")->limit($Page->firstRow . ',' . $Page->listRows)->select();


        $jiangsum = $BonusModel->where($where)->sum("amount");

        $jiangsum = $jiangsum ? $jiangsum : 0;


        $this->assign("userinfo", $userinfo);
        $this->assign("jiangsum", $jiangsum);

        $this->assign("bonusList", $bonusList);

        $this->assign("page", $show);

        $this->display();

    }


    /**
     *  奖金列表
     **/
    public function Bonuslists02()
    {
        $this->LoginTrue();
        $username = session("nvip_nvip_member_User");

        $BonusModel = M("income");

        $users = M("users");
        $userinfo = $users->where("loginname='{$username}'")->find();
        $userinfo['level'] = GetLevel($userinfo["standardlevel"]);

        $where = " 1=1 and types in ('推荐奖','见点奖','佣金') and userid='{$username}' ";

        $reason = isset($_POST['search_loginname']) ? trim($_POST['search_loginname']) : "";
        $from = isset($_POST['from']) ? strtotime(trim($_POST['from'])) : "";
        $until = isset($_POST['until']) ? strtotime(trim($_POST['until'])) : "";
        $jiangtype = isset($_POST['jiangtype']) ? trim($_POST['jiangtype']) : "";
        $isfafang = isset($_POST['isfafang']) ? trim($_POST['isfafang']) : "";
        $search_loginname_reg = isset($_POST['search_loginname_reg']) ? trim($_POST['search_loginname_reg']) : "";


        $where .= $reason ? " AND userid like '%$reason%'" : "";
        $where .= $from ? " AND addtime >= '{$from}'" : "";
        $where .= $until ? " AND addtime <= '{$until}'" : "";
        $where .= $jiangtype ? " AND types = '{$jiangtype}'" : "";
        $where .= $search_loginname_reg ? " and reason like '%{$search_loginname_reg}%'" : "";

        if ($isfafang) {
            $isfafang = $isfafang == 2 ? 0 : $isfafang;
            $where .= " and ischuli = $isfafang";
        }


        $count = $BonusModel->where($where)->count();

        $Page = Page($count, $this->pagesize);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();


        $bonusList = $BonusModel->field("FROM_UNIXTIME(addtime,'%Y-%m-%d') as fdate")->where($where)->order("id desc")->group("FROM_UNIXTIME(addtime,'%Y-%m-%d')")->limit($Page->firstRow . ',' . $Page->listRows)->select();

        $bonusnew = array();
        foreach ($bonusList as $k => $v) {
            //推荐奖
            $wheresql = $where . " and FROM_UNIXTIME(addtime,'%Y-%m-%d')='{$v[fdate]}'";
            $v["tuiamount"] = $BonusModel->where($wheresql . " and types='推荐奖'")->sum("jine");
            $v["jiandianamount"] = $BonusModel->where($wheresql . " and types='见点奖'")->sum("jine");
            $v["yongamount"] = $BonusModel->where($wheresql . " and types='佣金'")->sum("jine");
            $v["yongamount"] = $v["yongamount"] ? $v["yongamount"] : 0;
            $v["tuiamount"] = $v["tuiamount"] ? $v["tuiamount"] : 0;
            $v["jiandianamount"] = $v["jiandianamount"] ? $v["jiandianamount"] : 0;
            $bonusnew[] = $v;
        }


        $jiangsum = $BonusModel->where($where)->sum("amount");

        $jiangsum = $jiangsum ? $jiangsum : 0;


        $userinfo['recordCount'] = ($BonusModel->where("remark='系统发放' AND userid = '{$username}' and remark='系统发放'  ")->count());//共多少条记录

        $userinfo['jisuanCount'] = $BonusModel->where("remark='系统发放' and ischuli=1 AND userid = '{$username}' and remark='系统发放'  ")->count();//发放条数

        $jinsum = $BonusModel->field("sum(jine) as jineSum")->where("remark='系统发放' and ischuli=1 AND userid = '{$username}' and remark='系统发放' ")->find();//发放金额
        $userinfo['amountJie'] = $jinsum['jineSum'] ? $jinsum['jineSum'] : 0;

        $this->assign("userinfo", $userinfo);
        $this->assign("jiangsum", $jiangsum);

        $this->assign("bonusList", $bonusnew);

        $this->assign("page", $show);

        $this->display();

    }

    /**
     *  奖金列表统计 -- pp
     **/
    public function Bonuslists()
    {
        $this->LoginTrue();
        $search_loginname = session("nvip_nvip_member_User");

        $BonusModel = M("income");

        $where = " 1=1  AND userid = '{$search_loginname}'  and ischuli=1 ";

        if ($id)
            $where = "userid={$id}";

//		$reason = isset($_POST['search_loginname']) ? trim($_POST['search_loginname']) : "";
        $from = isset($_POST['from']) ? strtotime(trim($_POST['from'])) : "";
        $until = isset($_POST['until']) ? strtotime(trim($_POST['until'])) : "";
        $jiangtype = isset($_POST['jiangtype']) ? trim($_POST['jiangtype']) : "";


//		$where .= $reason ? " AND reason like '{$reason}'" :"";
        $where .= $from ? " AND addtime >= '{$from}'" : "";
        $where .= $until ? " AND addtime <= '{$until}'" : "";
        $where .= $jiangtype ? " AND types = '{$jiangtype}'" : "";


        $pagesize = 8;
        $count = $BonusModel->where($where)->count();

        $Page = Page($count, $pagesize);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();


        $bonusList = $BonusModel->where($where)->order("id desc")->limit($Page->firstRow . ',' . $Page->listRows)->select();

        $this->assign("bonusList", $bonusList);
        $this->assign("jianglist", $jianglist);
        $this->assign("page", $show);

        $this->display();
    }

    /**
     * 注册币明细   --pp
     */
    public function reg_coin_detail()
    {
        if (IS_POST) {
            if (!($_POST['from'] && $_POST['until'])) {
                $this->error('请输入正确的时间段');
            } else {
                $from = str_replace('T', ' ', $_POST['from']);
                $from_time = strtotime($from);
                $until = str_replace('T', ' ', $_POST['until']);
                $until_time = strtotime($until);
                $pagesize = 10;
                $count = M('account_log')->where("loginname='{$_SESSION['nvip_nvip_member_User']}' AND amount_type=1 AND change_time between $from_time and $until_time")->count();
                $Page = Page($count, $pagesize);//
                $show = $Page->show();
                $reg_coin_detail = M('account_log')->where("loginname='{$_SESSION['nvip_nvip_member_User']}' AND amount_type=1 AND change_time between $from_time and $until_time")->order("id desc")->limit($Page->firstRow . ',' . $Page->listRows)->select();

                $this->assign('reg_coin_detail', $reg_coin_detail);
                $this->assign("page", $show);
                $this->display();
                die;
            }
        }
        $pagesize = 10;
        $count = M('account_log')->where("loginname='{$_SESSION['nvip_nvip_member_User']}' AND amount_type=1")->count();
        $Page = Page($count, $pagesize);//
        $show = $Page->show();
        $reg_coin_detail = M('account_log')->order('id desc')->where("loginname='{$_SESSION['nvip_nvip_member_User']}' AND amount_type=1")->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $this->assign('reg_coin_detail', $reg_coin_detail);
        $this->assign("page", $show);
        $this->display();
    }

    /**
     * 奖金明细   --pp
     */
    public function bonus_detail()
    {
        if (IS_POST) {
            if (!($_POST['from'] && $_POST['until'])) {
                $this->error('请输入正确的时间段');
            } else {
                $from = str_replace('T', ' ', $_POST['from']);
                $from_time = strtotime($from);
                $until = str_replace('T', ' ', $_POST['until']);
                $until_time = strtotime($until);

                $pagesize = 10;
                $count = M('account_log')->where("loginname='{$_SESSION['nvip_nvip_member_User']}' AND amount_type=2 AND change_time between $from_time and $until_time")->count();
                $Page = Page($count, $pagesize);//
                $show = $Page->show();
                $reg_coin_detail = M('account_log')->where("loginname='{$_SESSION['nvip_nvip_member_User']}' AND amount_type=2 AND change_time between $from_time and $until_time")->order("id desc")->limit($Page->firstRow . ',' . $Page->listRows)->select();
                $this->assign("page", $show);
                $this->assign('reg_coin_detail', $reg_coin_detail);
                $this->display();
                die;
            }
        }

        $pagesize = 10;
        $count = M('account_log')->where("loginname='{$_SESSION['nvip_nvip_member_User']}' AND amount_type=2")->count();
        $Page = Page($count, $pagesize);//
        $show = $Page->show();
        $reg_coin_detail = M('account_log')->where("loginname='{$_SESSION['nvip_nvip_member_User']}' AND amount_type=2")->order("id desc")->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $this->assign('reg_coin_detail', $reg_coin_detail);
        $this->assign("page", $show);
        $this->display();
    }

    /**
     * 购物币明细
     */
    public function gwjf_detail()
    {
        if (IS_POST) {
            if (!($_POST['from'] && $_POST['until'])) {
                $this->error('请输入正确的时间段');
            } else {
                $from = str_replace('T', ' ', $_POST['from']);
                $from_time = strtotime($from);
                $until = str_replace('T', ' ', $_POST['until']);
                $until_time = strtotime($until);

                $pagesize = 10;
                $count = M('account_log')->where("loginname='{$_SESSION['nvip_nvip_member_User']}' AND amount_type=8 AND change_time between $from_time and $until_time")->count();
                $Page = Page($count, $pagesize);//
                $show = $Page->show();
                $reg_coin_detail = M('account_log')->where("loginname='{$_SESSION['nvip_nvip_member_User']}' AND amount_type=8 AND change_time between $from_time and $until_time")->order("id desc")->limit($Page->firstRow . ',' . $Page->listRows)->select();
                $this->assign("page", $show);
                $this->assign('reg_coin_detail', $reg_coin_detail);
                $this->display();
                die;
            }
        }

        $pagesize = 10;
        $count = M('account_log')->where("loginname='{$_SESSION['nvip_nvip_member_User']}' AND amount_type=8")->count();
        $Page = Page($count, $pagesize);//
        $show = $Page->show();
        $reg_coin_detail = M('account_log')->where("loginname='{$_SESSION['nvip_nvip_member_User']}' AND amount_type=8")->order("id desc")->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $this->assign('reg_coin_detail', $reg_coin_detail);
        $this->assign("page", $show);
        $this->display();
    }
    /**
     * 修改密码
     */
    public function UpdatePass()
    {

        $this->LoginTrue();
        $users = M("users");
        $bianhao = session("nvip_nvip_member_User");
        $this->assign("bianhao", $bianhao);
        $this->display();
    }

    public function UpdatepassAction()
    {
        $this->LoginTrue();
        $users = M("users");
        $id = session("member_id");
        $act = $_POST['act'];
        $oldpass = !isset($_POST['txt_oldpwd']) ? $this->error("原始密码不能为空") : trim($_POST['txt_oldpwd']);
        $txt_pwd = !isset($_POST['txt_pwd']) ? $this->error("原始密码不能为空") : trim($_POST['txt_pwd']);
        $txt_newpwd = !isset($_POST['txt_newpwd']) ? $this->error("原始密码不能为空") : trim($_POST['txt_newpwd']);
        if ($txt_pwd != $txt_newpwd) {
            $this->error("两次输入的密码不一致");
        }
        if ($act == "up1") {
            //修改一级密码
            $userinfo = $users->field("id,pwd1")->where("id={$id}")->find();


            if ($userinfo['pwd1'] == md5($oldpass)) {
                unset($data);
                $data['pwd1'] = md5($txt_pwd);

                if ($users->where("id={$id}")->save($data)) {
                    $this->success("修改一级密码成功");
                } else {
                    $this->error("两次修改的密码一致");
                }
            } else {
                $this->error("原始密码输入错误1");
            }

        } else {
            //修改二级密码
            $userinfo = $users->field("id,pwd2")->where("id={$id}")->find();
            if ($userinfo['pwd2'] == md5($oldpass)) {
                unset($data);
                $data['pwd2'] = md5($txt_pwd);
                if ($users->where("id={$id}")->save($data)) {
                    $this->success("修改二级密码成功");
                } else {
                    $this->error("两次修改的密码一致");
                }
            } else {
                $this->error("原始密码输入错误");
            }
        }


    }


    /**
     * 修改用户资料 --pp
     */
    public function Update_user()
    {
		
        if (IS_POST) {
            $loginname = session("nvip_nvip_member_User");
            //验证身份证是不是重复
            $id_sfz = $_POST['txt_identityid'];
            if (M('users')->where("identityid='$id_sfz' AND loginname<>'$loginname'")->find()) {
                $this->success('身份证号已被注册,请更换');
                die;
            }
            $data = array(
                'truename' => $_POST['txt_truename'],
                'identityid' => $_POST['txt_identityid'],
                'tel' => $_POST['txt_tel'],
                'shouhuo_name' => $_POST['txt_shouhuo_name'],
                'shouhuo_tel' => $_POST['txt_shouhuo_tel'],
                'address' => $_POST['txt_address'],
            );
            $where=array('loginname'=>$loginname);
            
            M('users')->where($where)->save($data);
            $this->success('会员信息更新成功');
            die;
        }
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $upinfo_pass = session("upinfo_pass");
		if($upinfo_pass == 1){
			$users = M("users");
			$userarr = $users->where("loginname='{$loginname}'")->find();
			$this->assign("userarr", $userarr);
		}
        $this->assign("ispassword", $upinfo_pass);

        $this->display();
    }

    /**
     * 银行信息 --pp
     */
    public function bank_info()
    {
        if (IS_POST) {
            $data = array(
                'truename' => $_POST['txt_truename'],
                'bank' => $_POST['txt_bank'],
                'bankno' => $_POST['txt_bankno'],
            );
            if (M('users')->where("loginname = '{$_SESSION['nvip_nvip_member_User']}'")->save($data)) {
                $this->success('银行信息更新成功');
                die;
            } else {
                $this->success('银行信息更新失败');
                die;
            }
            die();
        }
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $users = M("users");
        $userarr = $users->where("loginname='{$loginname}'")->find();
        $this->assign("userarr", $userarr);

        $this->display();
    }

    /**
     * 修改密码 登录密码--pp
     */
    public function update_password()
    {
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $users = M("users");
        $userarr = $users->where("loginname='{$loginname}'")->find();
        if (IS_POST) {
            $pwd1_now = $_POST['pwd1_now'];
            if (!(md5($pwd1_now) == $userarr['pwd1'])) {
                $this->success('原登录密码正确');
                die;
            }
            if (!($_POST['pwd1'] == $_POST['pwd1_confirm'])) {
                $this->success('两次密码不一致,请重新输入');
                die;
            }
            $data = array(
                'pwd1' => md5($_POST['pwd1'])
            );
            if (M('users')->where("loginname='$loginname'")->save($data)) {
                $this->success('登录密码修改成功');
                die;
            } else {
                $this->success('登录密码修改失败');
                die;
            }
            die;
        }

        $this->assign("userarr", $userarr);

        $this->display();
    }

    /**
     * 修改密码 二级密码--pp
     */
    public function update_pwd2()
    {
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $users = M("users");
        $userarr = $users->where("loginname='{$loginname}'")->find();
        if (IS_POST) {
            $pwd1_now = $_POST['pwd1_now'];
            if (!(md5($pwd1_now) == $userarr['pwd2'])) {
                $this->success('原二级密码正确');
                die;
            }
            if (!($_POST['pwd1'] == $_POST['pwd1_confirm'])) {
                $this->success('两次密码不一致,请重新输入');
                die;
            }
            $data = array(
                'pwd2' => md5($_POST['pwd1'])
            );
            if (M('users')->where("loginname='$loginname'")->save($data)) {
                $this->success('二级密码修改成功');
                die;
            } else {
                $this->success('二级密码修改失败');
                die;
            }
            die;
        }

        $this->assign("userarr", $userarr);

        $this->display();
    }

    /**
     * 密保添加--pp
     */
    public function password_protect_set()
    {
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $users = M("users");
        $userarr = $users->where("loginname='{$loginname}'")->find();
        if (($userarr['question1_answer'] || $userarr['question2_answer'] || $userarr['question3_answer']) && $_SESSION['again_set'] != '1') {
            $_SESSION['again_set'] == '';
            $this->success('您已设置过密保,验证后方可修改,正在跳转到密保验证页面', U("User/password_protect_update"));
            die;
        }
        $this->assign("userarr", $userarr);
        if (IS_POST) {
            if ($_POST['question1_answer'] == '' || $_POST['question2_answer'] == '' || $_POST['question3_answer'] == '') {
                $this->success('请将密保答案全部完善');
                die;
            }
            $arr = array($_POST['question1'], $_POST['question2'], $_POST['question3']);
            if (count($arr) != count(array_unique($arr))) {
                $this->success('请选择不同的密保问题,不要重复');
                die;
            }
            $data = array(
                'question1' => $_POST['question1'],
                'question2' => $_POST['question2'],
                'question3' => $_POST['question3'],
                'question1_answer' => $_POST['question1_answer'],
                'question2_answer' => $_POST['question2_answer'],
                'question3_answer' => $_POST['question3_answer'],
            );
            if (M('users')->where("loginname='{$loginname}'")->save($data)) {
                $_SESSION['again_set'] = '';
                $this->success('密保设置成功,请牢记');
                die;
            } else {
                $this->success('密保未改变');
                die;
            }
            die;
        }

        $this->display();
    }

    /**
     * 密保修改先验证--pp
     */
    public function password_protect_update()
    {
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $users = M("users");
        $userarr = $users->where("loginname='{$loginname}'")->find();
        $this->assign("userarr", $userarr);
        if (IS_POST) {
            if ($_POST['question1_answer'] == '' || $_POST['question2_answer'] == '' || $_POST['question3_answer'] == '') {
                $this->success('请将密保答案全部完善');
                die;
            }
            if ($userarr['question1_answer'] == $_POST['question1_answer'] && $userarr['question2_answer'] == $_POST['question2_answer'] && $userarr['question3_answer'] == $_POST['question3_answer']) {
                $_SESSION['again_set'] = 1;  //传递标识表示重新修改
                $this->success('密保验证成功,跳转到修改密保页面', U("User/password_protect_set"));
                die;
            } else {
                $this->success('密保验证失败,请开启大脑回忆模式');
                die;
            }
            die;
        }

        $this->display();
    }

    /**
     * 申请报单中心--pp
     */
    public function apply_baodan()
    {
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $users = M("users");
        $uinfo = $users->where("loginname='{$loginname}'")->find();
        $active_info = $this->sys_set();
        if (IS_POST) {
            if ($uinfo['invite_count'] < $active_info['baodan_pcount']) {
                $this->error('您需要直接推荐' . $active_info['baodan_pcount'] . '人才能申请报单中哦,当前已直推' . $uinfo['invite_count'] . '人');
                die;
            } else {
                $data['apply_desc'] = $_POST['apply_desc'] ? $_POST['apply_desc'] : '';
                $data['loginname'] = $loginname;
                $data['apply_time'] = time();
                $data['current_invite_count'] = $uinfo['invite_count'];
                $data['states'] = 0;
                $is_apply_baodan = M('apply_baodan_log')->where("loginname='{$loginname}'")->find();
                if ($is_apply_baodan) {
                    M('apply_baodan_log')->where("loginname='{$loginname}'")->save($data);
                } else {
                    M('apply_baodan_log')->add($data);
                }


                $this->success('报单中心申请成功,等待后台审核');
                die;
            }
        }

        $this->assign("baodan_pcount", $active_info['baodan_pcount']);
        $this->assign("userarr", $uinfo);
        if ($uinfo['invite_count'] >= $active_info['baodan_pcount']) {
            //查看是不是申请过报单
            $is_apply_baodan = M('apply_baodan_log')->where("loginname='{$loginname}'")->find();
            if ($is_apply_baodan && $is_apply_baodan['states'] <> 1) {
                $this->assign("baodan_info", $is_apply_baodan);
                $this->assign("is_apply_baodan", 1);
            }
            $this->display();
        } else {
            $this->error('您需要直接推荐' . $active_info['baodan_pcount'] . '人才能申请报单中哦,当前已直推' . $uinfo['invite_count'] . '人');
            die;
        }
    }

    /**
     * 检查pwd1一级密码
     */
    public function check_pwd1()
    {
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $users = M("users");
        $userarr = $users->where("loginname='{$loginname}'")->find();
        if (md5($_GET['pwd1']) == $userarr['pwd1']) {
            echo '1';
            die;
        } else {
            echo '0';
            die;
        }
    }

    /**
     * 检查pwd1二级密码
     */
    public function check_pwd2()
    {
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $users = M("users");
        $userarr = $users->where("loginname='{$loginname}'")->find();
        if (md5($_GET['pwd1']) == $userarr['pwd2']) {
            echo '1';
            die;
        } else {
            echo '0';
            die;
        }
    }

    public function Check_up_user()
    {
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $users = M('users');
        $userapp = array(
            'tel' => $_POST['txt_tel']

        );
        $res = $users->where('loginname=' . $loginname)->save($userapp);
        if ($res == 1) {
            $this->success("信息修改成功", U("Update_user"));
        } else {
            $this->redirect('User/Update_user', '', 1, '信息修改失败');
        }

    }

    /*
     * 收款账号
     * */
    public function Skzhanghao()
    {

        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $users = M("users");
        $userarr = $users->where('loginname=' . $loginname)->find();
        $dengji = $userarr['standardlevel'];
        $this->assign("userarr", $userarr);
        $this->assign("dengji", $dengji);
        $this->display();


    }


    /*
     * 奖金兑换
     * */
    public function bonusTob()
    {
        $this->LoginTrue();
        $loginname = session("nvip_member_User");

        $user = M("users");
        $userModel = $user->field("id,amount,baodanAmount,loginname")->where("loginname='{$loginname}'")->find();


        $this->assign("userarr", $userModel);
        $this->display();

    }

    /*
     * 兑换操作
     * */
    public function ActOpBonusToB()
    {
        $this->LoginTrue();
        $loginname = session("nvip_member_User");
        $user = M("users");
        $userModel = $user->field("id,amount,baodanAmount,loginname")->where("loginname='{$loginname}'")->find();

        $incoin = abs($_POST['txt_baodanCoin']);
        if (!$incoin) {
            $this->error("信息输入有误");
        }
        if ($incoin > $userModel['amount']) {
            $this->error("金额输入有误");
        }

        //金额减少
        //useramount_change($id,$loginname,$curlevel,$baodanAmount,$amount,$reason,$typedesc,$desc,$change_type,$fafang=0){
        $this->useramount_change($userModel['id'], $userModel['loginname'], $userModel['standardlevel'], 0, ($incoin * -1), "", '奖金兑换', '积分兑换报单币', 3);

        //报单币增加
        $this->useramount_change($userModel['id'], $userModel['loginname'], $userModel['standardlevel'], $incoin, 0, "", '奖金兑换', '积分兑换报单币', 102);

        $this->success("奖金兑换成功", U("Tobloglists"));

    }


    /*
     * 奖金兑换记录
     * */
    public function Tobloglists()
    {
        $this->LoginTrue();
        $loginname = session("nvip_member_User");

        $income = M("income");

        $where = "change_type=102";

        $from = isset($_POST['from']) ? strtotime(trim($_POST['from'])) : "";
        $until = isset($_POST['until']) ? strtotime(trim($_POST['until'])) : "";


        $where .= $from ? " AND addtime >= '{$from}'" : "";
        $where .= $until ? " AND addtime <= '{$until}'" : "";

        $count = $income->where($where)->count();

        $Page = Page($count, $this->pagesize);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();


        $incomeLists = $income->where($where)->order("id desc")->limit($Page->firstRow . "," . $Page->listRows)->select();

        $this->assign("bonusList", $incomeLists);
        $this->assign("page", $show);

        $this->display();

    }


    /*
     * 账户转账  -- pp
     * */
    public function zhuanZ()
    {
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $user = M("users");
        $userModel = $user->field("id,amount,loginname")->where("loginname='{$loginname}'")->find();
        $int = floor(($userModel['amount'] / 100)) * 100;
        $this->assign("userarr", $userModel);
        $this->assign("int", $int);

        $this->display();
    }

    /*
    * 账户转账处理  -- pp
    * */
    public function zhuanZOP()
    {
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $user_info = M('users')->where("loginname='{$loginname}'")->find();
        $int = floor(($user_info['amount'] / 100)) * 100;
        $incoin = $_POST["txt_baodanCoin"];
        $username = $_POST["toUser"];
//        print_r($_POST);die;
//        echo  "toUser , txt_baodanCoin";
        if ($incoin <= 0 || $incoin > $int) {
            $this->error("转账注册币输入错误");
            die;
        }
        if (!$username) {
            $this->error("请输入要转给的用户名");
            die;
        }
        $user_toUser = M('users')->where("loginname='{$username}'")->find();
        if (!$user_toUser) {
            $this->error("输入的用户名不存在");
            die;
        }
        $this->user_account_log($loginname, $user_info['id'], $user_toUser['id'], -$incoin, '用户' . $loginname . '给' . $user_toUser['loginname'] . '注册币转账', 1, 5);   //转账人减钱
        $this->user_account_log($user_toUser['loginname'], $user_toUser['id'], $user_info['id'], $incoin, '用户' . $loginname . '给' . $user_toUser['loginname'] . '注册币转账', 1, 6);   //被转账人得钱
        $this->success("转账成功", U("reg_coin_detail"));
    }

    /**
     * 奖金转注册币   --pp
     */
    public function change_bonus()
    {
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $user = M("users");
        $userModel = $user->field("id,amount,loginname,bonus_amount")->where("loginname='{$loginname}'")->find();
        $int = floor(($userModel['bonus_amount'] / 100)) * 100;
        $active_info = $this->sys_set();//获取比例
        $this->assign("bonus_ratio", $active_info['bonus_ratio']); //兑换奖金比例
        $this->assign("userarr", $userModel);
        $this->assign("int", $int);
        $this->display();
    }

    /**
     * 奖金转注册币处理   --pp
     */
    public function change_bonus_deail()
    {
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $user_info = M('users')->where("loginname='{$loginname}'")->find();
        $active_info = $this->sys_set();//获取比例
        $int = floor(($user_info['bonus_amount'] / 100)) * 100;
        $incoin = $_POST["txt_baodanCoin"];
//        print_r($_POST);die;
//        echo  "toUser , txt_baodanCoin";
        if ($incoin <= 0 || $incoin > $int) {
            $this->error("奖金兑换的输入错误");
            die;
        }
        $get_reg_coin = ($incoin * $active_info['bonus_ratio']) / 100;
        $this->user_account_log_bonus($loginname, $user_info['id'], $user_info['id'], -$incoin, -$incoin, '奖金转注册币-奖金减少', 2, 7);//奖金减少
        $this->user_account_log($loginname, $user_info['id'], $user_info['id'], $get_reg_coin, '奖金转注册币-注册币增加', 1, 8);   //奖金兑换注册币---注册币加钱
        $this->success("奖金兑换成功", U("bonus_detail"));
    }

    /**
     * 检测转账的用户是不是存在  --  pp
     */
    public function check_touser_pp()
    {
        $username = $_GET['username'];
        $user_Info = M('users')->field("id,truename")->where("loginname='{$username}'")->find();
        if (!$user_Info) {
            echo 'nouser';
            die;
        } else {
            echo $user_Info['truename'];
            die;
        }
    }

    public function checkTouser()
    {

        $username = $_POST['username'];
        $result = $this->is_checkTouser($username);
        echo json_encode($result);
        exit;
    }


    /*
     * 账户转账
     * */
    public function zhuanZloglists()
    {
        $this->LoginTrue();
        $loginname = session("nvip_member_User");
        $user = M("users");
        $userModel = $user->field("id,amount,baodanAmount,loginname")->where("loginname='{$loginname}'")->find();
        $changeMoney = M("change_money");

        $where = " 1=1 ";

        $from = isset($_POST['from']) ? strtotime(trim($_POST['from'])) : "";
        $until = isset($_POST['until']) ? strtotime(trim($_POST['until'])) : "";

        $types = isset($_POST["types"]) ? intval($_POST['types']) : 0;


        $where .= $from ? " AND change_time >= '{$from}'" : "";
        $where .= $until ? " AND change_time <= '{$until}'" : "";
        $where .= $types ? ($types == 2 ? " and to_userid='$loginname' " : "  and send_userid='$loginname' ") : " and (send_userid={$loginname} or to_userid={$loginname}) ";


        $count = $changeMoney->where($where)->count();

        $Page = Page($count, $this->pagesize);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();

        $changelists = $changeMoney->where($where)->order("id desc")->limit($Page->firstRow . "," . $Page->listRows)->select();


        $this->assign("bonusList", $changelists);

        $this->assign("page", $show);


        $this->display();
    }


    //会员关系图 akcsbnb
    public function relation()
    {
        $this->LoginTrue();

        $users = M("users");

        $loginname = isset($_POST['search_loginname']) ? $_POST['search_loginname'] : session("nvip_nvip_member_User");

        $top_info = $users->field("id,pid,rid,loginname,standardlevel")->where("loginname='{$loginname}'")->find();
//		print_r($top_info);die;
        //$a_lists[$top_info['loginname']]['name'] = $top_info['loginname'];

        $a_lists = $this->getZhituiTree($top_info, $users);
        $top_info['childrencount'] = count($a_lists);
        $top_info['level'] = GetLevel($top_info["standardlevel"]);
        //$a_lists[$top_info['loginname']]['childrencount'] = count($str);
        //$a_lists[$top_info['loginname']]['children'] = $str;

//		 print_r($a_lists);

        $this->assign("a_lists", $a_lists);
        $this->assign("top_info", $top_info);

        $this->display();
    }


    /**
     *  会员系谱图 - pp
     **/
    public function Affectlists()
    {
        $this->LoginTrue();
        if ($_GET['loginname']) {
            //先判断get到的用户名能不能被查询--一条绳上的蚂蚱才行
            $loginname = $_GET['loginname'];
            $get_name_info = M('users')->where("loginname='{$loginname}'")->field('ppath')->find();
            $get_user_name_pid = explode(',', $get_name_info['ppath']);

            $user_current_name = session("nvip_nvip_member_User");
            $user_current = M('users')->where("loginname='{$user_current_name}'")->find();
            //获取一下id为1的内置账号
            $uuuserid = M('users')->where('id=1')->find();
            if ($user_current['loginname'] == $uuuserid['loginname']) {
                $user_current['ppath'] = 1;  //系统内置的不能算
            }
            $user_current_pid = explode(',', $user_current['ppath']);

            if (!($get_user_name_pid[0] == $user_current_pid[0])) {
                $this->error("无权查看此人");
                die;
            }
            $this->assign('back', 1);
        } else {
            $loginname = session("nvip_nvip_member_User");
        }
        $users = M("users");
        $userarr = $users->where("loginname='{$loginname}'")->find();
        //只有报单中心才能点击链接
        if ($userarr['is_baodan']) {
            $this->assign('link_display', 1);
        }
        $left_self_count = M('users')->where("pid='{$loginname}' AND area=1")->count();
        $this->assign("left_self_count", $left_self_count);
        $right_self_count = M('users')->where("pid='{$loginname}' AND area=2")->count();
        $this->assign("right_self_count", $right_self_count);
        $three_self_count = M('users')->where("pid='{$loginname}' AND area=3")->count();
        $this->assign("three_self_count", $three_self_count);
        $this->assign("userarr", $userarr); //自己的信息
        //获取一部
        $left_area = M('users')->where("pid='{$loginname}' AND area=1")->find();
        $left_left_count = M('users')->where("pid='{$left_area['loginname']}' AND area=1")->count();
        $this->assign("left_left_count", $left_left_count);
        $left_right_count = M('users')->where("pid='{$left_area['loginname']}' AND area=2")->count();
        $this->assign("left_right_count", $left_right_count);
        $left_three_count = M('users')->where("pid='{$left_area['loginname']}' AND area=3")->count();
        $this->assign("left_three_count", $left_three_count);
        $this->assign("left_area", $left_area);
        //获取二部
        $right_area = M('users')->where("pid='{$loginname}' AND area=2")->find();
        $right_left_count = M('users')->where("pid='{$right_area['loginname']}' AND area=1")->count();
        $this->assign("right_left_count", $right_left_count);
        $right_right_count = M('users')->where("pid='{$right_area['loginname']}' AND area=2")->count();
        $this->assign("right_right_count", $right_right_count);
        $right_three_count = M('users')->where("pid='{$right_area['loginname']}' AND area=3")->count();
        $this->assign("right_three_count", $right_three_count);
        $this->assign("right_area", $right_area);
        //获取三部-------直接做判断---七星总监方可显示
        if ($userarr['standardlevel'] > 7) {
            $this->assign('star_level', 7);
            $three_area = M('users')->where("pid='{$loginname}' AND area=3")->find();
            $three_left_count = M('users')->where("pid='{$three_area['loginname']}' AND area=1")->count();
            $this->assign("three_left_count", $three_left_count);
            $three_right_count = M('users')->where("pid='{$three_area['loginname']}' AND area=2")->count();
            $this->assign("three_right_count", $three_right_count);
            $three_three_count = M('users')->where("pid='{$three_area['loginname']}' AND area=3")->count();
            $this->assign("three_three_count", $three_three_count);
            $this->assign("three_area", $three_area);
        }
        $this->display();
        die;
    }

    /**
     * 激活记录
     */
    public function active_log()
    {
        $this->LoginTrue();
        $loginname = session("nvip_nvip_member_User");
        $info = M()->table('nv_account_log  as  a')->join('nv_users  as  u  on  a.reason_user_id = u.id')->where("a.loginname='{$loginname}' AND a.change_type='88'")->order('a.id desc')->select();
        $this->assign('info', $info);
        $this->display();
    }

    /*
    * 汇款记录
    */
    public function Huikuanlog()
    {
        $hkdengji = M("hkdengji");

        $userid = session("nvip_nvip_member_User");
        $from = I("from") ? strtotime(I("from")) : "";
        $until = I("until") ? strtotime(I("until")) : "";

        $where = " 1=1 and userid='{$userid}'";


        $where .= $from ? " AND adddate >= $from " : "";
        $where .= $until ? " AND adddate <= $from " : "";


        $count = $hkdengji->where($where)->count();

        $Page = Page($count, $this->pagesize);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();


        $bonusList = $hkdengji->where($where)->order("id desc")->limit($Page->firstRow . ',' . $Page->listRows)->select();


        $this->assign("bonusList", $bonusList);
        $this->assign("page", $show);

        $this->display();

    }


    public function order_info()
    {

        $level = M('users')->where("loginname = {$_SESSION['nvip_nvip_member_User']}")->find();

        $this->assign("level", $level);


        if ($_POST) {
            if (!$_POST['userid']) {

                $this->error("您输入的用户名不存在");
            }

            $level = M('users')->where("loginname = {$_SESSION['nvip_nvip_member_User']}")->find();
            $data['userid'] = $_POST['userid'];
            $data['taobao'] = $_POST['taobao'];
            $data['zhifubao'] = $_POST['zhifubao'];
            $data['order_id'] = $_POST['order_id'];
            $data['order_jine'] = $_POST['order_jine'];
            $data['order_wupin'] = $_POST['order_wupin'];
            $data['order_dianpu'] = $_POST['order_dianpu'];
            $data['tijiaodate'] = time();
            $qwe['order_time'] = $_POST['order_nian'] . '-' . $_POST['order_yue'] . '-' . $_POST['order_ri'] . ' ' . $_POST['order_shi'] . ':' . $_POST['order_fen'] . ':' . $_POST['order_miao'];
            $data['order_time'] = strtotime($qwe['order_time']);
            $data['is_chuli'] = 0;

            $data['users_level'] = $level['standardlevel'];

            if ($_POST && M('order_info')->add($data)) {

            }

            $arr['keyongedu'] = $level['keyongedu'] - $_POST['order_jine'];
            $edu = M('users')->where("loginname = {$_SESSION['nvip_nvip_member_User']}")->save($arr);

            if ($edu) {
                $this->success("订单提交成功,请耐心等待审核", U("xiangqing"));
                exit;
            }


        }

        $this->display();

    }

    public function xiangqing()
    {

        $xiang = M('order_info');

        $where .= "userid = {$_SESSION['nvip_nvip_member_User']}  and 1=1";

        $count = $xiang->where($where)->count();

        $Page = Page($count, $this->pagesize);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();


        $row = $xiang->where($where)->order("id desc")->limit($Page->firstRow . ',' . $Page->listRows)->select();


        $this->assign("row", $row);
        $this->assign("page", $show);
        $this->display();
    }

    public function check_orderamount()
    {

        $user = M('users');
        $info = $user->where("loginname='{$_POST[username]}'")->field('keyongedu')->find();
        if ($info) {
            echo $info['keyongedu'];
        }
        exit;


    }

    // public function Remit(){
    // $data['userid']=$_POST['loginname'];
    // $data['nian']=$_POST['nian'];
    // $data['yue']=$_POST['yue'];
    // $data['ri']=$_POST['ri'];
    // $data['shi']=$_POST['shi'];
    // $data['fen']=$_POST['fen'];
    // $data['jine']=$_POST['jine'];
    // $data['xingming']=$_POST['xingming'];
    // $data['yinhang']=$_POST['yinhang'];
    // $data['tel']=$_POST['tel'];
    // $data['zhanghao']=$_POST['zhanghao'];
    // $data['content']=$_POST['content'];
    // $data["adddate"]=date('Y-m-d H:i:s');


    // if ($_POST && M('hkdengji')->add($data)) {

    // }
    // $this->assign("time",time());


    // $this->display();

    // }

    public function Remit()
    {

        $info = M("users")->where("loginname = {$_SESSION['nvip_nvip_member_User']}")->find();
        // $sys = M("system")->where("sId= 1") ->find;
        if ($info['standardlevel'] == 3 && $info["states"] == 1) {
            $this->error("您已经是最高级别了", U("Index/welcome"));
        }

        if ($info) {
            if ($info["states"]) {
                switch ($info['standardlevel'] + 1) {
                    case '1':
                        $sys = $this->Vip1;
                        $kyed = $this->Tongka;
                        break;
                    case '2':
                        $sys = $this->Vip2;
                        $kyed = $this->Yinka;
                        break;
                    case '3':
                        $sys = $this->Vip3;
                        $kyed = $this->Jinka;
                        break;

                }
            } else {

                switch ($info['standardlevel']) {
                    case '1':
                        $sys = $this->Vip1;
                        $kyed = $this->Tongka;
                        break;
                    case '2':
                        $sys = $this->Vip2;
                        $kyed = $this->Yinka;
                        break;
                    case '3':
                        $sys = $this->Vip3;
                        $kyed = $this->Jinka;
                        break;

                }
            }
        }
        if ($_POST) {

            $data['level'] = $info['standardlevel'];
            $data['new_level'] = $info['standardlevel'] + 1;
            $data['userid'] = $_POST['loginname'];
            $data['nian'] = $_POST['nian'];
            $data['yue'] = $_POST['yue'];
            $data['ri'] = $_POST['ri'];
            $data['shi'] = $_POST['shi'];
            $data['fen'] = $_POST['fen'];
            $data['jine'] = $_POST['jine'];
            $data['xingming'] = $_POST['xingming'];
            $data['yinhang'] = $_POST['yinhang'];
            $data['tel'] = $_POST['tel'];
            $data['zhanghao'] = $_POST['zhanghao'];
            $data['content'] = $_POST['content'];
            $data["adddate"] = time();
            $data["keyongedu"] = $kyed;


            if (!$info["states"]) {
                //激活
                $data["keyongedu"] = 0;
                $data['new_level'] = $info['standardlevel'];
            }


            $tujiao = M('hkdengji')->add($data);


            if ($tujiao) {

                $this->error("申请成功,请耐心等待审核", U("Index/welcome"));

            }

        }

        $this->assign("time", time());
        $this->assign("info", $info);
        $this->assign("sys", $sys);
        $this->display();

    }


    public function shengjilog()
    {

        $info = M("users")->where("loginname = {$_SESSION['nvip_nvip_member_User']}")->find();
        $sum = M("order_info")->where("userid= {$_SESSION['nvip_nvip_member_User']}")->sum("order_jine");
        $array = M("hkdengji")->where("userid = {$_SESSION['nvip_nvip_member_User']}")->select();

        // var_dump($array);die();
        $zong = $info["keyongedu"] + $sum;
        $this->assign("info", $info);
        $this->assign("sum", $sum);
        $this->assign("array", $array);
        // $this->assign("zong",$zong);


        $this->display();


    }

    public function remit_shanchu()
    {

        $stId = $_GET["stId"];
        $staffShow = M("users");

        //删除临时会员
        if ($stId) {
            $loginname = $staffShow->field('loginname')->where("id='$stId'")->find();

            $staffShow->where("id='$stId' AND states=0")->delete();
            if ($staffShow) {
                $this->success("临时会员{$loginname['loginname']}删除成功");
                exit;
            }

        }
    }
    public function moshi()
    {
		
        $id=100;
        $order='id desc';
        $new=M('news')->order($order)->select($id);
		// print_r($new);die;
		$new['content'] = htmlspecialchars_decode($new['content']);
        $this->assign('new',$new);
        $this->display();
	}
	
	
    public function gonggao()
    {
		
        $this->assign('news_list',M('news')->where("id <> 100")->order(array('id'=>'desc'))->select());
        $this->display("news_list");
	}

    public function newsdetail()
    {
		
        $id=$_REQUEST['id'];
        $order='id desc';
        $new=M('news')->order($order)->select($id);
		// print_r($new);die;
        $this->assign('new',$new);
        $this->display();
	}

}