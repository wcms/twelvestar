<?php
namespace Home\Controller;
use Think\Controller;
header("content-type:text/html;charset=utf-8");
class GoodsController extends LoginTrueController {
    public function __construct()
    {
        parent::__construct();

    }
    public function goods_list(){
        $goods_info=M('goods')->where("goods_type=2")->select();
        $this->assign("goods_info",$goods_info);
        $this->display();
    }
    public function goods_info(){
        $this->check_linshi();
        if ($_GET['goods_id']){
            //获取用户的地址信息
            $user_info=M('users')->where("loginname='$_SESSION[nvip_nvip_member_User]'")->find();
            if((!$user_info['address']) || (!$user_info['shouhuo_name']) ||(!$user_info['shouhuo_tel'])){
                $this->success('请先完善您的收货信息',U('User/update_user'));die;
            }
            $id=$_GET['goods_id'];
            $goods_info=M('goods')->where("goods_id='{$id}'")->find();
            $this->assign('goods_info',$goods_info);
            $this->assign('shop_price',$goods_info['goods_price']+100);
			
			$goods_num = M("order_info")->where("goods_id=$id and pay_time!=''")->sum("goods_number");
			$this->assign('goods_num',$goods_num);
            $this->display();die;
        }
        $this->display();
	}
    public function goods_buy(){
        
        $this->LoginTrue();

        
        if ($_GET['goods_id']){
            $id=$_GET['goods_id'];
            $goods_info=M('goods')->where("goods_id='{$id}'")->find();
            $this->assign('goods_info',$goods_info);
            $this->assign('shop_price',$goods_info['goods_price']+100);
            //获取用户的地址信息
            $user_info=M('users')->where("loginname='$_SESSION[nvip_nvip_member_User]'")->find();
            if((!$user_info['address']) || (!$user_info['shouhuo_name']) ||(!$user_info['shouhuo_tel'])){
                $this->success('请先完善您的收货信息',U('User/update_user'));die;
            }
            $this->assign('user_info',$user_info);
            $this->display();die;
        }else{
            $this->success('访问出错');
        }
    }

    public function goods_order(){
        if($_POST['goods_id'] && $_POST['goods_number']){
            do{
                $order_sn=$this->get_order_sn();
                $order_sn_table=M('order_info')->where("order_sn='$order_sn'")->field('order_sn')->find();
            }
            while($order_sn_table['order_sn']);
            //传递订单号
            $this->assign('order_sn',$order_sn);
            //获取用户的地址信息
            $user_info=M('users')->where("loginname='$_SESSION[nvip_nvip_member_User]'")->find();
            $this->assign('user_info',$user_info);
            //获取商品信息
            $goods_info=M('goods')->where("goods_id='{$_POST['goods_id']}'")->find();
            $this->assign('goods_info',$goods_info);
            //获取商品总价和数量
            $total_price=$_POST['goods_number']*$goods_info['goods_price'];
            $total_gwjf=$_POST['goods_number']*$goods_info['goods_gwjf'];
            $this->assign('total_price',$total_price);
            $this->assign('total_gwjf',$total_gwjf);
			

            $this->assign('goods_number',$_POST['goods_number']);
            $this->display();die;
        }elseif($_GET['pwd2'] == 'pwd2'){
            //获取二级密码
            $pwd2_array=M('users')->where("id='$_GET[user_id]'")->field('pwd2')->find();
            if (md5($_GET['password'])==$pwd2_array['pwd2']){
                echo '1';
            }else{
                echo '0';
            }
        }
    }
    public function goods_deal(){  //处理订单
      $user_id=$_GET['user_id'];
      $goods_id=$_GET['goods_id'];
      $goods_number=$_GET['goods_number'];
      $order_amount=$_GET['order_amount'];
      $order_gwjf=$_GET['order_gwjf'];
      $order_sn=$_GET['order_sn'];
     //获取用户信息
      $user_info=M('users')->where("loginname='$_SESSION[nvip_nvip_member_User]'")->find();
      if($order_amount>$user_info['bonus_amount']){
          echo '1';                            //您的余额不足
          die;
      }
	  if($order_gwjf>$user_info['gouwujf']){
          echo '2';                            //您的余额不足
          die;
      }
        //获取商品信息
      $goods_info=M('goods')->where("goods_id='$goods_id'")->find();
      $goods_day=(intval($goods_number) * intval($goods_info['goods_day']));
      //echo $goods_day;
      // if($goods_day){
          // $daoqi_time_new=($goods_day*24*60*60)+intval($user_info['daoqi_time']); //先获取当前用户的到期时间
          // $daoqi_time['daoqi_time']=$daoqi_time_new;
          // M('users')->where("id=$user_id")->save($daoqi_time);  //用户表更新一下到期的时间
      // }
        $data=array(
            'loginname'=>$user_info['loginname'],
            'order_sn'=>$order_sn,
            'order_status'=>1,
            'shipping_status'=>0,
            'pay_status'=>2,
            'order_amount'=>$order_amount,
            'order_gwjf'=>$order_gwjf,
			
            'truename'=>$user_info['shouhuo_name'],
            'address'=>$user_info['address'],
            'tel'=>$user_info['shouhuo_tel'],
            'add_time'=>time(),
            'pay_time'=>time(),
            'goods_id'=>$goods_id,
            'goods_name'=>$goods_info['goods_name'],
            'goods_number'=>$goods_number,
            'goods_price'=>$goods_info['goods_price'],
            'goods_gwjf'=>$goods_info['goods_gwjf'],
            'goods_day'=>$goods_day,  //增加的时长
        );
		 M('order_info')->startTrans();
		 
           M('order_info')->add($data);  //插入订单列表

        $this->user_account_log($user_info['loginname'],$user_id,'',-$order_amount,'订购产品'.$goods_info['goods_name'].'-'.$order_sn,'2',1);
        $this->user_account_log_gwjf($user_info['loginname'],$user_id,'',-$order_gwjf,'订购产品'.$goods_info['goods_name'].'-'.$order_sn,'8',1);

			M("users")->commit();
    }
    public  function get_order_sn()  //生成订单号
    {
        /* 选择一个随机的方案 */
        mt_srand((double) microtime() * 1000000);
        return date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
    }
    public function user_account_log($loginname,$user_id,$reason_user_id,$amount,$change_desc,$amount_type,$change_type)   //用户余额变动方法
    {
        $uinfo=M('users')->where("id = '{$user_id}'")->find();
        // $active_info = $this->sys_set();//获取比例
        $data=array(
            'loginname'=>$loginname,
            'user_id'=>$user_id,
            'reason_user_id'=>$reason_user_id,
            'amount'=>$amount,
            'change_time'=>time(),
            'change_desc'=>$change_desc,
            'amount_type'=>$amount_type,
            'change_type'=>$change_type,
            'current_amount'=>$uinfo['amount'], //当时未变化前的余额
            'current_bonus_amount'=>$uinfo['bonus_amount'], //当时奖金余额
            'bonus_ratio'=>$active_info['bonus_ratio'],
            'change_time'=>time(),
        );
        M('users')->where("id = '{$user_id}'")->setInc('bonus_amount',$amount);  //用户表
        M('account_log')->add($data);                    //用户金钱表
    }
    public function check_linshi(){
        $linshi=M('users')->field('states')->where("loginname='$_SESSION[nvip_nvip_member_User]'")->find();
        if (!$linshi['states']){
       $this->success("请先登录", U("login/index"));
       exit();
//     $this->success('您是临时会员,请先激活！',U("Index"));
        }
    }
}