<?php
namespace Home\Controller;
use Think\Controller;
header("content-type:text/html;charset=utf-8");
class IndexController extends LoginTrueController {
    public function Index(){
        $this->LoginTrue();
        $memberName = session("nvip_nvip_member_User");
        $users = M("users");
        $userModel = $users->where("loginname='{$memberName}'")->find();
        $userModel["levelName"] = GetLevel($userModel["standardlevel"]);
        $gonggao = M("news")->where("1=1")->limit('1')->select();
       
	   
        //查看有没有信件
        $znx_count=M('message')->where("states=0 AND(send_types=0 OR accept_username='{$_SESSION[nvip_nvip_member_User]}')")->order(array('id'=>'desc'))->count();
        //查看是不是申请过报单
      
        $this->assign("news_list",$gonggao);//公告
        $this->assign("user_info",$userModel);
        $this->display();
    }
	public function Indextemp(){

		$this->LoginTrueTemp();
		
		$this->display();
	}

	public function Tempinfo(){
		$this->LoginTrueTemp();

		//获取会员基本信息
		$member_id = session("nvip_membertemp_id_temp");
		$users = M("users");

		$userModel = $users->field("id,loginname,jihuoAmount,rid")->where("id=$member_id")->find();
		if(!$userModel)
		{
			$this->error("信息错误",U('/login/'));
		}
		
		$userModel['jihuoAmount'] = $userModel['jihuoAmount']? $userModel['jihuoAmount']:$jamount;

		//获取推荐人信息
		$ruser = $users->field("id,tel,loginname")->where("loginname='{$userModel[rid]}'")->find();

		$_SESSION['jihuos'] = time();

		$this->assign("jihuos",$_SESSION['jihuos']);
		$this->assign("ruser",$ruser);
		$this->assign("userModel",$userModel);



		$this->display();
	}
    public function Welcome(){
		$this->LoginTrue();
		$memberName = session("nvip_nvip_member_User");
		$users = M("users");
		$userModel = $users->where("loginname='{$memberName}'")->find();
		$userModel["levelName"] = GetLevel($userModel["standardlevel"]);
		$gonggao = M("news")->where("1=1")->limit('10')->select();
		//获取距离到期还有多少天
        if($userModel['jihuotime']){
            $info=intval($userModel['daoqi_time'])-intval($userModel['jihuotime']);
            $day_num=floor($info/(24*60*60));
            $this->assign("day_num",$day_num);
			
			if($userModel['standardlevel'] == 0){
				
				$this->assign("day_num1",'未生效');
			}else{

            //到期提醒js
            $info1=intval($userModel['daoqi_time'])-intval(time());
            $day_num1=floor($info1/(24*60*60));
			if($day_num1<0)
				$day_num1 = 0;
            $this->assign("day_num1",$day_num1);
			}
        }else{
            $this->assign("day_num",'您还未激活');
        }
        $this->assign("news_list",$gonggao);//公告
		$this->assign("user_info",$userModel);
        $this->display();
	}

}