<?php
namespace Home\Controller;
use Common\Controller\BaseController;

header("content-type:text/html;charset=utf-8");
class LoginController extends BaseController {
	
	
		
    public function Index(){
		
        $this->xitonginfo();
		$systemName=M("system");


       if(strlen(session("nvip_nvip_member_User"))>0){
          $this->success("你已登陆，正在跳转",U("Home/Index"));
       }else{
			
           $this->assign("rs_systemName",$rs_systemName);
            $year=date("Y");
            $this->assign("year",$year);
            $this->display();
       }
    }
	
	public function findpwdOp(){
		
		$mobile = $_POST['mobile'];
			$msginfo = M("msgvalidate")->where("mobile='$mobile' and ".time()."-addtime < 180")->find();
			if(!$msginfo){
				$this->error("验证码错误");
			}
			
	$code = $_POST['code'];
	$password = $_POST['password'];
		
	if($code != $msginfo['code']){
				$this->error("验证码错误");
	}
		$save = array("pwd1"=>md5($password));
		M("users")->where("loginname='$mobile'")->save($save);
		$this->success("设置密码成功",U("index"));
exit;		
	}
	public function findpwd(){
		
		$mobile = $_REQUEST['mobile_phone'];
		if($mobile){
			$msginfo = M("msgvalidate")->where("mobile='$mobile'")->find();
			if(time() - $msginfo['addtime']  < 60){
				$this->error("请稍后再操作");
			}
		$email_code = $this->rand_number(6);
		$msgtext = "【猫咪源码】您正在找回密码，验证码为".$email_code."，千万不要告诉别人哦。";
		
		if($this->SendMsg($mobile,$msgtext)){
			
			$save = array(
				"mobile" =>$mobile,
				"code" =>$email_code,
				"addtime" => time()
			);
			
			M("msgvalidate")->add($save);
			
			echo "ok";
			exit;
		}
		}
		echo "短信发送失败";
		exit;
	}
/**
 * 随机生成指定长度的数字
 *
 * @param number $length        	
 * @return number
 */
function rand_number ($length = 6)
{
	if($length < 1)
	{
		$length = 6;
	}
	
	$min = 1;
	for($i = 0; $i < $length - 1; $i ++)
	{
		$min = $min * 10;
	}
	$max = $min * 10 - 1;
	
	return rand($min, $max);
}
    public function forgetpwd(){
		
		
            $this->display();
		
	}
    public function CheckLogin(){

            $system=M("system");
            $rs_system=$system->field("sCheckCodeSwitch,sErrorPwdLockNum,sLoginTimeout")->where("sId=1")->find();

			$Verify = new \Think\Verify;
			if(!$Verify->check($_POST['Verifycode'])){
				$this->error("验证码错误！");
				return;
			}
			//获取此次登录IP信息
        $ip=$this->getIP();
         $data['logip']=$ip;
        $data['log_time']=time();
        $users=M("users");
            $UserName=$_POST["UserName"];
			if(!$UserName){
				
				$this->error("请填写用户名");
			}
            $PWD=md5($_POST["PWD"]);
            $rs=$users->field('id,pwd1,states,logincishu,loginname,standardlevel,tuijianma,lockuser')->where("loginname='{$UserName}'")->find();
            if(count($rs)>0){
				
				if($rs['lockuser'] == 1){
					$this->error("用户异常");
				}

				if($PWD==$rs["pwd1"]){
                    //加入登录日志
                    $data['loginname']=$UserName;
                    M('users_login')->add($data);
                    $cishu= M('users_login')->field("id")->count();
                    unset($data['loginname']);
//                    $aLoginNum=$rs["logincishu"]+1;
					$data["logincishu"]=$cishu;
					$data["last_logintime"]=time();
					$users->where("loginname='{$UserName}'")->save($data);

                        session("nvip_member_id",$rs["id"]);
                        session("nvip_member_tuijianma",$rs["tuijianma"]);
                        session("nvip_nvip_member_User",$rs["loginname"]);
                        session("nvip_member_time",time());
                        session("nvip_member_level",$rs['standardlevel']);
                        $_SESSION['nvip_user_expiretime'] = time() + (($rs_system["sLoginTimeout"])*60);
                        $loginTime=date("Y-m-d H:i:s");
                        session("nvip_member_loginTime",$loginTime);

                     session("nvip_member_states",$rs["states"]);

                        $this->success("登陆成功",U('/Goods/goods_list'));
                }else{
                    $this->error("密码不正确");
                    exit;
                }
            }else{
				$this->error("用户名不存在");
				exit;
            }
      }
	  
	  //用户注册
    public function UserReg(){
		
		if($this->Sys_systemName['tjcheckcode'] == 1){
			 $tuijianma = $_REQUEST['rid'];
			
			if(!$tuijianma){
				header('Location: '.U('/Login/'));
				exit;
			}
			$tuijianma = base64_decode(base64_decode($tuijianma));
			$this->assign("tuijianma", $tuijianma);
       
	
			$this->display();
			
		}
		else{
			header('Location: '.U('/Login/'));
			exit;
		}
		
		
	}
    /**
     * 注册会员提交后的处理-pp
     */
    public function Add_Action()
    {
		
        $txt_loginname = $_POST["mobile"];
        if (!$txt_loginname) {
            $this->error("手机号不能为空");
            exit();
        }
        if ($this->check_loginname($txt_loginname)) {
            $this->error("手机号重复，请重新获取!");
            exit();
        }
		if($this->Sys_systemName['sRegUserCheckCode'] == 1){
			$code = $_POST["code"];
			if (!$code) {
				$this->error("手机验证码不能为空");
				exit();
			}
			$msginfo = M("msgvalidate")->where("mobile='$txt_loginname' and ".time()."-addtime < 180")->order("id desc")->find();
			if(!$msginfo){
				$this->error("验证码错误");
			}
			
			if($code != $msginfo['code']){
						$this->error("验证码错误");
					exit();
			}
		}
	
	
        //验证身份证是不是重复
        $id_sfz = $_POST['txt_identityid'];
		$rid = $_POST['tuijianmanew'];
        if (!$rid) {
            $this->error("推荐人不能为空");
            exit();
        }
		
        if (!$_POST["realname"]) {
            $this->error("商家姓名不能为空");
            exit();
        }
        if (!$_POST["password"]) {
            $this->error("请填写密码");
            exit();
        }
        if ($_POST["cpassword"] != $_POST["password"]) {
            $this->error("两次输入的密码不一致");
            exit();
        }

		$upload = new \Think\Upload();// 实例化上传类
		$upload->rootPath = './uploads/';
		$upload->maxSize   =     3145728 ;// 设置附件上传大小
		$upload->saveName = array('uniqid','');
		$upload->exts     = array('jpg', 'gif', 'png', 'jpeg');
		$upload->autoSub  = true;
		$upload->subName  = array('date','Ymd');
		$info   =   $upload->upload();
		if(!$info) {// 上传错误提示错误信息
			$this->error($upload->getError());
		}else{// 上传成功 获取上传文件信息
			foreach($info as $file){
				$file_path =  $file['savepath'].$file['savename'];
			}
		}
        if (!$file_path) {
            $this->error("请选择微信二维码上传");
            exit();
        }
		
		
        $r_user = M("users")->where("tuijianma='{$rid}'")->find();//rpath根据推荐人来计算
		if(!$r_user){
            $this->error("推荐人信息错误");
            exit();
		}
        if ($r_user['rpath']) {
            $data['rpath'] = $r_user['rpath'] . "," . $r_user['id'];//推荐rpath
        } else {
            $data['rpath'] = $r_user['id'];//推荐rpath
        }
       
	   
        $data['wximg'] = $file_path;//微信二维码
        $data['ceng'] = $r_user_p['ceng'] + 1;//层
        $data['dai'] = $r_user['dai'] + 1;
        $data['standardlevel'] = '0';//刚开始就是临时会员
        $data["loginname"] = $txt_loginname;
        $data["tuijianma"] = $this->SysSet['inviteStart'].$this->get_random(3);;
        $data["truename"] = $_POST['realname'];
        $data["tel"] = $txt_loginname;
        $data["rid"] = $r_user['id'];
        $data["pwd1"] = md5(trim($_POST["password"]));
        $data["pwd2"] = md5(trim($_POST["password"]));
        $data["states"] = 0;
        $data["addtime"] = time();
		
		
       
        $result = M('users')->add($data);
        if ($result) {
			
            $this->assign("txt_loginname", $data['loginname']);
            $this->assign("txt_pwd1", $_POST["password"]);
            $this->assign("txt_pwd2", $_POST["password"]);
            $this->assign("rid", base64_encode($data["rid"]));
            $this->assign("result", $result);
        } else {
            $this->error("注册失败,请重新注册");
            exit();
        }


        $this->display();
    }
	public function reguser(){
		
		$mobile = $_REQUEST['mobile_phone'];
		if($mobile){
			$msginfo = M("msgvalidate")->where("mobile='$mobile'")->find();
			if(time() - $msginfo['addtime']  < 60){
				$this->error("请稍后再操作");
			}
			$email_code = $this->rand_number(6);
			$msgtext = "【创客联盟】欢迎注册，您的验证码为".$email_code."，千万不要告诉别人哦。";
		
			if($this->SendMsg($mobile,$msgtext)){
				
				$save = array(
					"mobile" =>$mobile,
					"code" =>$email_code,
					"addtime" => time()
				);
				
				M("msgvalidate")->add($save);
				
				echo "ok";
				exit;
			}
		}
		echo "短信发送失败";
		exit;
	}
}
