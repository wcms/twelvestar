<?php
namespace Home\Controller;
use Think\Controller;
header("content-type:text/html;charset=utf-8");
class MessageController extends LoginTrueController {
    public function send_zxn(){     //用户发送站内信
        if($_REQUEST['accept_username']){  //先看看是不是发站内信的操作
            $zn_info['send_types']='2';  //发送给管理员  使用types等于2
            $zn_info['accept_username']='admin';  //接收人:admin
            $zn_info['accept_title']=$_REQUEST['accept_title'];//内容标题
            $zn_info['accept_content']=$_REQUEST['accept_content'];//发送内容
            $zn_info['send_username']=$_SESSION['nvip_nvip_member_User'];//发送人姓名:为自己
            $zn_info['addtime']=date('Y-m-d H:i:s',time());//发送时间
            $zn_info['states']='0';//发送时间
            if(M('message')->add($zn_info)){
                $this->success('发送成功,返回到首页');die;
            }
        }
        $this->display();
    }
    public function znx_box(){  //用户站内信的列表
        $where['accept_username'] = $_SESSION['nvip_nvip_member_User'];
        $where['send_types'] = '0';
        $where['_logic'] = 'OR';
        $znx_info=M('message')->where($where)->order(array('id'=>'desc'))->select();
        $this->assign('znx_info',$znx_info);
        $this->display();
    }
    public function change_status(){  //是否已经阅读的修改
         $id=$_POST['id'];
         $where['id']=$id;
         $save['states']=1;
         M('message')->where($where)->save($save);
    }
    public function read_znx(){  //用户阅读站内信
        $id=$_GET['id'];
        $where['id']=$id;
        $new=M('message')->where($where)->select();

        $this->assign('new',$new);
        $this->display();
    }
    public function delete(){
        $id=$_GET['id'];
        $where['id']=$id;
        $res=M('message')->where($where)->delete();
        if($res){
            $this->success('删除成功');
        }

    }

}