<?php
namespace Home\Controller;
use Think\Controller;
header("content-type:text/html;charset=utf-8");
class TixianController extends LoginTrueController{
    //pp
    public function Index(){
        $loginname = session("nvip_nvip_member_User");
        //获取用户
        $user_info=M('users')->where("loginname='$loginname'")->find();
        if((!$user_info['truename']) || (!$user_info['bank']) ||(!$user_info['bankno'])){
            $this->success('请先完善您的银行信息',U('User/bank_info'));die;
        }
//         if(!isset($_POST['password'])){
//            $this->redirect('Tixian/tixan_denglu');
//         }else{
//             $user =  M('users');
//             $pass = $user->where("loginname='".$loginname."'AND pwd2='".md5($_POST['password'])."'")->count();
//             if(!$pass){
//                  $this->redirect('Tixian/tixan_denglu','',1,'密码错误');
//             }
//         }
        $this->assign("user_info", $user_info);
        $int=floor(($user_info['bonus_amount']/100))*100;
        $active_info = $this->sys_set();//获取比例
        $cfxf_ratio=$active_info['cfxf_ratio']/100;
        $tixian_ratio=$active_info['tixian_ratio']/100;
        $whf_ratio=$active_info['whf_ratio']/100;
        $this->assign("cfxf_ratio",$cfxf_ratio);
        $this->assign("tixian_ratio",$tixian_ratio);
        $this->assign("whf_ratio",$whf_ratio);
        $this->assign("int",$int);
        $this->assign("time",date("Y-m-d", time()) );
        if ($user_info['tixiantime']){
            $tixian_time =strtotime("+1 months", $user_info['tixiantime']);
        }else{
            $tixian_time =strtotime("+1 months", $user_info['jihuotime']);
        }
        if(time()>$tixian_time){
            $this->assign('ttime',date('Y-m-d H:i:s',$tixian_time));
            $this->assign('flag',1);
        }else{
            $this->assign('ttime',date('Y-m-d H:i:s',$tixian_time));
            $this->assign('flag',0);
        }


        $this->display();
    }
    //pp
    public function Tixan_denglu(){
        $this->display();
    }
    //pp
    public function Tixian_list()
    {
        $loginname = session("nvip_nvip_member_User");
        $user_name  = M('users');
        $whereid = $user_name->where("loginname='".$loginname."'")->find();
        $tixian = M("tixian_log");
        $where = "user_id=".$whereid['id'];
        $pagesize = 10;
        $count = $tixian->where($where)->count();
        $Page       = Page($count,$pagesize);//
        $show = $Page->show();

        $tixianList = $tixian->where($where)->order("log_id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
        $this->assign("tixianList",$tixianList);
        $this->assign("page",$show);

        $this->display();
    }
    //pp
    public function Check_tixian(){
        $loginname = session("nvip_nvip_member_User");
        $user =  M('users');
        $amount  = $user->where("loginname='{$loginname}'")->find();
        $txtamount = abs($_POST['txtamount']);
        $txtmemo =  $_POST['txtmemo']?$_POST['txtmemo']:'用户提现';
        if (fmod($txtamount,100)!=0){
            $this->error("请输入100的整数倍进行提现,并且提现金额不得小于500");die;
        }
		if($txtamount<500){
			 $this->error("提现金额不得小于500");die;
		}
        if($txtamount>$amount['bonus_amount']){
            $this->error("账户奖金不足,不能提现");die;
        }
        if(!$txtamount){
            $this->error("提现金额输入有误,无法提现");die;
        }

        $time  = time();
        $active_info = $this->sys_set();//获取比例
        //实际到账金额
        // $txtamount_shiji=($txtamount-(($txtamount*($active_info['cfxf_ratio']/100)+($txtamount*($active_info['tixian_ratio']/100))+($txtamount*($active_info['whf_ratio']/100)))));
		$txtamount_shiji=($txtamount-($txtamount*($active_info['tixian_ratio']/100)));
        $log = M("tixian_log");
        $add=array(
            'user_id'       =>  $amount['id'],
            'loginname'     =>  $amount['loginname'],
            'amount'        =>  $txtamount ,
            'real_amount'   =>  $txtamount_shiji ,
            'change_time'   =>  $time,
            'cfxf_ratio'    =>  $active_info['cfxf_ratio'],
            'tixian_ratio'  =>  $active_info['tixian_ratio'],
            'whf_ratio'     =>  $active_info['whf_ratio'],
            'change_desc'   =>  $txtmemo,
            'change_type'   =>  1,
            'states'        => 0,
            'bank'        => $amount['bank'],
            'bankno'        => $amount['bankno'],
            'truename'        => $amount['truename'],
        );
        $resultadd = $log->add($add); // 写入数据到数据库
        if(!$resultadd){
            $this->error("提现申请提交失败");
        }
        $this->user_account_log_bonus($loginname,$amount['id'],$amount['id'],-$txtamount,-$txtamount,'用户提现-奖金减少',2,9);//奖金减少
        $data['tixiantime']=time();
        M('users')->where("loginname='{$loginname}'")->save($data);
        $this->error("提现申请成功",U("Tixian/tixian_list"));die;
    }
}