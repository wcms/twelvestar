// JavaScript Document
	function provincedo(t,url){
		$("#city option").each(function(){
			$(this).remove();
		});
		$("#area option").each(function(){
			$(this).remove();
		});
		$("<option value=''>请选择</option>").appendTo($("#city"));
		$("<option value=''>请选择</option>").appendTo($("#area"));
		$.getJSON(url,{pid:t},
			function(data){
				if (data){
					$.each(data,function(t,item){
						$('<option value='+item.cityID+'>'+item.city+'</option>').appendTo($('#city'));
					});
				}
			}
		);
	}
	function citydo(t,url){
		$('#area option').each(function(){
			$(this).remove();
		});
		$("<option value=''>请选择</option>").appendTo('#area');
		$.getJSON(url,{cid:t},function(data){
			if(data){
				$.each(data,function(t,item){
					$('<option value='+item.areaID+'>'+item.area+'</option>').appendTo($('#area'));
				});
			}
		});
	}
	function checkusername(url){
		var usernameval=$.trim($('#username').val());
		if (usernameval==''){
			alert('请输入会员编号');
			return false;
		}
		var usernamepreg=/^[a-zA-Z0-9]{4,50}$/;
		if(!usernamepreg.test(usernameval)){
			alert('会员编号格式错误');
		}else{
			$.getJSON(url,{username:usernameval},function(data){
				if(data){
					alert('会员编号存在');
				}else{
					alert('会员编号不存在');	
				}
			});
		}
	}
	function checkusernameok(url){
		var usernameval=$.trim($('#username').val());
		if (usernameval==''){
			alert('请输入会员编号');
			return false;
		}
		var usernamepreg=/^[a-zA-Z0-9]{4,50}$/;
		if(!usernamepreg.test(usernameval)){
			alert('会员编号格式错误');
		}else{
			$.getJSON(url,{username:usernameval},function(data){
				if(data){
					alert('会员编号已存在');
				}else{
					alert('会员编号可以使用');	
				}
			});
		}
	}
	function checktjuser(url){
		var tjval=$.trim($('#tjuser').val());
		if (tjval==''){
			alert('请输入推荐会员');
			return false;
		}
		var usernamepreg=/^[a-zA-Z0-9]{4,50}$/;
		if(!usernamepreg.test(tjval)){
			alert('推荐会员格式错误');
		}else{
			$.getJSON(url,{username:tjval},function(data){
				if(data){
					alert('推荐会员存在');
				}else{
					alert('推荐会员不存在');	
				}
			});
		}
	}
	function checkgluser(url){
		var glval=$.trim($('#gluser').val());
		if (glval==''){
			alert('请输入管理会员');
			return false;
		}
		var usernamepreg=/^[a-zA-Z0-9]{4,50}$/;
		if(!usernamepreg.test(glval)){
			alert('管理会员格式错误');
		}else{
			$.getJSON(url,{username:glval},function(data){
				if(data){
					alert('管理会员存在');
				}else{
					alert('管理会员不存在');	
				}
			});
		}
	}
	function checkbduser(url){
		var bdval=$.trim($('#bduser').val());
		if (bdval==''){
			alert('请输入报单会员');
			return false;
		}
		var usernamepreg=/^[a-zA-Z0-9]{4,50}$/;
		if(!usernamepreg.test(bdval)){
			alert('商务中心格式错误');
		}else{
			$.getJSON(url,{username:bdval},function(data){
				if(data){
					alert('商务中心存在');
				}else{
					alert('商务中心不存在');	
				}
			});
		}
	}
	function checkmsgtohy(intp,url){
		var usernameval=$.trim($('#'+intp).val());
		if (usernameval==''){
			alert('请输入会员编号');	
			return false;
		}
		var usernamepreg=/^[a-zA-Z0-9]{4,50}$/;
		if(!usernamepreg.test(usernameval)){
			alert('会员编号格式错误');
		}else{
			$.getJSON(url,{username:usernameval},function(data){
				if(data){
					alert('会员编号存在');
				}else{
					alert('会员编号不存在');	
				}
			});
		}		
	}
	function ptypechange(intp,url){
		var ptypeval=$('#'+intp).val();
		$("#type option").each(function(){
			$(this).remove();
		});
		$("<option value=''>全部类型</option>").appendTo($("#type"));
		$.getJSON(url,{ptype:ptypeval},
			function(data){
				if (data){
					$.each(data,function(t,item){
						$('<option value='+t+'>'+item+'</option>').appendTo($('#type'));
					});
				}
			}
		);
	}
	
	function provincedoqy(t,url){
		$("#cityID option").each(function(){
			$(this).remove();
		});
		$("<option value=''>请选择</option>").appendTo($("#cityID"));
		$.getJSON(url,{pid:t},
			function(data){
				if (data){
					$.each(data,function(t,item){
						$('<option value='+item.cityID+'>'+item.city+'</option>').appendTo($('#cityID'));
					});
				}
			}
		);
	}
	
	function rankchange(t,url){
		$.getJSON(url,{rank:t},
			function(data){
				if (data>0){
					$('#tbodyps').css('display','');
				}else{
					$('#tbodyps').css('display','none');
				}
			}
		);
	}
	