-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 2019-04-08 00:40:03
-- 服务器版本： 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `star`
--

-- --------------------------------------------------------

--
-- 表的结构 `nv_account_log`
--

DROP TABLE IF EXISTS `nv_account_log`;
CREATE TABLE IF NOT EXISTS `nv_account_log` (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `loginname` varchar(20) DEFAULT NULL,
  `user_id` mediumint(8) UNSIGNED NOT NULL,
  `reason_user_id` mediumint(8) UNSIGNED NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `current_amount` decimal(10,2) NOT NULL,
  `current_bonus_amount` decimal(10,2) NOT NULL,
  `change_time` int(10) UNSIGNED NOT NULL,
  `change_desc` varchar(255) NOT NULL,
  `amount_type` varchar(255) NOT NULL,
  `change_type` tinyint(3) UNSIGNED NOT NULL,
  `bonus_ratio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- 表的结构 `nv_admin`
--

DROP TABLE IF EXISTS `nv_admin`;
CREATE TABLE IF NOT EXISTS `nv_admin` (
  `aId` int(3) NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `aUser` varchar(16) NOT NULL COMMENT '管理员用户名',
  `aName` varchar(10) NOT NULL COMMENT '管理员姓名',
  `aPwd` varchar(32) NOT NULL COMMENT '管理员密码',
  `aTel` varchar(11) NOT NULL COMMENT '管理员手机',
  `aEmail` varchar(40) DEFAULT NULL COMMENT '管理员邮箱',
  `aSex` int(1) NOT NULL COMMENT '管理员性别',
  `aPowers` int(1) NOT NULL DEFAULT '0' COMMENT '管理员级别',
  `aDid` int(8) NOT NULL DEFAULT '0' COMMENT '管理员的管理部门ID',
  `aLoginNum` int(9) NOT NULL DEFAULT '0' COMMENT '记录登陆次数',
  `aStatic` int(1) NOT NULL DEFAULT '1' COMMENT '管理员状态',
  `aErrorPwdNum` int(2) NOT NULL DEFAULT '0' COMMENT '记录密码登陆错误次数',
  `logip` varchar(20) DEFAULT NULL,
  `log_country` varchar(20) DEFAULT NULL,
  `log_area` varchar(20) DEFAULT NULL,
  `log_province` varchar(20) DEFAULT NULL,
  `log_city` varchar(20) DEFAULT NULL,
  `log_isp` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`aId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `nv_admin`
--

INSERT INTO `nv_admin` (`aId`, `aUser`, `aName`, `aPwd`, `aTel`, `aEmail`, `aSex`, `aPowers`, `aDid`, `aLoginNum`, `aStatic`, `aErrorPwdNum`, `logip`, `log_country`, `log_area`, `log_province`, `log_city`, `log_isp`) VALUES
(1, 'admin', '系统管理员', 'e10adc3949ba59abbe56e057f20f883e', '18888888', '888888@qq.com', 1, 0, 0, 18, 1, 0, '::1', '中国', '', '山东', '菏泽', '电信'),
(2, '11111', '', 'e10adc3949ba59abbe56e057f20f883e', '', NULL, 0, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `nv_admin_login`
--

DROP TABLE IF EXISTS `nv_admin_login`;
CREATE TABLE IF NOT EXISTS `nv_admin_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loginname` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `logip` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_country` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_area` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_province` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_city` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_isp` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `log_time` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `nv_admin_login`
--

INSERT INTO `nv_admin_login` (`id`, `loginname`, `logip`, `log_country`, `log_area`, `log_province`, `log_city`, `log_isp`, `log_time`) VALUES
(1, 'admin', '144.0.152.232', '中国', '', '山东', '菏泽', '电信', 1548454170),
(2, 'admin', '144.0.152.232', '中国', '', '山东', '菏泽', '电信', 1548530868),
(3, 'admin', '144.0.152.232', '中国', '', '山东', '菏泽', '电信', 1548531360),
(4, 'admin', '144.0.153.27', '中国', '', '山东', '菏泽', '电信', 1549295591),
(5, 'admin', '124.127.99.32', '中国', '', '北京', '北京', '电信', 1549297005),
(6, 'admin', '144.0.152.146', '中国', '', '山东', '菏泽', '电信', 1550298539),
(7, 'test', '144.0.152.146', '中国', '', '山东', '菏泽', '电信', 1550298814),
(8, 'test', '106.117.61.247', '中国', '', '河北', '石家庄', '电信', 1550300048),
(9, 'test', '106.117.61.247', '中国', '', '河北', '石家庄', '电信', 1550300048),
(10, 'test', '1.68.88.22', '中国', '', '山西', '太原', '电信', 1550300057),
(11, 'test', '106.117.61.247', '中国', '', '河北', '石家庄', '电信', 1550300156),
(12, 'test', '144.0.152.146', '中国', '', '山东', '菏泽', '电信', 1550300291),
(13, 'test', '144.0.152.146', '中国', '', '山东', '菏泽', '电信', 1550306614),
(14, 'test', '1.25.221.173', '中国', '', '内蒙古', '包头', '联通', 1550307747),
(15, 'test', '1.25.221.173', '中国', '', '内蒙古', '包头', '联通', 1550308315),
(16, 'test', '1.25.221.173', '中国', '', '内蒙古', '包头', '联通', 1550308370),
(17, 'test', '144.0.152.36', '中国', '', '山东', '菏泽', '电信', 1550311949),
(18, 'admin', '144.0.152.30', '中国', '', '山东', '菏泽', '电信', 1550464784);

-- --------------------------------------------------------

--
-- 表的结构 `nv_admin_role`
--

DROP TABLE IF EXISTS `nv_admin_role`;
CREATE TABLE IF NOT EXISTS `nv_admin_role` (
  `arId` int(2) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `arName` varchar(16) NOT NULL COMMENT '角色名称',
  `arInfo` varchar(200) NOT NULL COMMENT '角色描述',
  `arPowers` varchar(60) NOT NULL COMMENT '角色值',
  PRIMARY KEY (`arId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `nv_admin_role`
--

INSERT INTO `nv_admin_role` (`arId`, `arName`, `arInfo`, `arPowers`) VALUES
(1, '超级管理组', '除系统管理员外，本组拥有至高无上的权限', 'A-A1-A2-A3-B'),
(2, '系统维护组', '拥有系统设置，数据库备份等功能', 'A-A1-0-0-0');

-- --------------------------------------------------------

--
-- 表的结构 `nv_apply_baodan_log`
--

DROP TABLE IF EXISTS `nv_apply_baodan_log`;
CREATE TABLE IF NOT EXISTS `nv_apply_baodan_log` (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `loginname` varchar(20) DEFAULT NULL,
  `apply_time` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `apply_desc` varchar(255) NOT NULL DEFAULT '0',
  `current_invite_count` int(11) NOT NULL DEFAULT '0',
  `states` int(11) DEFAULT '0' COMMENT '-1表示临时会员1注册会员2VIP会员...',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `nv_department`
--

DROP TABLE IF EXISTS `nv_department`;
CREATE TABLE IF NOT EXISTS `nv_department` (
  `dId` int(3) NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `dPid` int(9) NOT NULL COMMENT '下级部门',
  `dPsid` int(9) NOT NULL COMMENT '二级部门ID',
  `dName` varchar(20) NOT NULL COMMENT '部门名称',
  `dDirector` varchar(10) NOT NULL COMMENT '部门负责人',
  `dDirectorTel` varchar(15) NOT NULL COMMENT '部门负责人电话',
  `dDirectorQQ` varchar(11) NOT NULL COMMENT '部门负责人QQ',
  `dDirectorEmail` varchar(50) NOT NULL COMMENT '部门负责人邮箱',
  `dInfo` text NOT NULL COMMENT '部门简介',
  PRIMARY KEY (`dId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `nv_goods`
--

DROP TABLE IF EXISTS `nv_goods`;
CREATE TABLE IF NOT EXISTS `nv_goods` (
  `goods_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `goods_name` varchar(120) NOT NULL DEFAULT '',
  `goods_price` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `goods_desc` text NOT NULL,
  `goods_img` varchar(255) DEFAULT NULL,
  `add_time` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `goods_day` int(11) DEFAULT '0' COMMENT '增加天数',
  `goods_type` int(11) DEFAULT '0',
  PRIMARY KEY (`goods_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `nv_goods`
--

INSERT INTO `nv_goods` (`goods_id`, `goods_name`, `goods_price`, `goods_desc`, `goods_img`, `add_time`, `goods_day`, `goods_type`) VALUES
(6, '华为 HUAWEI Mate 20 麒麟980AI智能芯片全面屏超微距影像超大广角徕卡三摄6GB+128GB亮黑色全网通版双4G手机', '3999.00', '<img src=\"/Public/Theme1/kindeediitor/attached/image/20190407/20190407055112_84847.jpg\" alt=\"\" />', '/uploads/goods/2019-04-07/5ca997a1075f8.png', 1554618273, 0, 2),
(7, '小米9 骁龙855 游戏手机 深空灰 全网通6GB+128GB', '3199.00', '<img src=\"/Public/Theme1/kindeediitor/attached/image/20190407/20190407062558_77153.png\" alt=\"\" />', '/uploads/goods/2019-04-07/5ca997f83dd10.png', 1554618360, 0, 2);

-- --------------------------------------------------------

--
-- 表的结构 `nv_hkdengji`
--

DROP TABLE IF EXISTS `nv_hkdengji`;
CREATE TABLE IF NOT EXISTS `nv_hkdengji` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `nian` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `yue` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `ri` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `shi` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `fen` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `jine` decimal(10,2) DEFAULT NULL,
  `xingming` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `yinhang` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `zhanghao` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `tel` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `pic` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `who` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `content` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `adddate` int(11) DEFAULT NULL,
  `level` int(10) DEFAULT NULL,
  `new_level` int(10) DEFAULT NULL,
  `is_chuli` int(10) DEFAULT '0',
  `keyongedu` decimal(50,4) DEFAULT '0.0000',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `nv_income`
--

DROP TABLE IF EXISTS `nv_income`;
CREATE TABLE IF NOT EXISTS `nv_income` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `types` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `jine` decimal(18,4) DEFAULT '0.0000',
  `amount` decimal(18,4) DEFAULT '0.0000',
  `nowamount` decimal(18,4) DEFAULT '0.0000',
  `addtime` int(10) DEFAULT NULL,
  `reason` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `ischuli` int(4) DEFAULT '0',
  `chulitime` int(10) DEFAULT NULL,
  `huan` decimal(18,4) DEFAULT '0.0000',
  `cfamount` decimal(18,4) DEFAULT '0.0000',
  `sfjj` int(4) DEFAULT '0',
  `zhou` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `yue` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `remark` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `nv_jiesuan_day`
--

DROP TABLE IF EXISTS `nv_jiesuan_day`;
CREATE TABLE IF NOT EXISTS `nv_jiesuan_day` (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `loginname` varchar(20) DEFAULT NULL,
  `user_id` mediumint(8) UNSIGNED NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `current_amount` decimal(10,2) DEFAULT '0.00',
  `current_bonus_amount` decimal(10,2) DEFAULT '0.00',
  `jiesuan_time` int(10) UNSIGNED DEFAULT '0',
  `jiesuan_desc` varchar(255) NOT NULL,
  `standardlevel` int(11) DEFAULT '0',
  `invite_count` int(11) DEFAULT '0' COMMENT '直推人数',
  `left_count` int(11) DEFAULT '0' COMMENT '左区总人数',
  `right_count` int(11) DEFAULT '0' COMMENT '右区总人数',
  `three_count` int(11) DEFAULT '0' COMMENT '三部总人数',
  `daoqi_time` int(10) UNSIGNED NOT NULL,
  `date` varchar(255) NOT NULL,
  `is_chuli` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=371 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- 表的结构 `nv_jiesuan_wangqi`
--

DROP TABLE IF EXISTS `nv_jiesuan_wangqi`;
CREATE TABLE IF NOT EXISTS `nv_jiesuan_wangqi` (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fa_zong` decimal(10,2) DEFAULT '0.00',
  `people_zong` decimal(10,0) DEFAULT '0',
  `vip_amount` decimal(10,2) DEFAULT '0.00',
  `one_star_amount` decimal(10,2) DEFAULT '0.00' COMMENT '直推人数',
  `two_star_amount` decimal(10,2) DEFAULT '0.00' COMMENT '直推人数',
  `three_star_amount` decimal(10,2) DEFAULT '0.00' COMMENT '直推人数',
  `four_star_amount` decimal(10,2) DEFAULT '0.00' COMMENT '直推人数',
  `five_star_amount` decimal(10,2) DEFAULT '0.00' COMMENT '直推人数',
  `six_star_amount` decimal(10,2) DEFAULT '0.00' COMMENT '直推人数',
  `seven_star_amount` decimal(10,2) DEFAULT '0.00' COMMENT '直推人数',
  `ds_amount` decimal(10,2) DEFAULT '0.00' COMMENT '直推人数',
  `vip_people` int(11) DEFAULT '0' COMMENT '左区总人数',
  `one_star_people` int(11) DEFAULT '0' COMMENT '右区总人数',
  `two_star_people` int(11) DEFAULT '0' COMMENT '三部总人数',
  `three_star_people` int(10) UNSIGNED DEFAULT NULL,
  `four_star_people` int(10) UNSIGNED DEFAULT NULL,
  `five_star_people` int(10) UNSIGNED DEFAULT NULL,
  `six_star_people` int(10) UNSIGNED DEFAULT NULL,
  `seven_star_people` int(10) UNSIGNED DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `is_chuli` int(11) DEFAULT '0',
  `qg_ds_amount` decimal(10,2) DEFAULT '0.00' COMMENT '直推人数',
  `hy_ds_amount` decimal(10,2) DEFAULT '0.00' COMMENT '直推人数',
  `zc_ds_amount` decimal(10,2) DEFAULT '0.00' COMMENT '直推人数',
  `hg_ds_amount` decimal(10,2) DEFAULT '0.00' COMMENT '直推人数',
  `ds_people` int(11) DEFAULT '0' COMMENT '右区总人数',
  `qg_ds_people` int(11) DEFAULT '0' COMMENT '三部总人数',
  `hy_ds_people` int(10) UNSIGNED DEFAULT NULL,
  `zc_ds_people` int(10) UNSIGNED DEFAULT NULL,
  `hg_ds_people` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- 表的结构 `nv_jiesuan_zhou`
--

DROP TABLE IF EXISTS `nv_jiesuan_zhou`;
CREATE TABLE IF NOT EXISTS `nv_jiesuan_zhou` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zq_start` int(10) NOT NULL,
  `zq_end` int(10) NOT NULL,
  `is_jiesuan` int(11) NOT NULL DEFAULT '0',
  `add_time` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `nv_js_info`
--

DROP TABLE IF EXISTS `nv_js_info`;
CREATE TABLE IF NOT EXISTS `nv_js_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `zhouqi_num` int(10) DEFAULT NULL,
  `zhouqi_start` date DEFAULT NULL,
  `zhouqi_end` date DEFAULT NULL,
  `zhouqi` varchar(50) DEFAULT NULL,
  `zamount` decimal(18,4) DEFAULT NULL,
  `zfenhong` decimal(18,4) DEFAULT NULL,
  `zucount` int(10) DEFAULT NULL,
  `fa_money` decimal(18,4) DEFAULT NULL,
  `kous_bili` decimal(18,4) DEFAULT NULL,
  `add_time` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `nv_message`
--

DROP TABLE IF EXISTS `nv_message`;
CREATE TABLE IF NOT EXISTS `nv_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_types` int(11) DEFAULT '0' COMMENT '发送类型',
  `accept_title` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '发送的标题',
  `accept_content` text CHARACTER SET utf8 COMMENT '发送的内容',
  `send_username` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '发送人用户名',
  `accept_username` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '接收人用户名',
  `addtime` datetime DEFAULT NULL,
  `states` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `nv_message`
--

INSERT INTO `nv_message` (`id`, `send_types`, `accept_title`, `accept_content`, `send_username`, `accept_username`, `addtime`, `states`) VALUES
(1, 2, '更改推荐人', '公司，请修改推荐关系如下：mx216972（猫咪源码）直接推荐mx216972（猫咪科技出品）谢谢。', 'h7152137', 'admin', '2017-05-20 15:14:18', 1),
(2, 1, '', '', '系统管理员', '18801108888', '2019-04-07 15:45:06', 0);

-- --------------------------------------------------------

--
-- 表的结构 `nv_msgvalidate`
--

DROP TABLE IF EXISTS `nv_msgvalidate`;
CREATE TABLE IF NOT EXISTS `nv_msgvalidate` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `addtime` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `nv_msgvalidate`
--

INSERT INTO `nv_msgvalidate` (`id`, `mobile`, `code`, `addtime`) VALUES
(1, '15315686836', '273104', 1548418638),
(2, '15315686836', '845739', 1550467243);

-- --------------------------------------------------------

--
-- 表的结构 `nv_news`
--

DROP TABLE IF EXISTS `nv_news`;
CREATE TABLE IF NOT EXISTS `nv_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `types` int(11) NOT NULL,
  `pic` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `title` varchar(50) CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `UpdateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `nv_news`
--

INSERT INTO `nv_news` (`id`, `types`, `pic`, `title`, `content`, `UpdateTime`) VALUES
(1, 0, '0', 'QQ614096466', 'QQ614096466', '2019-01-25 09:54:05');

-- --------------------------------------------------------

--
-- 表的结构 `nv_order_info`
--

DROP TABLE IF EXISTS `nv_order_info`;
CREATE TABLE IF NOT EXISTS `nv_order_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loginname` varchar(50) DEFAULT NULL,
  `users_level` tinyint(5) DEFAULT NULL,
  `order_amount` decimal(50,2) NOT NULL COMMENT '0.00',
  `address` varchar(255) DEFAULT NULL,
  `tel` varchar(60) NOT NULL DEFAULT '',
  `add_time` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `confirm_time` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `pay_time` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `shipping_time` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `chulidate` int(11) DEFAULT NULL,
  `is_chuli` int(4) DEFAULT '0',
  `goods_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `goods_name` varchar(120) NOT NULL DEFAULT '',
  `goods_number` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `goods_day` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `goods_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `order_status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `shipping_status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `pay_status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `order_sn` varchar(20) NOT NULL DEFAULT '',
  `truename` varchar(20) DEFAULT NULL,
  `kd_name` varchar(20) DEFAULT NULL,
  `kd_order` varchar(20) NOT NULL DEFAULT '',
  `order_jine` decimal(50,2) DEFAULT NULL COMMENT '先留着,没用',
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `nv_order_info`
--

INSERT INTO `nv_order_info` (`id`, `loginname`, `users_level`, `order_amount`, `address`, `tel`, `add_time`, `confirm_time`, `pay_time`, `shipping_time`, `chulidate`, `is_chuli`, `goods_id`, `goods_name`, `goods_number`, `goods_day`, `goods_price`, `order_status`, `shipping_status`, `pay_status`, `order_sn`, `truename`, `kd_name`, `kd_order`, `order_jine`, `userid`) VALUES
(1, '18801108888', NULL, '3999.00', '321213', '15822222222', 1554622602, 0, 1554622602, 0, NULL, 0, 6, '华为 HUAWEI Mate 20 麒麟980AI智能芯片全面屏超微距影像超大广角徕卡三摄6GB+128GB亮黑色全网通版双4G手机', 1, 0, '3999.00', 1, 0, 2, '2019040715045', '路大师', NULL, '', NULL, 0);

-- --------------------------------------------------------

--
-- 表的结构 `nv_system`
--

DROP TABLE IF EXISTS `nv_system`;
CREATE TABLE IF NOT EXISTS `nv_system` (
  `sId` int(1) NOT NULL AUTO_INCREMENT COMMENT '系统设置ID',
  `sName` varchar(50) NOT NULL COMMENT '系统名称',
  `sUrl` varchar(100) NOT NULL COMMENT '系统网址',
  `sCheckCodeSwitch` int(3) NOT NULL COMMENT '验证码开关',
  `sCheckCodeSwitch1` int(3) DEFAULT '0',
  `sRegUserCheckCode` int(3) DEFAULT '0',
  `tjcheckcode` int(3) DEFAULT '0',
  `sErrorPwdLockNum` int(3) NOT NULL DEFAULT '3' COMMENT '密码登陆错误几次锁定',
  `sLoginTimeout` int(8) NOT NULL DEFAULT '30' COMMENT '登陆超时（分钟）',
  `vip1` decimal(18,4) DEFAULT NULL,
  `vip2` decimal(18,4) DEFAULT NULL,
  `vip3` decimal(18,4) DEFAULT NULL,
  `tongka` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `yinka` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `jinka` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `dianmoney` decimal(18,4) DEFAULT NULL,
  `dianceng` int(10) DEFAULT NULL,
  `affectmoney` decimal(18,4) DEFAULT NULL,
  `affectceng` int(10) DEFAULT NULL,
  `cxbili` decimal(18,4) DEFAULT NULL,
  `ksbili` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `sysreg` int(10) DEFAULT '0',
  `syslogin` int(10) DEFAULT '0',
  `sysset1` int(10) DEFAULT '0',
  `guanbegin` int(10) DEFAULT NULL,
  `guanend` int(10) DEFAULT NULL,
  `sysset` int(10) DEFAULT '0',
  `jiandian_tong` int(10) DEFAULT NULL,
  `jiandian_yin` int(10) DEFAULT NULL,
  `jiandian_jin` int(10) DEFAULT NULL,
  `tong_bili` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `yin_bili` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `jin_bili` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `hdp1` varchar(255) DEFAULT NULL,
  `hdp2` varchar(255) DEFAULT NULL,
  `hdp3` varchar(255) DEFAULT NULL,
  `slogo` varchar(255) DEFAULT NULL,
  `sCompanyTel` varchar(255) DEFAULT NULL,
  `sCompany` varchar(255) DEFAULT NULL,
  `sCompanyName` varchar(255) DEFAULT NULL,
  `hdp1url` varchar(255) DEFAULT NULL,
  `hdp2url` varchar(255) DEFAULT NULL,
  `hdp3url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sId`) USING BTREE,
  KEY `sId` (`sId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `nv_system`
--

INSERT INTO `nv_system` (`sId`, `sName`, `sUrl`, `sCheckCodeSwitch`, `sCheckCodeSwitch1`, `sRegUserCheckCode`, `tjcheckcode`, `sErrorPwdLockNum`, `sLoginTimeout`, `vip1`, `vip2`, `vip3`, `tongka`, `yinka`, `jinka`, `dianmoney`, `dianceng`, `affectmoney`, `affectceng`, `cxbili`, `ksbili`, `sysreg`, `syslogin`, `sysset1`, `guanbegin`, `guanend`, `sysset`, `jiandian_tong`, `jiandian_yin`, `jiandian_jin`, `tong_bili`, `yin_bili`, `jin_bili`, `hdp1`, `hdp2`, `hdp3`, `slogo`, `sCompanyTel`, `sCompany`, `sCompanyName`, `hdp1url`, `hdp2url`, `hdp3url`) VALUES
(1, '创客新零售', 'http://', 0, 1, 1, 1, 20, 500, '200.0000', '1000.0000', '3000.0000', '1000.0000', '3000.0000', '6000.0000', '1.0000', 15, '10.0000', 20, '10.0000', '5.0000', 0, 0, 0, 8, 8, 0, 10, 15, 20, '1.0000', '1.2000', '1.5000', '2019-04-07/5ca9840749890.png', '2019-02-18/5c6a4597ddbcb.jpg', '2019-02-18/5c6a4597de062.jpg', '2019-02-18/5c6a5953cdacc.png', '08:00-18:00', '猫咪科技创客新零售会员登录', 'QQ614096466', 'www.maomikeji.top', 'www.maomikj.cn', 'www.maomikj.cn');

-- --------------------------------------------------------

--
-- 表的结构 `nv_sys_set`
--

DROP TABLE IF EXISTS `nv_sys_set`;
CREATE TABLE IF NOT EXISTS `nv_sys_set` (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `close_sys` int(11) NOT NULL DEFAULT '0',
  `inviteStart` varchar(50) CHARACTER SET utf16 DEFAULT NULL,
  `one_star_ge` int(10) DEFAULT NULL,
  `one_star_level` int(10) DEFAULT NULL,
  `one_star_team_ge` int(10) DEFAULT NULL,
  `one_star_team_level` int(10) DEFAULT NULL,
  `one_star_sh1` int(10) DEFAULT NULL,
  `one_star_sh1_dai` int(10) DEFAULT NULL,
  `one_star_sh2` int(10) DEFAULT NULL,
  `one_star_sh2_dai` int(10) DEFAULT NULL,
  `two_star_ge` int(10) DEFAULT NULL,
  `two_star_level` int(10) DEFAULT NULL,
  `two_star_team_ge` int(10) DEFAULT NULL,
  `two_star_team_level` varchar(255) DEFAULT NULL,
  `two_star_sh1` int(10) DEFAULT NULL,
  `two_star_sh1_dai` int(10) DEFAULT NULL,
  `two_star_sh2` int(10) DEFAULT NULL,
  `two_star_sh2_dai` int(10) DEFAULT NULL,
  `three_star_ge` int(10) DEFAULT NULL,
  `three_star_level` int(10) DEFAULT NULL,
  `three_star_team_ge` varchar(255) DEFAULT NULL,
  `three_star_team_level` varchar(255) DEFAULT NULL,
  `three_star_sh1` int(10) DEFAULT NULL,
  `three_star_sh1_dai` int(10) DEFAULT NULL,
  `three_star_sh2` int(10) DEFAULT NULL,
  `three_star_sh2_dai` int(10) DEFAULT NULL,
  `four_star_ge` int(10) DEFAULT NULL,
  `four_star_level` int(10) DEFAULT NULL,
  `four_star_team_ge` varchar(255) DEFAULT NULL,
  `four_star_team_level` varchar(255) DEFAULT NULL,
  `four_star_sh1` int(10) DEFAULT NULL,
  `four_star_sh1_dai` int(10) DEFAULT NULL,
  `four_star_sh2` int(10) DEFAULT NULL,
  `four_star_sh2_dai` int(10) DEFAULT NULL,
  `five_star_ge` int(10) DEFAULT NULL,
  `five_star_level` int(10) DEFAULT NULL,
  `five_star_team_ge` varchar(255) DEFAULT NULL,
  `five_star_team_level` varchar(255) DEFAULT NULL,
  `five_star_sh1` int(10) DEFAULT NULL,
  `five_star_sh1_dai` int(10) DEFAULT NULL,
  `five_star_sh2` int(10) DEFAULT NULL,
  `five_star_sh2_dai` int(10) DEFAULT NULL,
  `six_star_ge` int(10) DEFAULT NULL,
  `six_star_level` int(10) DEFAULT NULL,
  `six_star_team_ge` varchar(255) DEFAULT NULL,
  `six_star_team_level` varchar(255) DEFAULT NULL,
  `six_star_sh1` int(10) DEFAULT NULL,
  `six_star_sh1_dai` int(10) DEFAULT NULL,
  `six_star_sh2` int(10) DEFAULT NULL,
  `six_star_sh2_dai` int(10) DEFAULT NULL,
  `seven_star_ge` int(10) DEFAULT NULL,
  `seven_star_level` int(10) DEFAULT NULL,
  `seven_star_team_ge` varchar(255) DEFAULT NULL,
  `seven_star_team_level` varchar(255) DEFAULT NULL,
  `seven_star_sh1` int(10) DEFAULT NULL,
  `seven_star_sh1_dai` int(10) DEFAULT NULL,
  `seven_star_sh2` int(10) DEFAULT NULL,
  `seven_star_sh2_dai` int(10) DEFAULT NULL,
  `eight_star_ge` int(10) DEFAULT NULL,
  `eight_star_level` int(10) DEFAULT NULL,
  `eight_star_team_ge` varchar(255) DEFAULT NULL,
  `eight_star_team_level` varchar(255) DEFAULT NULL,
  `eight_star_sh1` int(10) DEFAULT NULL,
  `eight_star_sh1_dai` int(10) DEFAULT NULL,
  `eight_star_sh2` int(10) DEFAULT NULL,
  `eight_star_sh2_dai` int(10) DEFAULT NULL,
  `nine_star_ge` int(10) DEFAULT NULL,
  `nine_star_level` int(10) DEFAULT NULL,
  `nine_star_team_ge` varchar(255) DEFAULT NULL,
  `nine_star_team_level` varchar(255) DEFAULT NULL,
  `nine_star_sh1` int(10) DEFAULT NULL,
  `nine_star_sh1_dai` int(10) DEFAULT NULL,
  `nine_star_sh2` int(10) DEFAULT NULL,
  `nine_star_sh2_dai` int(10) DEFAULT NULL,
  `ten_star_ge` int(10) DEFAULT NULL,
  `ten_star_level` int(10) DEFAULT NULL,
  `ten_star_team_ge` int(10) DEFAULT NULL,
  `ten_star_team_level` int(10) DEFAULT NULL,
  `ten_star_sh1` int(10) DEFAULT NULL,
  `ten_star_sh1_dai` int(10) DEFAULT NULL,
  `ten_star_sh2` int(10) DEFAULT NULL,
  `ten_star_sh2_dai` int(10) DEFAULT NULL,
  `eleven_star_ge` int(10) DEFAULT NULL,
  `eleven_star_level` int(10) DEFAULT NULL,
  `eleven_star_team_ge` int(10) DEFAULT NULL,
  `eleven_star_team_level` int(10) DEFAULT NULL,
  `eleven_star_sh1` int(10) DEFAULT NULL,
  `eleven_star_sh1_dai` int(10) DEFAULT NULL,
  `eleven_star_sh2` int(10) DEFAULT NULL,
  `eleven_star_sh2_dai` int(10) DEFAULT NULL,
  `Twelve_star_ge` int(10) DEFAULT NULL,
  `Twelve_star_level` int(10) DEFAULT NULL,
  `Twelve_star_team_ge` int(10) DEFAULT NULL,
  `Twelve_star_team_level` int(10) DEFAULT NULL,
  `Twelve_star_sh1` int(10) DEFAULT NULL,
  `Twelve_star_sh1_dai` int(10) DEFAULT NULL,
  `Twelve_star_sh2` int(10) DEFAULT NULL,
  `Twelve_star_sh2_dai` int(10) DEFAULT NULL,
  `msgno` varchar(50) DEFAULT NULL,
  `msgscrepet` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `nv_sys_set`
--

INSERT INTO `nv_sys_set` (`id`, `close_sys`, `inviteStart`, `one_star_ge`, `one_star_level`, `one_star_team_ge`, `one_star_team_level`, `one_star_sh1`, `one_star_sh1_dai`, `one_star_sh2`, `one_star_sh2_dai`, `two_star_ge`, `two_star_level`, `two_star_team_ge`, `two_star_team_level`, `two_star_sh1`, `two_star_sh1_dai`, `two_star_sh2`, `two_star_sh2_dai`, `three_star_ge`, `three_star_level`, `three_star_team_ge`, `three_star_team_level`, `three_star_sh1`, `three_star_sh1_dai`, `three_star_sh2`, `three_star_sh2_dai`, `four_star_ge`, `four_star_level`, `four_star_team_ge`, `four_star_team_level`, `four_star_sh1`, `four_star_sh1_dai`, `four_star_sh2`, `four_star_sh2_dai`, `five_star_ge`, `five_star_level`, `five_star_team_ge`, `five_star_team_level`, `five_star_sh1`, `five_star_sh1_dai`, `five_star_sh2`, `five_star_sh2_dai`, `six_star_ge`, `six_star_level`, `six_star_team_ge`, `six_star_team_level`, `six_star_sh1`, `six_star_sh1_dai`, `six_star_sh2`, `six_star_sh2_dai`, `seven_star_ge`, `seven_star_level`, `seven_star_team_ge`, `seven_star_team_level`, `seven_star_sh1`, `seven_star_sh1_dai`, `seven_star_sh2`, `seven_star_sh2_dai`, `eight_star_ge`, `eight_star_level`, `eight_star_team_ge`, `eight_star_team_level`, `eight_star_sh1`, `eight_star_sh1_dai`, `eight_star_sh2`, `eight_star_sh2_dai`, `nine_star_ge`, `nine_star_level`, `nine_star_team_ge`, `nine_star_team_level`, `nine_star_sh1`, `nine_star_sh1_dai`, `nine_star_sh2`, `nine_star_sh2_dai`, `ten_star_ge`, `ten_star_level`, `ten_star_team_ge`, `ten_star_team_level`, `ten_star_sh1`, `ten_star_sh1_dai`, `ten_star_sh2`, `ten_star_sh2_dai`, `eleven_star_ge`, `eleven_star_level`, `eleven_star_team_ge`, `eleven_star_team_level`, `eleven_star_sh1`, `eleven_star_sh1_dai`, `eleven_star_sh2`, `eleven_star_sh2_dai`, `Twelve_star_ge`, `Twelve_star_level`, `Twelve_star_team_ge`, `Twelve_star_team_level`, `Twelve_star_sh1`, `Twelve_star_sh1_dai`, `Twelve_star_sh2`, `Twelve_star_sh2_dai`, `msgno`, `msgscrepet`) VALUES
(1, 0, 'ck', -1, -1, -1, -1, 1, 1, 4, 4, 3, 1, -1, '-1', 2, 2, -1, -1, 3, 1, '-1', '-1', 3, 3, -1, -1, 3, 1, '-1', '-1', 4, 4, 7, 7, 3, 1, '81', '1', 5, 5, -1, -1, 3, 1, '243', '1', 6, 6, -1, -1, 3, 1, '729', '1', 7, 7, 10, 10, 3, 1, '2187', '1', 8, 8, -1, -1, 3, 1, '6561', '1', 9, 9, -1, -1, 3, 1, 19683, 1, 10, 10, 12, 12, 3, 1, 59049, 1, 11, 11, -1, -1, 3, 1, 177147, 1, 12, 12, -1, -1, 'maomiyuanma', 'maomiyuanma');

-- --------------------------------------------------------

--
-- 表的结构 `nv_tixian_log`
--

DROP TABLE IF EXISTS `nv_tixian_log`;
CREATE TABLE IF NOT EXISTS `nv_tixian_log` (
  `log_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) UNSIGNED NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `real_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `change_time` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `change_desc` varchar(255) NOT NULL DEFAULT '0',
  `change_type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `cfxf_ratio` int(11) NOT NULL DEFAULT '20',
  `tixian_ratio` int(11) NOT NULL DEFAULT '5',
  `whf_ratio` int(11) NOT NULL DEFAULT '20',
  `loginname` varchar(20) DEFAULT NULL,
  `states` int(11) DEFAULT '0' COMMENT '-1表示临时会员1注册会员2VIP会员...',
  `bank` varchar(20) DEFAULT NULL,
  `bankno` varchar(20) DEFAULT NULL,
  `truename` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`log_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `nv_tousuinfo`
--

DROP TABLE IF EXISTS `nv_tousuinfo`;
CREATE TABLE IF NOT EXISTS `nv_tousuinfo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tsorderid` int(10) DEFAULT NULL,
  `tsuser` varchar(50) DEFAULT NULL,
  `tsuserobj` varchar(50) DEFAULT NULL,
  `addtime` int(10) DEFAULT NULL,
  `content` varbinary(255) DEFAULT NULL,
  `tstype` int(10) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- 表的结构 `nv_users`
--

DROP TABLE IF EXISTS `nv_users`;
CREATE TABLE IF NOT EXISTS `nv_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `rid` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `loginname` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `standardlevel` int(11) DEFAULT '-1' COMMENT '-1表示临时会员1注册会员2VIP会员...',
  `invite_count` int(11) DEFAULT '0' COMMENT '直推人数',
  `pwd1` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `pwd2` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `states` int(11) DEFAULT '0' COMMENT '0未激活1已激活',
  `truename` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `identityid` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `tel` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `addtime` int(10) DEFAULT NULL,
  `amount` decimal(18,2) DEFAULT '0.00',
  `dai` int(11) DEFAULT '0',
  `ceng` int(11) DEFAULT '0',
  `lockuser` int(11) DEFAULT '0',
  `lockusermod` int(4) DEFAULT '0',
  `lockuserpass` int(4) DEFAULT '0',
  `ppath` text CHARACTER SET utf8,
  `jihuo_path` text CHARACTER SET utf8,
  `reg_path` text CHARACTER SET utf8,
  `rpath` text CHARACTER SET utf8,
  `logincishu` int(9) DEFAULT '0',
  `email` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `logip` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_country` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_area` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_province` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_city` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_isp` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `tuijianma` varchar(50) DEFAULT NULL,
  `wximg` varbinary(255) DEFAULT NULL,
  `address` varchar(160) DEFAULT NULL,
  `shouhuo_name` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '收货人姓名',
  `shouhuo_tel` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '收货手机号码',
  `bonus_amount` decimal(10,2) DEFAULT '0.00',
  `gouwujf` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `nv_users`
--

INSERT INTO `nv_users` (`id`, `pid`, `rid`, `loginname`, `standardlevel`, `invite_count`, `pwd1`, `pwd2`, `states`, `truename`, `identityid`, `tel`, `addtime`, `amount`, `dai`, `ceng`, `lockuser`, `lockusermod`, `lockuserpass`, `ppath`, `jihuo_path`, `reg_path`, `rpath`, `logincishu`, `email`, `logip`, `log_country`, `log_area`, `log_province`, `log_city`, `log_isp`, `tuijianma`, `wximg`, `address`, `shouhuo_name`, `shouhuo_tel`, `bonus_amount`, `gouwujf`) VALUES
(1, '0', '0', '18801108888', 12, 0, 'e10adc3949ba59abbe56e057f20f883e', 'e10adc3949ba59abbe56e057f20f883e', 1, '老板', '370102198510182256', '18801108888', 1494969316, '1000000.00', 0, 0, 0, 0, 0, NULL, '', '', NULL, 8, NULL, '::1', '中国', '', '山东', '菏泽', '电信', 'ck190123001', 0x32303139303330332f356337616163616337613334612e6a7067, '321213', '路大师', '15822222222', '1002.00', 0);

-- --------------------------------------------------------

--
-- 表的结构 `nv_usersjinfo`
--

DROP TABLE IF EXISTS `nv_usersjinfo`;
CREATE TABLE IF NOT EXISTS `nv_usersjinfo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `loginname` varchar(50) DEFAULT NULL,
  `curlevel` int(10) DEFAULT NULL,
  `targetlevel` int(10) DEFAULT NULL,
  `shuser1` varchar(50) DEFAULT NULL,
  `shuser2` varchar(50) DEFAULT NULL,
  `status1` int(10) DEFAULT '0' COMMENT '0 未处理 1拒绝  2同意',
  `opuser` varchar(50) DEFAULT NULL,
  `addtime` int(10) DEFAULT NULL,
  `shtime1` int(10) DEFAULT NULL,
  `status2` int(10) DEFAULT '0',
  `shtime2` int(10) DEFAULT NULL,
  `sh1dianzan` int(10) DEFAULT '0',
  `sh2dianzan` int(10) DEFAULT '0',
  `sh1dztime` int(10) DEFAULT NULL,
  `sh2dztime` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- 表的结构 `nv_users_login`
--

DROP TABLE IF EXISTS `nv_users_login`;
CREATE TABLE IF NOT EXISTS `nv_users_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loginname` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `logip` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_country` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_area` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_province` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_city` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `log_isp` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `log_time` int(10) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `nv_users_login`
--

INSERT INTO `nv_users_login` (`id`, `loginname`, `logip`, `log_country`, `log_area`, `log_province`, `log_city`, `log_isp`, `log_time`) VALUES
(1, '18801108888', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, '18801108888', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, '18801108888', '144.0.152.232', '中国', '', '山东', '菏泽', '电信', 1548433942),
(4, '18801108888', '144.0.152.30', '中国', '', '山东', '菏泽', '电信', 1550465869),
(5, '18801108888', '144.0.152.35', NULL, NULL, NULL, NULL, NULL, 1551370220),
(6, '18801108888', '119.187.85.111', NULL, NULL, NULL, NULL, NULL, 1551380311),
(7, '18801108888', '144.0.152.35', NULL, NULL, NULL, NULL, NULL, 1551380547),
(8, '18801108888', '::1', NULL, NULL, NULL, NULL, NULL, 1554602553);

-- --------------------------------------------------------

--
-- 表的结构 `nv_yanqilog`
--

DROP TABLE IF EXISTS `nv_yanqilog`;
CREATE TABLE IF NOT EXISTS `nv_yanqilog` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `curtime` int(10) DEFAULT NULL,
  `addtime` int(10) DEFAULT NULL,
  `curlevel` int(10) DEFAULT NULL,
  `targettime` int(10) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
